package net.fuix.shop.init;

import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.*;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.fuix.library.network.FXNetworkRegistry;
import net.fuix.library.server.command.CommandManager;
import net.fuix.library.util.ISideCommon;
import net.fuix.shop.network.*;
import net.fuix.shop.server.command.CommandShop;

@Mod(modid = FuixShop.MOD_ID, name = FuixShop.MOD_NAME, version = FuixShop.MOD_VERSION, dependencies = "required-after:fuixlibrary;required-after:fuixdonate;")
public class FuixShop {

    public static final String MOD_ID = "fuixshop";
    public static final String MOD_NAME = "FuixShop";
    public static final String MOD_VERSION = "0.1";

    @Mod.Instance
    public static FuixShop instance;

    @SideOnly(Side.SERVER)
    public static CommandManager commandManager;

    @SidedProxy(clientSide = "net.fuix.shop.init.ClientSide", serverSide = "net.fuix.shop.init.ServerSide")
    public static ISideCommon proxy;

    /** ============================================================================================================ **/

    @Mod.EventHandler
    public void onConstruction(FMLConstructionEvent event) {
        proxy.onConstruction(event);
    }

    @Mod.EventHandler
    public void onInit(FMLInitializationEvent event) { proxy.onInit(event); }

    @Mod.EventHandler
    public void onPreInit(FMLPreInitializationEvent event) {
        proxy.onPreInit(event);

        FXNetworkRegistry.registerPacket(PacketShopGui.class);
    }

    @Mod.EventHandler
    public void onPostInit(FMLPostInitializationEvent event) {
        proxy.onPostInit(event);
    }

    @SideOnly(Side.SERVER)
    @Mod.EventHandler
    public void onServerStarting(FMLServerStartingEvent e) {
        this.commandManager = new CommandManager(e);
        this.commandManager.registerCommandExecutor(new CommandShop());
    }

    /** ============================================================================================================ **/

    public static FuixShop getInstance() { return instance; }

    public static ServerSide getServer() { return (ServerSide) getInstance().proxy; }
    public static ClientSide getClient() { return (ClientSide) getInstance().proxy; }

}
