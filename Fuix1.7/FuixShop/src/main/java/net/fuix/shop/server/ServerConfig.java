package net.fuix.shop.server;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.fuix.library.config.*;
import net.minecraftforge.common.config.Configuration;

import java.io.File;

@SideOnly(Side.SERVER)
@Config(name = "FuixShop")
public class ServerConfig {

    @ConfigInt(category = "main")
    public static int server_id = 1;

    /** **/

    @ConfigString(category = "database", comment = "localhost")
    public static String address = "localhost";

    @ConfigInt(category = "database")
    public static int port = 3306;

    @ConfigString(category = "database")
    public static String username = "";

    @ConfigString(category = "database")
    public static String password = "";

    @ConfigString(category = "database")
    public static String basename = "Shop";

    /** **/

    @ConfigString(category = "database_tables")
    public static String moneyTable = "fc_user";

    @ConfigString(category = "database_fields")
    public static String moneyField = "gold";

    @ConfigString(category = "database_fields")
    public static String uuidField = "uuid";

    @ConfigString(category = "database_tables")
    public static String catalogTable = "fx_game_shop";

    @ConfigString(category = "database_tables")
    public static String cartTable = "fx_game_shop_cart";

    @ConfigString(category = "database_tables")
    public static String enchantTable = "fx_game_shop_enchant";

    static {
        ConfigUtils.readConfig(ServerConfig.class);
    }

}
