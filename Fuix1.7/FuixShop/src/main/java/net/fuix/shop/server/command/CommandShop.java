package net.fuix.shop.server.command;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.fuix.donate.init.FuixDonate;
import net.fuix.donate.network.PacketDonate;
import net.fuix.library.network.packet.PacketNotification;
import net.fuix.library.server.command.CommandExecuteException;
import net.fuix.library.server.command.CommandExecutor;
import net.fuix.library.server.command.CommandHandler;
import net.fuix.library.util.PermissionsUtils;
import net.fuix.shop.init.FuixShop;
import net.fuix.shop.network.PacketShopGui;
import net.fuix.shop.server.ServerDataHelper;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.ChatComponentText;

@SideOnly(Side.SERVER)
public class CommandShop extends CommandExecutor {

    @SideOnly(Side.SERVER)
    @CommandHandler(name = "shop", group = "FuixShop")
    public void shop(final ICommandSender sender, final String[] args) throws CommandExecuteException {
        if (sender instanceof EntityPlayerMP) {
            EntityPlayerMP player = (EntityPlayerMP) sender;
            if(args.length <= 0 || args[0].equalsIgnoreCase("item")) {
                ServerDataHelper dataHelper = FuixShop.getServer().dataHelper;
                new PacketShopGui(PacketShopGui.Type.GUISHOP, dataHelper.getShopProduct(), dataHelper.getShopEnchant()).setEditMode(PermissionsUtils.hasPex(player, "fuix.shop.edit")).sendTo((EntityPlayerMP) sender);

                FuixDonate.getServer().onUpdateMoney(player);
            } else if(args[0].equalsIgnoreCase("group")) {
                new PacketNotification("Покупка донат групп в разработки! Ожидайте...", 3, "INFO").sendTo((EntityPlayerMP) sender);
            }
        }
    }
}