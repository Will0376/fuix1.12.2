package net.fuix.shop.client.ui.elements;

import net.fuix.donate.network.data.products.ProductItem;
import net.fuix.library.client.ui.MouseAction;
import net.fuix.library.client.ui.util.DrawHelper;
import net.fuix.library.client.ui.util.FontHelper;
import net.fuix.library.client.ui.widget.ListView;
import net.fuix.library.client.ui.widget.ListViewItem;
import net.fuix.shop.init.FuixShop;
import net.fuix.shop.client.ui.GuiShop;
import net.fuix.shop.client.ui.modal.ModalProductInfo;
import net.fuix.shop.network.data.Product;
import net.minecraft.util.ResourceLocation;

import java.awt.*;

public class ElementProduct extends ListViewItem {

    private GuiShop guiShop;
    private Product product;

    public ElementProduct(GuiShop guiShop, int width, int height, ListView listView) {
        super(width, height, listView);
        this.guiShop = guiShop;
    }

    public ElementProduct(GuiShop guiShop, Product product, int width, int height, ListView listView) {
        super(width, height, listView);
        this.guiShop = guiShop;
        this.product = product;
    }

    @Override
    public void onInit(int x, int y) { }

    @Override
    protected void onItemDraw(int x, int y, int topLeftX, int topLeftY) {
        int posX = getAbsoluteBounds().x;
        int posY = getAbsoluteBounds().y;
        int width = getBounds().width;
        int height = getBounds().height;
        if(product == null) this.product = ((Product)getListView().getObserver().getObservableList().get(getRenderId()));

        if(product.getTypeId() == 1) {
            ProductItem productItem = (ProductItem) product.getValue();

            DrawHelper.drawRectRounded(posX, posY, width, height, 0.4, new Color(255, 255, 255, 80).hashCode());
            DrawHelper.drawRectRounded(posX + 0.3, posY + 0.3, width- 0.6, height - 0.6, 0.4, new Color(0, 0, 0, 100).hashCode());

            DrawHelper.drawItem(productItem.getStack(), posX + (width / 2) - 12, posY, 24);

            String price = product.getPrice() + "";
            if(product.getDiscount() > 0) {
                price = "§m" + product.getPrice() + "§6 " + (product.getPrice() * (100 - product.getDiscount())/100);
            }

            DrawHelper.drawRect(posX + 0.3, posY + height - 11, width - 0.6, 0.3, new Color(255, 255, 255, 40).hashCode());

            if(isFocused()) {
                DrawHelper.drawRectRounded(posX + 0.3, posY + height - 11, width - 0.6, 11 - 0.3, 0.4, new Color(52, 255, 62, 50).hashCode());
                FontHelper.draw("Купить", posX + (width / 2), posY + height - 5.5, new Color(255, 255, 255).hashCode(), FontHelper.Align.CENTER, 0.8F);
            } else {
                DrawHelper.drawRectRounded(posX + 0.3, posY + height - 11, width - 0.6, 11 - 0.3, 0.4, new Color(255, 255, 255, 18).hashCode());
                DrawHelper.draw(new ResourceLocation(FuixShop.MOD_ID, "textures/gui/balance.png"), posX + 2, posY + height - 8, 6, 6);
                FontHelper.draw(price, posX + 11, posY + height - 5.5, new Color(255, 255, 255).hashCode(), FontHelper.Align.CENTER_Y, 0.8F);
            }

            //DrawHelper.draw(new ResourceLocation(FuixShop.MOD_ID, "textures/gui/balance.png"), posX + 20, posY + 3, 7, 7);
            //FontHelper.draw(price, posX + 29, posY + 2, new Color(255, 255, 255).hashCode(), 1);

            //FontHelper.draw("за " + product.getAmount() + "шт.", posX + 20, posY + 10, new Color(152, 251, 152, 180).hashCode(), 1);
        }
    }

    @Override
    protected void onUpdate(int x, int y) { }

    @Override
    protected void handleMouse(MouseAction mouseAction) {
        if (mouseAction.getActionType().equals(MouseAction.ActionType.Release) && getAbsoluteBounds().contains(mouseAction.getPosX(), mouseAction.getPosY())) {
            if(product == null) this.product = ((Product)getListView().getObserver().getObservableList().get(getRenderId()));

            /*((DialogBuying) guiShop.dialogBuying.getDialogPanel()).setProduct(product);
            guiShop.productPanel.setVisible(true);*/

            getWindow().onViewModal(new ModalProductInfo(product, guiShop.shopEnchant, getWindow()));
        }
    }

}
