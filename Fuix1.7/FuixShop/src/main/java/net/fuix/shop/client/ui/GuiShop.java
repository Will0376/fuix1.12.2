package net.fuix.shop.client.ui;

import com.google.common.collect.ImmutableList;
import net.fuix.donate.network.IGuiUpdateMoney;
import net.fuix.donate.network.data.products.ProductItem;
import net.fuix.library.client.ui.GuiComponent;
import net.fuix.library.client.ui.Window;
import net.fuix.library.client.ui.drawable.ButtonDrawable;
import net.fuix.library.client.ui.drawable.Drawable;
import net.fuix.library.client.ui.util.*;
import net.fuix.library.client.ui.widget.Button;
import net.fuix.library.client.ui.widget.ListView;
import net.fuix.library.client.ui.widget.ListViewItem;
import net.fuix.library.client.ui.widget.TextField;
import net.fuix.shop.init.FuixShop;
import net.fuix.shop.client.ui.elements.ElementCategory;
import net.fuix.shop.client.ui.elements.ElementProduct;

import net.fuix.shop.client.ui.modal.ModalSelectItem;
import net.fuix.shop.network.data.ShopEnchant;
import net.fuix.shop.network.data.Product;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.StatCollector;

import java.awt.*;
import java.util.*;
import java.util.List;
import java.util.stream.Collectors;

public class GuiShop extends Window implements IGuiUpdateMoney {

    public CategotyOption all_category = new CategotyOption("Все категории");
    public HashMap<String, CategotyOption> category = new HashMap<String, CategotyOption>();

    public int selectCategory;

    public ListView listView;
    public ListView categoryList;
    public TextField textSearch;

    public int money;
    public boolean editMode;
    public List<ShopEnchant> shopEnchant;

    private ArrayList<Product> shopProducts = new ArrayList<>();

    public GuiShop(boolean editMode, List<Product> shopProduct, List<ShopEnchant> shopEnchant) {
        super(344, 200);

        this.editMode = editMode;

        this.onUpdateProduct(shopProduct);

        this.shopEnchant = shopEnchant;
        this.selectCategory = 0;
    }

    @Override
    public void onUpdateMoney(int money) {
        this.money = money;
    }

    public void onUpdateProduct(List<Product> shopProduct) {
        for(Product shopProduct1 : shopProduct) {
            if(shopProduct1.getTypeId() == 1) {

                ItemStack stack = ((ProductItem)shopProduct1.getValue()).getStack();
                if (stack != null && stack.getItem().getCreativeTab() != null) {

                    if (category != null && category.containsKey(stack.getItem().getCreativeTab().getTabLabel())) {
                        category.get(stack.getItem().getCreativeTab().getTabLabel()).addItem(shopProduct1);
                    } else {
                        String localized_name = StatCollector.translateToLocal(stack.getItem().getCreativeTab().getTranslatedTabLabel());
                        category.put(stack.getItem().getCreativeTab().getTabLabel(), new CategotyOption(localized_name).addItem(shopProduct1));
                    }
                }

                all_category.addItem(shopProduct1);
            }
        }
    }

    @Override
    protected void onInit(int x, int y) {
        listView = new ListView("productList", new Rectangle(103, 5, this.getWidth() - 107, this.getHeight() - 10), this, new ArrayListObserver() {
            @Override
            public ListViewItem getListViewItem() {
                return new ElementProduct(GuiShop.this, 44, 38, getListView());
            }
        });
        listView.getListViewContent().setElementsPadding(3);
        addComponent(listView);
        setShopProducts(all_category.getItems());

        categoryList = new ListView("categoryList", new Rectangle(2, 35, 100 - 2, 200 - 35 - 2), this, new ArrayListObserver() {
            @Override
            public ListViewItem getListViewItem() {
                return new ElementCategory(GuiShop.this, 97, 14, getListView());
            }
        });
        categoryList.setDrawable(new Drawable() {
            @Override
            public void onDraw(GuiComponent component, Drawable.State state) {
                Rectangle rec = component.getAbsoluteBounds();
                DrawHelper.drawRect(rec.x, rec.y, rec.width, rec.height, new Color(255, 255, 255, 10).hashCode());

            }
        });

        categoryList.getListViewSlider().setSize(2);
        categoryList.getListViewContent().setElementsPadding(0);

        ArrayList list = new ArrayList(category.values());
        Comparator<CategotyOption> movieComparator = Comparator.comparing(CategotyOption::getName);
        Collections.sort(list, movieComparator);
        list.add(0, all_category);


        addComponent(categoryList);

        categoryList.updateList(new ArrayList<ItemStack>());
        categoryList.updateList(list);

        textSearch = new TextField("textSearch", new Rectangle(2, 20, 98, 15), this);
        textSearch.setHintText("Поиск");
        textSearch.setPaddingHorizontal(4);
        textSearch.setDrawable(new Drawable() {
            @Override
            public void onDraw(GuiComponent component, State state) {




            }
        });
        addComponent(textSearch);

        if(editMode) {
            addComponent(new Button("textAdd", new Rectangle(getWidth() - 70, -14, 70, 12), this).setText("Добавить товар").setCallback(new Runnable() {
                @Override
                public void run() {
                    onViewModal(new ModalSelectItem(GuiShop.this));
                }
            }).setDrawable(ButtonDrawable.RED));
        }
    }

    private ArrayList<Product> filterBySearch(final String query) {
        listView.updateList(new ArrayList<>());

        List<Product> list = ImmutableList.copyOf(shopProducts).stream().filter(it -> ((ProductItem)it.getValue()).getStack().getDisplayName().toLowerCase().contains(query.toLowerCase().trim())).collect(Collectors.toList());
        listView.updateList(new ArrayList<>(list));

        return new ArrayList<>(list);
    }

    protected void onUpdate() {
        if(textSearch != null) {
            if(textSearch.getIsChanged()) filterBySearch(textSearch.getText());
        }
    }

    @Override
    protected void onDraw() {
        Point p = getZeroPoint();

        DrawHelper.drawRect(0, 0, Monitor.getWidth(), Monitor.getHeight() + 10, new Color(32,32,32, 80).hashCode());

        GuiUtils.drawBackgroundGui(getZeroPoint(), getWidth(), getHeight());

        DrawHelper.drawRect(getZeroPoint().x, getZeroPoint().y, 100, 20, new Color(255, 255, 255, 10).hashCode());
        DrawHelper.drawRect(p.x, p.y + 20, 100, 0.3, new Color(255,255,255, 60).hashCode());

        FontHelper.draw("Магазин", p.x + 6, p.y + 3, new Color(255, 255, 255).hashCode(), 1);

        DrawHelper.draw(new ResourceLocation(FuixShop.MOD_ID, "textures/gui/balance.png"), p.x + 6, p.y + 11, 7, 7);
        FontHelper.draw("§6" + money, p.x + 15, p.y + 10, new Color(255, 255, 255).hashCode(), 1);

        DrawHelper.drawRect(p.x, p.y + 35, 100, 0.3, new Color(255,255,255, 60).hashCode());

        DrawHelper.drawRect(getZeroPoint().x + 100, getZeroPoint().y, 0.3, getHeight(), new Color(255,255,255, 60).hashCode());

    }

    public void setShopProducts(ArrayList<Product> items) {
        this.shopProducts = items;
        listView.updateList(new ArrayList<ItemStack>());
        listView.updateList(items);
    }

    public static class CategotyOption {

        private String name;
        private ArrayList<Product> items;

        public CategotyOption(String name) {
            this.name = name;
            this.items = new ArrayList<>();
        }

        public CategotyOption setName(String name) {
            this.name = name;
            return this;
        }

        public CategotyOption setItems(ArrayList<Product> items) {
            this.items = items;
            return this;
        }

        public CategotyOption addItem(Product item) {
            this.items.add(item);
            return this;
        }

        public String getName() {
            return name;
        }
        public ArrayList<Product> getItems() {
            return items;
        }
    }
}
