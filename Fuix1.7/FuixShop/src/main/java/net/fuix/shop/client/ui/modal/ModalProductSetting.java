package net.fuix.shop.client.ui.modal;

import net.fuix.donate.network.data.products.ProductItem;
import net.fuix.library.client.ui.GuiComponent;
import net.fuix.library.client.ui.ModalWindow;
import net.fuix.library.client.ui.Window;
import net.fuix.library.client.ui.drawable.ButtonDrawable;
import net.fuix.library.client.ui.drawable.Drawable;
import net.fuix.library.client.ui.util.DrawHelper;
import net.fuix.library.client.ui.widget.Button;
import net.fuix.library.client.ui.widget.*;
import net.fuix.library.client.ui.widget.TextField;
import net.fuix.shop.init.FuixShop;
import net.fuix.shop.client.ui.GuiUtils;
import net.fuix.library.network.IGuiResult;
import net.fuix.library.network.packet.data.ResultGui;
import net.fuix.shop.network.PacketShopGui;
import net.fuix.shop.network.data.ShopEnchant;
import net.fuix.shop.network.data.Product;
import net.minecraft.item.Item;

import java.awt.*;
import java.util.List;

public class ModalProductSetting extends ModalWindow implements IGuiResult {

    private Product shopProduct;
    private List<ShopEnchant> shopEnchant;

    private boolean edit = true;

    private Button btnSave;
    private Button btnRemove;

    public ModalProductSetting(Product shopProduct, List<ShopEnchant> shopEnchant, boolean edit, Window mainWindow) {
        super(mainWindow, 185, 200); // 161
        this.shopProduct = shopProduct;
        this.shopEnchant = shopEnchant;
        this.edit = edit;
    }

    @Override
    public void onResultGui(ResultGui result) {
        this.btnSave.setEnabled(true);
        this.btnRemove.setEnabled(true);
    }

    @Override
    protected void onInit(int x, int y) {
        addComponent(new ImageButton("textBack", new Rectangle(getWidth() - 18, - 10, 8, 8), new Rectangle(5, 5), FuixShop.MOD_ID, "textures/gui/icons/ic_back.png", this).setCallback(() -> {
            if(edit) {
                getMainWindow().onViewModal(new ModalProductInfo(shopProduct, shopEnchant, getMainWindow()));
            } else {
                getMainWindow().onViewModal(new ModalSelectItem(getMainWindow()));
            }
        }).setDrawable(ButtonDrawable.ORANGE));

        addComponent(new ImageButton("textExit", new Rectangle(getWidth() - 8, - 10, 8, 8), new Rectangle(4, 4), FuixShop.MOD_ID, "textures/gui/icons/ic_close.png", this).setCallback(() -> {
            onClose();
        }).setDrawable(ButtonDrawable.RED));

        if(shopProduct.getTypeId() == 1) {

            btnRemove = (Button) new Button("btnRemove", new Rectangle(5, getHeight() - 20, 40, 14), this).setText("Удалить").setCallback(() -> {
                btnSave.setEnabled(false);
                btnRemove.setEnabled(false);
                new PacketShopGui(PacketShopGui.Type.REMOVE, shopProduct).sendToServer();
            }).setShadow(true).setDrawable(ButtonDrawable.RED);

            TextField editItemId = addTextField("editItemId", "Идентификатор:", new Rectangle(5, 5, getWidth() - 45, 13));
            editItemId.setText(Item.itemRegistry.getNameForObject(((ProductItem)shopProduct.getValue()).getStack().getItem()));

            TextField editItemData = addTextField("editItemData", "Дата:", new Rectangle(getWidth() - 35, 5, 30, 13));
            editItemData.setText(String.valueOf(((ProductItem)shopProduct.getValue()).getStack().getItemDamage()));

            TextField editName = addTextField("editName", "Название предмета:", new Rectangle(5, 34, getWidth() - 10, 13));
            editName.setText(shopProduct.getName() != null && !shopProduct.getName().equals("None") ? shopProduct.getName() : ((ProductItem)shopProduct.getValue()).getStack().getDisplayName());

            TextField editDescription = addTextField("editDescription", "Описание:", new Rectangle(5, 58, getWidth() - 10, 13));
            if(edit) editDescription.setText(shopProduct.getDescription() != null && !shopProduct.getDescription().equals("None") ? shopProduct.getDescription() : "");

            TextField editImageUrl = addTextField("editImageUrl", "Ссылка на кратинку:", new Rectangle(5, 82, getWidth() - 10, 13));
            editImageUrl.setText(String.valueOf(shopProduct.getImageUrl()));

            TextField editServerId = addTextField("editServerId", "Сервера:", new Rectangle(5 + 45 * 0, 106, 40, 13));
            editServerId.setText(String.valueOf(shopProduct.getServers()));

            TextField editAmount = addTextField("editAmount", "Колво:", new Rectangle(5 + 45 * 1, 106, 40, 13));
            editAmount.setText(String.valueOf(shopProduct.getAmount()));

            TextField editPrice = addTextField("editPrice", "Цена:", new Rectangle(5 + 45 * 2, 106, 40, 13));
            editPrice.setText(String.valueOf(shopProduct.getPrice()));

            TextField editDiscount = addTextField("editDiscount", "Скидка:", new Rectangle(5 + 45 * 3, 106, 40, 13));
            editDiscount.setText(String.valueOf(shopProduct.getDiscount()));

            CheckBox checkEnchant = new CheckBox("checkEnchant", new Rectangle(5, 130, 9, 9), this, "Возможность зачаровать");
            if(((ProductItem)shopProduct.getValue()).getStack().isItemEnchantable()){
                checkEnchant.setChecked(true);
            }
            if(edit) checkEnchant.setChecked(shopProduct.isEnchant());
            addComponent(checkEnchant);

            CheckBox checkEnable = new CheckBox("checkEnable", new Rectangle(5, 143, 9, 9), this, "Возможность покупки");
            checkEnable.setChecked(true);
            if(edit) checkEnable.setChecked(shopProduct.isEnable());
            addComponent(checkEnable);

            addComponent(btnSave = (Button) new Button("btnSave", new Rectangle(getWidth() - 45, getHeight() - 20, 40,14), this).setText(edit ? "Сохранить" : "Добавить").setCallback(() -> {
                btnSave.setEnabled(false);
                btnRemove.setEnabled(false);
                if(shopProduct.getTypeId() == 1) {
                    shopProduct.setName(editName.getText());
                    shopProduct.setDescription(editDescription.getText());
                    shopProduct.setImageUrl(editImageUrl.getText());
                    shopProduct.setServers(editServerId.getText().isEmpty() ? "" : editServerId.getText());
                    shopProduct.setAmount(Integer.parseInt(editAmount.getText().isEmpty() ? "1" : editAmount.getText()));
                    shopProduct.setPrice(Integer.parseInt(editPrice.getText().isEmpty() ? "1" : editPrice.getText()));
                    shopProduct.setDiscount(Integer.parseInt(editDiscount.getText().isEmpty() ? "0" : editDiscount.getText()));
                    shopProduct.setEnchant(checkEnchant.isChecked());
                    shopProduct.setEnable(checkEnable.isChecked());
                }

                new PacketShopGui(PacketShopGui.Type.EDIT, shopProduct).sendToServer();
            }).setShadow(true));

            if(edit) {
                addComponent(btnRemove);
            }
        }
    }


    @Override
    protected void onDraw() {
        Point p = getZeroPoint();

        btnSave.setBounds(new Rectangle(new Point(getWidth() - 45, getHeight() - 20), btnSave.getBounds().getSize()));

        Window mw = getMainWindow();
        DrawHelper.drawRectRounded(mw.getZeroPoint().x, mw.getZeroPoint().y, mw.getWidth(), mw.getHeight(), 1, new Color(32,32,32, 150).hashCode());

        GuiUtils.drawBackgroundGui(getZeroPoint(), getWidth(), getHeight());

        if(shopProduct.getTypeId() == 1) {
            DrawHelper.drawRect(p.x, p.y + 29, getWidth(), 0.3, new Color(255,255,255, 60).hashCode());
            DrawHelper.drawRect(p.x, p.y + getHeight() - 23, getWidth(), 0.3, new Color(255,255,255, 60).hashCode());
        }
    }

    private TextField addTextField(String editId, String desc, Rectangle r) {
        addComponent(new TextView(editId + "Desc", new Rectangle(r.x, r.y - 2, r.width, 0), this).setText(desc).setFontSize(0.8F).setFontColor(new Color(255, 255, 255, 180).hashCode()));
        TextField editName = new TextField(editId, new Rectangle(r.x, r.y + 7, r.width, r.height), this);
        editName.setDrawable(new Drawable() {
            public void onDraw(GuiComponent component, State state) {
                Rectangle rec = component.getAbsoluteBounds();
                DrawHelper.drawRectRounded(rec.x, rec.y, rec.width, rec.height, 1, new Color(255, 255, 255, 40).hashCode());
            }
        });
        addComponent(editName);
        return editName;
    }
}
