package net.fuix.shop.network.data;

import net.fuix.donate.network.data.products.ProductGroup;
import net.fuix.donate.network.data.products.ProductItem;
import net.minecraft.network.PacketBuffer;

import java.io.IOException;

public class Product {

    private int id;
    private int type_id;
    private String name = "None";
    private String description = "None";

    private Object value;

    private int amount = 1;
    private int price = 0;
    private int discount = 0;
    private boolean enchant = false;
    private boolean enable = true;
    private int bought = 0;
    private String servers = "";
    private String imageUrl = "";

    public Product(int id, int type_id, int amount, int price) {
        this.id = id;
        this.type_id = type_id;
        this.amount = amount;
        this.price = price;
    }

    public int getId() { return id; }
    public int getTypeId() { return type_id; }
    public Object getValue() { return value; }
    public String getName() { return name; }
    public String getDescription() { return description; }
    public int getAmount() { return amount; }
    public int getPrice() { return price; }
    public boolean isEnchant() { return enchant; }
    public boolean isEnable() { return enable; }
    public int getBought() { return bought; }
    public int getDiscount() { return discount; }
    public String getServers() { return servers; }
    public String getImageUrl() { return imageUrl; }

    public Product setValue(Object value) {
        this.value = value;
        return this;
    }

    public Product setName(String name) {
        this.name = name;
        return this;
    }

    public Product setDescription(String description) {
        this.description = description;
        return this;
    }

    public Product setAmount(int amount) {
        this.amount = amount;
        return this;
    }

    public Product setPrice(int price) {
        this.price = price;
        return this;
    }

    public Product setEnchant(boolean enchant) {
        this.enchant = enchant;
        return this;
    }

    public Product setEnable(boolean enable) {
        this.enable = enable;
        return this;
    }

    public Product setBought(int bought) {
        this.bought = bought;
        return this;
    }

    public Product setDiscount(int discount) {
        this.discount = discount;
        return this;
    }

    public Product setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
        return this;
    }

    public Product setServers(String servers) {
        this.servers = servers;
        return this;
    }

    /**==============================================================================================================**/

    public PacketBuffer write(PacketBuffer buf) throws IOException {
        buf.writeInt(getId());
        buf.writeInt(getTypeId());
        buf.writeInt(getAmount());
        buf.writeInt(getPrice());

        buf.writeStringToBuffer(getName());
        buf.writeStringToBuffer(getDescription());
        buf.writeInt(getDiscount());
        buf.writeBoolean(isEnchant());
        buf.writeBoolean(isEnable());
        buf.writeInt(getBought());

        buf.writeStringToBuffer(getImageUrl());
        buf.writeStringToBuffer(getServers());

        switch (getTypeId()) {
            case 1: ((ProductItem) getValue()).write(buf); break;
            case 3: ((ProductGroup) getValue()).write(buf); break;
        }

        return buf;
    }

    public static Product read(PacketBuffer buf) throws IOException {
        int id = buf.readInt();
        int type_id = buf.readInt();

        Product shopProduct = new Product(id, type_id, buf.readInt(), buf.readInt());
        shopProduct.setName(buf.readStringFromBuffer(64));
        shopProduct.setDescription(buf.readStringFromBuffer(255));
        shopProduct.setDiscount(buf.readInt());
        shopProduct.setEnchant(buf.readBoolean());
        shopProduct.setEnable(buf.readBoolean());
        shopProduct.setBought(buf.readInt());

        shopProduct.setImageUrl(buf.readStringFromBuffer(255));
        shopProduct.setServers(buf.readStringFromBuffer(32));

        Object value = null;

        switch (type_id) {
            case 1: value = ProductItem.read(buf); break;
            case 3: value = ProductGroup.read(buf); break;
        }

        shopProduct.setValue(value);

        return shopProduct;
    }

    public Product clone() {
        return new Product(getId(), getTypeId(), getAmount(), getPrice())
                .setValue(getValue())
                .setName(getName())
                .setDescription(getDescription())
                .setAmount(getAmount())
                .setEnchant(isEnchant())
                .setEnable(isEnable())
                .setBought(getBought())
                .setDiscount(getDiscount())
                .setImageUrl(getImageUrl())
                .setServers(getServers());
    }
}
