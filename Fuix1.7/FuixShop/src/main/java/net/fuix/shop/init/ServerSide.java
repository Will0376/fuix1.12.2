package net.fuix.shop.init;

import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.event.FMLConstructionEvent;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import net.fuix.library.config.ConfigUtils;
import net.fuix.library.util.ISideCommon;
import net.fuix.shop.server.ServerConfig;
import net.fuix.shop.server.ServerDataHelper;
import net.minecraftforge.common.MinecraftForge;

public class ServerSide implements ISideCommon {

    public ServerDataHelper dataHelper;

    @Override
    public void onConstruction(FMLConstructionEvent event) { }

    @Override
    public void onInit(FMLInitializationEvent event) {
        dataHelper = new ServerDataHelper();
        dataHelper.onLoadCatalog();
        dataHelper.onLoadEnchant();
        MinecraftForge.EVENT_BUS.register(this);
        FMLCommonHandler.instance().bus().register(this);
    }

    @Override
    public void onPreInit(FMLPreInitializationEvent event) {
        ConfigUtils.readConfig(ServerConfig.class);
    }

    @Override
    public void onPostInit(FMLPostInitializationEvent event) { }

}
