package net.fuix.shop.init;

import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.event.FMLConstructionEvent;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.gameevent.InputEvent;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.fuix.library.client.ui.GuiScreenAdapter;
import net.fuix.library.util.ISideCommon;
import net.minecraft.client.Minecraft;
import net.minecraftforge.common.MinecraftForge;
import org.lwjgl.input.Keyboard;

public class ClientSide implements ISideCommon {

    /*@SideOnly(Side.CLIENT)
    @SubscribeEvent
    public void onKeyInput(InputEvent.KeyInputEvent e) {
        if (Keyboard.isKeyDown(Keyboard.KEY_G)) {
            Minecraft.getMinecraft().displayGuiScreen(new GuiScreenAdapter(new GuiDonate()));
        }
    }*/

    @Override
    public void onConstruction(FMLConstructionEvent event) { }

    @Override
    public void onInit(FMLInitializationEvent event) {
        FMLCommonHandler.instance().bus().register(this);
        MinecraftForge.EVENT_BUS.register(this);
    }

    @SideOnly(Side.CLIENT)
    @Override
    public void onPreInit(FMLPreInitializationEvent event) { }

    @Override
    public void onPostInit(FMLPostInitializationEvent event) { }

}
