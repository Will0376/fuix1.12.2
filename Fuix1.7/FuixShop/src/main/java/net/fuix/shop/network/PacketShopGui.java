package net.fuix.shop.network;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.fuix.donate.init.FuixDonate;
import net.fuix.library.client.ui.GuiScreenAdapter;
import net.fuix.library.client.ui.Window;
import net.fuix.library.network.FXPacket;
import net.fuix.library.network.IGuiResult;
import net.fuix.library.network.packet.PacketNotification;
import net.fuix.library.util.PermissionsUtils;
import net.fuix.shop.init.FuixShop;
import net.fuix.shop.client.ui.GuiShop;
import net.fuix.library.network.packet.data.ResultGui;
import net.fuix.shop.network.data.ShopEnchant;
import net.fuix.shop.network.data.Product;
import net.fuix.shop.server.ServerDataHelper;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.network.NetHandlerPlayClient;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.network.NetHandlerPlayServer;
import net.minecraft.network.PacketBuffer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class PacketShopGui extends FXPacket {

    private Type type = Type.NONE;
    private boolean editMode = false;

    private List<Product> shopProduct = new ArrayList<>();
    private List<ShopEnchant> shopEnchant = new ArrayList<>();

    private Product product = null;

    public ResultGui result = null;

    public PacketShopGui() {  }

    public PacketShopGui(Type type, ResultGui result) {
        this.type = type;
        this.result = result;
    }

    public PacketShopGui(Type type, List<Product> shopProduct, List<ShopEnchant> shopEnchant) {
        this.type = type;
        this.shopProduct = shopProduct;
        this.shopEnchant = shopEnchant;
    }

    public PacketShopGui(Type type, Product product, List<ShopEnchant> shopEnchant) {
        this.type = type;
        this.product = product;
        this.shopEnchant = shopEnchant;
    }

    public PacketShopGui(Type type, Product product) {
        this.type = type;
        this.product = product;
    }

    @Override
    public void write(PacketBuffer buf) throws IOException {
        buf.writeStringToBuffer(type.name());
        if(type == Type.GUISHOP) {
            buf.writeBoolean(editMode);

            buf.writeInt(shopProduct.size());
            for (Product item : shopProduct) {
                item.write(buf);
            }

            buf.writeInt(shopEnchant.size());
            for (ShopEnchant item : shopEnchant) {
                item.write(buf);
            }
        } else if(type == Type.EDIT || type == Type.REMOVE) {
            product.write(buf);
        } else if(type == Type.BUY) {
            this.product.write(buf);
            buf.writeInt(shopEnchant.size());
            for(ShopEnchant item : shopEnchant) {
                item.write(buf);
            }
        }
    }

    @Override
    public void read(PacketBuffer buf) throws IOException {
        type = PacketShopGui.Type.valueOf(buf.readStringFromBuffer(32));
        if(type == Type.GUISHOP) {
            editMode = buf.readBoolean();

            int size = buf.readInt();
            shopProduct = size == 0 ? Collections.<Product>emptyList() : new ArrayList<Product>(size);
            for (int i = 0; i < size; i++) {
                shopProduct.add(Product.read(buf));
            }

            size = buf.readInt();
            shopEnchant = size == 0 ? Collections.<ShopEnchant>emptyList() : new ArrayList<ShopEnchant>(size);
            for (int i = 0; i < size; i++) {
                shopEnchant.add(ShopEnchant.read(buf));
            }
        } else if(type == Type.EDIT || type == Type.REMOVE) {
            product = Product.read(buf);
        } else if(type == Type.BUY) {
            this.product = Product.read(buf);
            int size = buf.readInt();
            shopEnchant = size == 0 ? Collections.<ShopEnchant>emptyList() : new ArrayList<ShopEnchant>(size);
            for(int i = 0; i < size; i++) {
                shopEnchant.add(ShopEnchant.read(buf));
            }
        }
    }

    @SideOnly(Side.SERVER)
    @Override
    public void processServer(NetHandlerPlayServer net) {
        EntityPlayerMP player = net.playerEntity;
        ServerDataHelper dataHelper = FuixShop.getServer().dataHelper;

        if(type == Type.EDIT) {
            if(!PermissionsUtils.hasPex(player, "fuix.shop.edit")) {
                new PacketNotification("§cНет прав на использование (EDIT)!", 3, "INFO").sendTo(net);
                new PacketShopGui(Type.RESULTCODE, new ResultGui(1, "")).sendTo(net);
                return;
            }
            dataHelper.onEditProduct(product);
        } else if(type == Type.REMOVE) {
            if(!PermissionsUtils.hasPex(player, "fuix.shop.remove")) {
                new PacketNotification("§cНет прав на использование (REMOVE)!", 3, "INFO").sendTo(net);
                new PacketShopGui(Type.RESULTCODE, new ResultGui(1, "")).sendTo(net);
                return;
            }
            dataHelper.onRemoveProduct(product);
        }

        if(type == Type.GUISHOP || type == Type.EDIT || type == Type.REMOVE) {
            new PacketShopGui(Type.GUISHOP, dataHelper.getShopProduct(), dataHelper.getShopEnchant()).setEditMode(PermissionsUtils.hasPex(player, "fuix.shop.edit")).sendTo(net);
            FuixDonate.getServer().onUpdateMoney(player);
        }

        if(type == Type.BUY) {
            if(!PermissionsUtils.hasPex(player, "fuix.shop.buy")) {
                new PacketNotification("Нет прав на покупку предмета!", 3, "INFO").sendTo(net);
                new PacketShopGui(Type.RESULTCODE, new ResultGui(1, "")).sendTo(net);
                return;
            }
            FuixShop.getServer().dataHelper.onBuyProduct(player, product, shopEnchant);
        }

        new PacketShopGui(Type.RESULTCODE, new ResultGui(0, "")).sendTo(net);
    }

    @SideOnly(Side.CLIENT)
    @Override
    public void processClient(NetHandlerPlayClient net) {
        if(type == Type.GUISHOP) {
            Minecraft.getMinecraft().displayGuiScreen(new GuiScreenAdapter(new GuiShop(editMode, shopProduct, shopEnchant)));
        } else if(type == Type.RESULTCODE) {
            GuiScreen currentScreen = Minecraft.getMinecraft().currentScreen;
            if(currentScreen instanceof GuiScreenAdapter) {
                Window gui = ((GuiScreenAdapter) currentScreen).getWindow().getModalWindow();
                if(gui instanceof IGuiResult) {
                    ((IGuiResult) gui).onResultGui(result);
                }
            }
        }
    }

    public static enum Type {
        NONE, BUY, EDIT, REMOVE, RESULTCODE, GUISHOP,
    }

    public PacketShopGui setEditMode(boolean editMode) {
        this.editMode = editMode;
        return this;
    }
}
