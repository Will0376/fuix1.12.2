-- phpMyAdmin SQL Dump
-- version 4.6.6deb4
-- https://www.phpmyadmin.net/
--
-- Хост: localhost:3306
-- Время создания: Окт 20 2019 г., 23:30
-- Версия сервера: 10.3.12-MariaDB-1:10.3.12+maria~stretch-log
-- Версия PHP: 7.0.33-11+0~20190923.20+debian9~1.gbpd05c7e

SET
SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET
time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `fuix_centurymine_site`
--

-- --------------------------------------------------------

--
-- Структура таблицы `fx_game_shop`
--

CREATE TABLE `fx_game_shop`
(
    `id`          int(16) NOT NULL,
    `type_id`     int(2) NOT NULL,
    `servers`     varchar(36)  NOT NULL,
    `value`       text         NOT NULL,
    `price`       int(11) NOT NULL DEFAULT 10,
    `name`        varchar(64)  NOT NULL,
    `description` varchar(255) NOT NULL,
    `amount`      int(6) NOT NULL,
    `discount`    int(6) NOT NULL DEFAULT 0,
    `enchant`     tinyint(1) NOT NULL DEFAULT 0,
    `enable`      tinyint(1) NOT NULL DEFAULT 0,
    `bought`      int(16) NOT NULL DEFAULT 0,
    `image_url`   varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `fx_game_shop`
--
ALTER TABLE `fx_game_shop`
    ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `fx_game_shop`
--
ALTER TABLE `fx_game_shop`
    MODIFY `id` int (16) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
