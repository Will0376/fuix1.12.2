-- phpMyAdmin SQL Dump
-- version 4.6.6deb4
-- https://www.phpmyadmin.net/
--
-- Хост: localhost:3306
-- Время создания: Окт 20 2019 г., 23:30
-- Версия сервера: 10.3.12-MariaDB-1:10.3.12+maria~stretch-log
-- Версия PHP: 7.0.33-11+0~20190923.20+debian9~1.gbpd05c7e

SET
SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET
time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `fuix_centurymine_site`
--

-- --------------------------------------------------------

--
-- Структура таблицы `fx_game_shop_enchant`
--

CREATE TABLE `fx_game_shop_enchant`
(
    `id`         int(10) NOT NULL,
    `enchant_id` int(10) NOT NULL,
    `name`       varchar(36) NOT NULL,
    `desc`       varchar(84) NOT NULL,
    `price`      int(20) NOT NULL,
    `max`        int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Дамп данных таблицы `fx_game_shop_enchant`
--

INSERT INTO `fx_game_shop_enchant` (`id`, `enchant_id`, `name`, `desc`, `price`, `max`)
VALUES (1, 0, 'Защита', 'Преобразует весь урон от всех источников в урон брони', 5, 10),
       (2, 1, 'Огнестойкость', 'Защита от огня, лавы и огненных шаров ифритов. Уменьшает время горения', 8, 10),
       (3, 2, 'Лёгкость', 'Защита от урона при падении', 1, 10),
       (4, 3, 'Взрывоустойчивость', 'Защита от взрывов. Уменьшает отдачу от взрывов', 2, 10),
       (5, 4, 'Защита от снарядов', 'Защита от снарядов (стрел и огненных шаров)', 3, 10),
       (6, 5, 'Дыхание', 'Уменьшает потерю воздуха под водой, увеличивает время между приступами удушья', 1, 10),
       (7, 6, 'Родство с водой', 'Увеличивает скорость работы под водой', 8, 1),
       (8, 7, 'Шипы', 'С некоторым шансом наносит урон атакующему', 3, 10),
       (9, 16, 'Острота', 'Дополнительный урон', 2, 10),
       (10, 17, 'Небесная кара', 'Дополнительный урон зомби, свинозомби, скелетам, иссушителям и скелетам-иссушителям',
        1, 10),
       (11, 18, 'Бич членистоногих', 'Дополнительный урон паукам и чешуйницам', 1, 10),
       (12, 19, 'Отбрасывание', 'Отбрасывает мобов и игроков', 4, 10),
       (13, 20, 'Аспект огня', 'Поджигает цель', 2, 10),
       (14, 32, 'Эффективность', 'Более быстрая добыча ресурсов', 5, 10),
       (15, 33, 'Шёлковое касание', 'При разрушении блока из него выпадает он сам', 8, 1),
       (16, 34, 'Прочность', 'С некоторым шансом ресурс инструмента не уменьшится', 3, 5),
       (17, 35, 'Удача', 'Даёт шанс выпадения большего количества ресурсов', 20, 5),
       (18, 48, 'Сила', 'Дополнительный урон', 2, 10),
       (19, 49, 'Ударная волна', 'Отбрасывание цели', 3, 10),
       (20, 50, 'Воспламенение', 'Поджигает стрелы', 1, 10),
       (21, 51, 'Бесконечность', 'Бесконечные стрелы', 20, 1);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `fx_game_shop_enchant`
--
ALTER TABLE `fx_game_shop_enchant`
    ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `fx_game_shop_enchant`
--
ALTER TABLE `fx_game_shop_enchant`
    MODIFY `id` int (10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
