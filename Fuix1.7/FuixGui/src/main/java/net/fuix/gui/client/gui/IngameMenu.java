package net.fuix.gui.client.gui;

import net.fuix.library.client.ui.GuiComponent;
import net.fuix.library.client.ui.MouseAction;
import net.fuix.library.client.ui.Window;
import net.fuix.library.client.ui.drawable.ButtonDrawable;
import net.fuix.library.client.ui.drawable.Drawable;
import net.fuix.library.client.ui.render.ParticleEngine;
import net.fuix.library.client.ui.util.*;
import net.fuix.library.client.ui.widget.Button;
import net.fuix.library.client.ui.widget.ListView;
import net.fuix.library.client.ui.widget.ListViewItem;
import net.fuix.library.util.Timer;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.*;
import net.minecraft.client.gui.achievement.GuiAchievements;
import net.minecraft.client.resources.I18n;

import java.awt.*;

public class IngameMenu extends Window {

    private GuiIngameMenu gui;

    private Timer timer1 = new Timer();
    private ParticleEngine particleEngine = new ParticleEngine(false);

    public ListView btnList;
    public Button btnPlay;
    public Button btnExit;

    public IngameMenu(GuiIngameMenu gui) {
        super(Monitor.getWidth(), Monitor.getHeight());
        this.gui = gui;
    }

    @Override
    protected void onInit(int x, int y) {
        int width = 120;
        int height = 75;

        int posX = getWidth() / 2 - width / 2;
        int posY = (getHeight() / 2 - height / 2) + 15;

        btnList = new ListView("categoryList", new Rectangle(posX, posY, width, height), this);

        btnList.getListViewSlider().setSize(2);
        btnList.getListViewSlider().setColorSlider(new Color(0, 0, 0, 0), new Color(255, 165, 0));
        btnList.getListViewContent().setElementsPadding(0);

        btnList.setDrawable(new Drawable() {
            @Override
            public void onDraw(GuiComponent component, State state) {
                Rectangle rec = component.getAbsoluteBounds();
                DrawHelper.drawRectRounded(rec.x, rec.y, rec.width, rec.height,0.5, new java.awt.Color(255, 255, 255, 60).hashCode());
                DrawHelper.drawRectRounded(rec.x + 0.3, rec.y + 0.3, rec.width - 0.6, rec.height - 0.6, 0.5, new Color(0, 0, 0, 120).hashCode());

            }
        });

        addComponent(btnList);

        btnList.addItem(new ElementCategory("Настройки", () -> Minecraft.getMinecraft().displayGuiScreen(new GuiOptions(gui, Minecraft.getMinecraft().gameSettings)), width, 15, btnList));
        btnList.addItem(new ElementCategory("§aВеб инвентарь", () -> Minecraft.getMinecraft().thePlayer.sendChatMessage("/inventory"), width, 15, btnList));
        btnList.addItem(new ElementCategory("§6Донат меню", () -> Minecraft.getMinecraft().thePlayer.sendChatMessage("/donate"), width, 15, btnList));
        btnList.addItem(new ElementCategory("Запрещенные предметы", () -> Minecraft.getMinecraft().thePlayer.sendChatMessage("/bound"), width, 15, btnList));
        btnList.addItem(new ElementCategory(I18n.format("gui.achievements"), () -> {
            if (Minecraft.getMinecraft().thePlayer != null) Minecraft.getMinecraft().displayGuiScreen(new GuiAchievements(gui, Minecraft.getMinecraft().thePlayer.getStatFileWriter()));
        }, width, 15, btnList));

        addComponent(btnPlay = (Button) new Button("btnPlay", new Rectangle(posX, posY - (16 + 2), width, 15), this).setText(I18n.format("menu.returnToGame")).setCallback(() -> {
            Minecraft.getMinecraft().displayGuiScreen(null);
            Minecraft.getMinecraft().setIngameFocus();
        }).setDrawable(ButtonDrawable.GREEN));
        addComponent(btnExit = (Button) new Button("btnExit", new Rectangle(posX, posY + height + 3, width, 15), this).setText("Выйти в меню").setCallback(() -> {
            Minecraft.getMinecraft().theWorld.sendQuittingDisconnectingPacket();
            Minecraft.getMinecraft().loadWorld(null);
            Minecraft.getMinecraft().displayGuiScreen(new GuiMainMenu());
        }).setDrawable(ButtonDrawable.RED));
    }

    @Override
    protected void onDraw() {
        setSize(Monitor.getWidth(), Monitor.getHeight());

        particleEngine.render();
        if (timer1.hasReach(30)) {
            particleEngine.spawnParticles(0, 0, getWidth(), getHeight(), 25F, 15F);
            timer1.reset();
        }

        //DrawHelper.drawGradientRect(0, 0, getWidth(), getHeight(), -2130706433, 16777215);
        DrawHelper.drawGradientRect(0, 0, getWidth(), getHeight(), new Color(0, 0, 0, 120).hashCode(), Integer.MIN_VALUE);

        MainMenu.drawLogo(getWidth() / 2.0 - 96 / 2.0, getHeight() / 2.0 - 86, 96, 22);

        int posX = getWidth() / 2 - btnList.getBounds().width / 2;
        int posY = (getHeight() / 2 - btnList.getBounds().height / 2) + 15;

        btnList.getBounds().x = posX;
        btnList.getBounds().y = posY;

        btnPlay.getBounds().x = posX;
        btnPlay.getBounds().y = posY - (16 + 3);

        btnExit.getBounds().x = posX;
        btnExit.getBounds().y = posY +  btnList.getBounds().height + 3;
    }

    @Override
    public void onUpdate() {
        particleEngine.updateParticles();
    }


    public class ElementCategory extends ListViewItem {

        private String text = "";
        private Runnable callback = null;

        public ElementCategory(String text, Runnable callback, int width, int height, ListView listView) {
            super(width, height, listView);
            this.text = text;
            this.callback = callback;
        }

        @Override
        public void onInit(int x, int y) {
            this.setEnabled(true);
        }

        @Override
        protected void onItemDraw(int x, int y, int topLeftX, int topLeftY) {
            int posX = getAbsoluteBounds().x;
            int posY = getAbsoluteBounds().y;
            int width = getBounds().width;
            int height = getBounds().height;

            Color textColor = new Color(255, 255, 255);

            if(isFocused()) {
                DrawHelper.drawRect(posX, posY, width, height, new Color(255, 255, 255, 30).hashCode());
            }

            DrawHelper.drawRect(posX, posY + height, width, 0.3, new Color(255, 255, 255, 28).hashCode());

            FontHelper.draw(text, posX + width / 2.0, posY + height / 2.0,  textColor.hashCode(), FontHelper.Align.CENTER, 1);

        }

        @Override
        protected void onUpdate(int x, int y) { }

        @Override
        protected void handleMouse(MouseAction mouseAction) {
            if (mouseAction.getActionType().equals(MouseAction.ActionType.Release) && getAbsoluteBounds().contains(mouseAction.getPosX(), mouseAction.getPosY())) {
                if(callback != null) callback.run();
            }
        }
    }
}
