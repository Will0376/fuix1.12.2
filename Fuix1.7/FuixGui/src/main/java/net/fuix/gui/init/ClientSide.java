package net.fuix.gui.init;

import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.event.FMLConstructionEvent;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import net.fuix.gui.client.ClientEvents;
import net.fuix.library.util.ISideCommon;
import net.minecraftforge.common.MinecraftForge;

public class ClientSide implements ISideCommon {
    @Override
    public void onConstruction(FMLConstructionEvent event) { }

    @Override
    public void onInit(FMLInitializationEvent event) { }

    @Override
    public void onPreInit(FMLPreInitializationEvent event) {
        ClientEvents clientEvents = new ClientEvents();
        FMLCommonHandler.instance().bus().register(clientEvents);
        MinecraftForge.EVENT_BUS.register(clientEvents);
    }

    @Override
    public void onPostInit(FMLPostInitializationEvent event) { }
}
