package net.fuix.gui.client.gui;

import cpw.mods.fml.client.FMLClientHandler;
import net.fuix.gui.init.FuixGui;
import net.fuix.library.client.ui.GuiComponent;
import net.fuix.library.client.ui.MouseAction;
import net.fuix.library.client.ui.Window;
import net.fuix.library.client.ui.drawable.ButtonDrawable;
import net.fuix.library.client.ui.drawable.Drawable;
import net.fuix.library.client.ui.render.ParticleEngine;
import net.fuix.library.client.ui.util.*;
import net.fuix.library.client.ui.widget.Button;
import net.fuix.library.client.ui.widget.ListView;
import net.fuix.library.client.ui.widget.ListViewItem;
import net.fuix.library.util.Timer;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.*;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;

import java.awt.*;
import java.util.*;

public class MainMenu extends Window {

    private GuiMainMenu gui;
    private ResourceLocation randomBackground;

    private Timer timer1 = new Timer();
    private ParticleEngine particleEngine = new ParticleEngine(false);

    public ListView btnList;
    public Button btnPlay;
    public Button btnExit;

    public MainMenu(GuiMainMenu gui) {
        super(Monitor.getWidth(), Monitor.getHeight());
        this.gui = gui;
    }

    @Override
    protected void onInit(int x, int y) {
        randomBackground = new ResourceLocation(FuixGui.MOD_ID, "textures/gui/background/background_" + new Random().nextInt(3) + ".jpg");

        int width = 120;
        int height = 75;

        int posX = getWidth() / 2 - width / 2;
        int posY = (getHeight() / 2 - height / 2) + 15;

        btnList = new ListView("categoryList", new Rectangle(posX, posY, width, height), this);

        btnList.getListViewSlider().setSize(2);
        btnList.getListViewSlider().setColorSlider(new Color(0, 0, 0, 0), new Color(255, 165, 0));
        btnList.getListViewContent().setElementsPadding(0);

        btnList.setDrawable(new Drawable() {
            @Override
            public void onDraw(GuiComponent component, State state) {
                Rectangle rec = component.getAbsoluteBounds();
                DrawHelper.drawRectRounded(rec.x, rec.y, rec.width, rec.height,0.5, new java.awt.Color(255, 255, 255, 60).hashCode());
                DrawHelper.drawRectRounded(rec.x + 0.3, rec.y + 0.3, rec.width - 0.6, rec.height - 0.6, 0.5, new Color(0, 0, 0, 120).hashCode());

            }
        });

        addComponent(btnList);

        btnList.addItem(new ElementCategory("Одиночная игра", () -> Minecraft.getMinecraft().displayGuiScreen(new GuiSelectWorld(gui)), width, 15, btnList));
        btnList.addItem(new ElementCategory("Настройки", () -> Minecraft.getMinecraft().displayGuiScreen(new GuiOptions(gui, Minecraft.getMinecraft().gameSettings)), width, 15, btnList));
        btnList.addItem(new ElementCategory("§6Перейти на сайт", () -> SimpleHelper.openUrl("http://nostrecraft.com/"), width, 15, btnList));
        btnList.addItem(new ElementCategory("§6Сообщество в VK", () -> SimpleHelper.openUrl("https://vk.com/nostrecraft"), width, 15, btnList));
        btnList.addItem(new ElementCategory("§6Мы в Discord", () -> SimpleHelper.openUrl("https://discord.gg/GV7CBpG"), width, 15, btnList));

        addComponent(btnPlay = (Button) new Button("btnPlay", new Rectangle(posX, posY - (16 + 2), width, 15), this).setText("Продолжить игру").setCallback(() -> Minecraft.getMinecraft().displayGuiScreen(new GuiMultiplayer(gui))).setDrawable(ButtonDrawable.GREEN));
        addComponent(btnExit = (Button) new Button("btnExit", new Rectangle(posX, posY + height + 3, width, 15), this).setText("Выйти из игры").setCallback(() -> Minecraft.getMinecraft().shutdown()).setDrawable(ButtonDrawable.RED));
    }

    @Override
    protected void onDraw() {
        setSize(Monitor.getWidth(), Monitor.getHeight());

        Tessellator tessellator = Tessellator.instance;
        GL11.glDisable(2929);
        GL11.glDepthMask(false);
        OpenGlHelper.glBlendFunc(770, 771, 1, 0);
        GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
        GL11.glDisable(3008);
        FMLClientHandler.instance().getClient().renderEngine.bindTexture(randomBackground);
        tessellator.startDrawingQuads();
        tessellator.addVertexWithUV(0.0D, getHeight(), -90.0D, 0.0D, 1.0D);
        tessellator.addVertexWithUV(getWidth(), getHeight(), -90.0D, 1.0D, 1.0D);
        tessellator.addVertexWithUV(getWidth(), 0.0D, -90.0D, 1.0D, 0.0D);
        tessellator.addVertexWithUV(0.0D, 0.0D, -90.0D, 0.0D, 0.0D);
        tessellator.draw();
        GL11.glDepthMask(true);
        GL11.glEnable(2929);
        GL11.glEnable(3008);
        GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);

        particleEngine.render();
        if (timer1.hasReach(60)) {
            particleEngine.spawnParticles(0, 0, getWidth(), getHeight(), 25F, 25F);
            timer1.reset();
        }

        DrawHelper.drawGradientRect(0, 0, getWidth(), getHeight(), 0, Integer.MIN_VALUE);

        drawLogo(getWidth() / 2.0 - 96 / 2.0, getHeight() / 2.0 - 86, 96, 22);

        String text1 = "§6Права на игру принадлежат: §fMojang";
        String text2 = "§6Модификация: §fMatrix";
        String text3 = "§6Вы играете за:";
        String text4 = "§f" + Minecraft.getMinecraft().getSession().getUsername();

        FontHelper.draw(text1, getWidth() - 5, getHeight() - 14, -1, FontHelper.Align.RIGHT, 1);
        FontHelper.draw(text2, getWidth() - 5, getHeight() - 24, -1, FontHelper.Align.RIGHT, 1);

        FontHelper.draw(text3, 5, getHeight() - 24, -1, 1);
        FontHelper.draw(text4, 5, getHeight() - 14, -1, 1);

        int posX = getWidth() / 2 - btnList.getBounds().width / 2;
        int posY = (getHeight() / 2 - btnList.getBounds().height / 2) + 15;

        btnList.getBounds().x = posX;
        btnList.getBounds().y = posY;

        btnPlay.getBounds().x = posX;
        btnPlay.getBounds().y = posY - (16 + 3);

        btnExit.getBounds().x = posX;
        btnExit.getBounds().y = posY +  btnList.getBounds().height + 3;
    }

    @Override
    public void onUpdate() {
        particleEngine.updateParticles();
    }

    public static void drawLogo(double posX, double posY, double w, double h) {
        GL11.glEnable(3042);
        GL11.glDisable(GL11.GL_DEPTH_TEST);
        GL11.glDepthMask(true);
        GL11.glBlendFunc(770, 771);
        GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
        GL11.glDisable(GL11.GL_ALPHA_TEST);
        DrawHelper.draw(new ResourceLocation(FuixGui.MOD_ID, "textures/logo.png"), posX, posY, w, h);
        GL11.glDepthMask(true);
        GL11.glEnable(GL11.GL_DEPTH_TEST);
        GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
        GL11.glDisable(3042);
        GL11.glEnable(GL11.GL_ALPHA_TEST);
        GL11.glEnable(GL11.GL_DEPTH_TEST);
    }

    public class ElementCategory extends ListViewItem {

        private String text = "";
        private Runnable callback = null;

        public ElementCategory(String text, Runnable callback, int width, int height, ListView listView) {
            super(width, height, listView);
            this.text = text;
            this.callback = callback;
        }

        @Override
        public void onInit(int x, int y) {
            this.setEnabled(true);
        }

        @Override
        protected void onItemDraw(int x, int y, int topLeftX, int topLeftY) {
            int posX = getAbsoluteBounds().x;
            int posY = getAbsoluteBounds().y;
            int width = getBounds().width;
            int height = getBounds().height;

            Color textColor = new Color(255, 255, 255);

            if(isFocused()) {
                DrawHelper.drawRect(posX, posY, width, height, new Color(255, 255, 255, 30).hashCode());
            }

            DrawHelper.drawRect(posX, posY + height, width, 0.3, new Color(255, 255, 255, 28).hashCode());

            FontHelper.draw(text, posX + width / 2.0, posY + height / 2.0,  textColor.hashCode(), FontHelper.Align.CENTER, 1);

        }

        @Override
        protected void onUpdate(int x, int y) { }

        @Override
        protected void handleMouse(MouseAction mouseAction) {
            if (mouseAction.getActionType().equals(MouseAction.ActionType.Release) && getAbsoluteBounds().contains(mouseAction.getPosX(), mouseAction.getPosY())) {
                if(callback != null) callback.run();
            }
        }
    }
}
