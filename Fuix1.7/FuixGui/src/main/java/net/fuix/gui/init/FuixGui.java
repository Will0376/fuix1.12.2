package net.fuix.gui.init;

import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.*;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

import net.fuix.library.server.command.CommandManager;
import net.fuix.library.util.ISideCommon;

@Mod(modid = FuixGui.MOD_ID, name = FuixGui.MOD_NAME, version = FuixGui.MOD_VERSION, dependencies = "required-after:fuixlibrary;")
public class FuixGui {

    public static final String MOD_ID = "fuixgui";
    public static final String MOD_NAME = "FuixGui";
    public static final String MOD_VERSION = "0.1";

    @Mod.Instance
    public static FuixGui instance;

    @SideOnly(Side.SERVER)
    public static CommandManager commandManager;

    @SidedProxy(clientSide = "net.fuix.gui.init.ClientSide", serverSide = "net.fuix.gui.init.ServerSide")
    public static ISideCommon proxy;

    /** ============================================================================================================ **/

    @Mod.EventHandler
    public void onConstruction(FMLConstructionEvent event) {
        proxy.onConstruction(event);
    }

    @Mod.EventHandler
    public void onInit(FMLInitializationEvent event) { proxy.onInit(event); }

    @Mod.EventHandler
    public void onPreInit(FMLPreInitializationEvent event) { proxy.onPreInit(event); }

    @Mod.EventHandler
    public void onPostInit(FMLPostInitializationEvent event) {
        proxy.onPostInit(event);
    }

    @SideOnly(Side.SERVER)
    @Mod.EventHandler
    public void onServerStarting(FMLServerStartingEvent e) { }

    /** ============================================================================================================ **/

    public static FuixGui getInstance() { return instance; }

    @SideOnly(Side.SERVER)
    public static ServerSide getServer() { return (ServerSide) getInstance().proxy; }

    @SideOnly(Side.CLIENT)
    public static ClientSide getClient() { return (ClientSide) getInstance().proxy; }

}
