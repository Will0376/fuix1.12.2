package net.fuix.gui.client;

import cpw.mods.fml.client.FMLClientHandler;
import cpw.mods.fml.common.eventhandler.EventPriority;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.gameevent.InputEvent;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.fuix.gui.client.gui.IngameMenu;
import net.fuix.gui.client.gui.MainMenu;
import net.fuix.library.client.ui.GuiScreenAdapter;
import net.fuix.library.client.ui.util.DrawHelper;
import net.fuix.library.client.ui.util.FontHelper;
import net.fuix.library.client.ui.util.Monitor;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiIngameMenu;
import net.minecraft.client.gui.GuiMainMenu;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.client.renderer.texture.TextureUtil;
import net.minecraft.client.shader.Framebuffer;
import net.minecraft.util.Direction;
import net.minecraft.util.MathHelper;
import net.minecraft.util.ScreenShotHelper;
import net.minecraftforge.client.event.GuiOpenEvent;
import net.minecraftforge.client.event.RenderGameOverlayEvent;
import org.lwjgl.opengl.GL11;

import java.awt.*;

@SideOnly(Side.CLIENT)
public class ClientEvents {

    @SideOnly(Side.CLIENT)
    @SubscribeEvent
    public void onKeyInput(InputEvent.KeyInputEvent e) { }

    @SideOnly(Side.CLIENT)
    @SubscribeEvent
    public void onGuiOpen(GuiOpenEvent event) {
        Minecraft mc = Minecraft.getMinecraft();

        if (event.gui instanceof net.minecraft.client.gui.GuiMainMenu) event.gui = new GuiScreenAdapter(new MainMenu((GuiMainMenu) event.gui));
        if (event.gui instanceof net.minecraft.client.gui.GuiIngameMenu) event.gui = new GuiScreenAdapter(new IngameMenu((GuiIngameMenu) event.gui));
    }

    @SideOnly(Side.CLIENT)
    @SubscribeEvent(priority = EventPriority.HIGHEST)
    public void onRenderGameOverlayEvent(RenderGameOverlayEvent.Pre event) {
        Minecraft mc = Minecraft.getMinecraft();


        if (event.type == RenderGameOverlayEvent.ElementType.DEBUG) {
            event.setCanceled(true);
            mc.mcProfiler.startSection("debug");

            GL11.glPushMatrix();
            int uX = MathHelper.floor_double(mc.thePlayer.posX);
            int x = MathHelper.floor_double(mc.thePlayer.posY);
            int uZ = MathHelper.floor_double(mc.thePlayer.posZ);

            long memMax = Runtime.getRuntime().maxMemory();
            long memTotal = Runtime.getRuntime().totalMemory();
            long memFree = Runtime.getRuntime().freeMemory();
            long memUsed = memTotal - memFree;

            long memUsed1 = memUsed / 1024L / 1024L;
            long memMax1 = memMax / 1024L / 1024L;

            String line1 = "§3X:" + uX + " | Y:" + x + " | Z:" + uZ;
            String line2 = "§6Направление: ";
            String line3 = "§6" + Direction.directions[MathHelper.floor_double((double)(mc.thePlayer.rotationYaw * 4.0F / 360.0F) + 0.5D) & 3] + ", " + MathHelper.floor_double((double)(MathHelper.wrapAngleTo180_float(mc.thePlayer.rotationYaw) + 180.0F)) + ", " + (mc.thePlayer.onGround ? "На земле" : "В воздухе");
            String line4 = "§7" + mc.debug;
            String line5 = "§7" + memUsed1 + "MB из " + memMax1 + "MB";

            double debugWidth = 0;
            if(FontHelper.getWidth(line1, 1) > debugWidth) debugWidth = FontHelper.getWidth(line1, 1);
            if(FontHelper.getWidth(line2, 1) > debugWidth) debugWidth = FontHelper.getWidth(line2, 1);
            if(FontHelper.getWidth(line3, 1) > debugWidth) debugWidth = FontHelper.getWidth(line3, 1);
            if(FontHelper.getWidth(line4, 1) > debugWidth) debugWidth = FontHelper.getWidth(line4, 1);
            if(FontHelper.getWidth(line5, 1) > debugWidth) debugWidth = FontHelper.getWidth(line5, 1);

            debugWidth += 8;

            Rectangle rec = new Rectangle(Monitor.getWidth() - (int) debugWidth - 4, 5, (int) debugWidth, 57);

            DrawHelper.drawRectRounded(rec.x, rec.y, rec.width, rec.height,0.5, new java.awt.Color(255, 255, 255, 60).hashCode());
            DrawHelper.drawRectRounded(rec.x + 0.3, rec.y + 0.3, rec.width - 0.6, rec.height - 0.6, 0.5, new Color(0, 0, 0, 120).hashCode());

            FontHelper.draw(line1, rec.x + 4, rec.y + 4, 1);
            FontHelper.draw(line2, rec.x + 4, rec.y + 4 + (10 * 1), 1);
            FontHelper.draw(line3, rec.x + 4, rec.y + 4 + (10 * 2), 1);
            FontHelper.draw(line4, rec.x + 4, rec.y + 4 + (10 * 3), 1);
            FontHelper.draw(line5, rec.x + 4, rec.y + 4 + (10 * 4), 1);


            Rectangle rec1 = new Rectangle(Monitor.getWidth() - (int) debugWidth - 4, 64, (int) debugWidth, 8);

            DrawHelper.drawRectRounded(rec1.x, rec1.y, rec1.width, rec1.height,0.5, new java.awt.Color(255, 255, 255, 60).hashCode());
            DrawHelper.drawRectRounded(rec1.x + 0.3, rec1.y + 0.3, rec1.width - 0.6, rec1.height - 0.6, 0.5, new Color(0, 0, 0, 120).hashCode());

            long progress = 100 * memUsed1 / memMax1;
            long progress1 = progress * rec1.width / 100;

            Color color = new Color(123, 255, 0, 160);
            if(progress > 70) color = new Color(255, 211, 0, 160);
            if(progress > 90) color = new Color(255, 74, 13, 160);

            DrawHelper.drawRectRounded(rec1.x + 0.3, rec1.y + 0.3, progress1 - 0.6, rec1.height - 0.6, 0.5, color.hashCode());

            GL11.glPopMatrix();


            mc.mcProfiler.endSection();
        }
    }

}
