package ru.will0376.fuix;

import net.minecraftforge.fml.common.Loader;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.*;
import net.minecraftforge.fml.common.network.simpleimpl.SimpleNetworkWrapper;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.justagod.cutter.GradleSide;
import ru.justagod.cutter.GradleSideOnly;
import ru.will0376.fuix.donate.network.PacketUpdateMoney;
import ru.will0376.fuix.donate.server.ForgeVault;
import ru.will0376.fuix.donate.server.command.CommandDonate;
import ru.will0376.fuix.library.init.ClientSide;
import ru.will0376.fuix.library.init.ServerSide;
import ru.will0376.fuix.library.network.FXNetworkRegistry;
import ru.will0376.fuix.library.network.packet.PacketNotification;
import ru.will0376.fuix.library.network.simplenet.*;
import ru.will0376.fuix.library.server.command.CommandFXReload;
import ru.will0376.fuix.library.server.command.CommandManager;
import ru.will0376.fuix.library.util.ISideCommon;
import ru.will0376.fuix.shop.server.command.CommandShop;

import java.io.File;

@Mod(modid = Fuix.MOD_ID, name = Fuix.MOD_NAME, version = Fuix.VERSION)
public class Fuix {

	public static final String MOD_ID = "fuix";
	public static final String MOD_NAME = "Fuix";
	public static final String VERSION = "1.0.0";
	public static final File CFGDIR = new File(Loader.instance().getConfigDir(), "fuix");
	public static final Logger LOGGER = LogManager.getLogger("Fuix");
	@SidedProxy(clientSide = "ru.will0376.fuix.library.init.ClientSide", serverSide = "ru.will0376.fuix.library.init.ServerSide")
	public static ISideCommon proxy;
	@Mod.Instance(MOD_ID)
	public static Fuix instance;
	@SideOnly(Side.SERVER)
	@GradleSideOnly(GradleSide.SERVER)
	public static CommandManager commandManager;
	public static SimpleNetworkWrapper net = new SimpleNetworkWrapper(MOD_ID);

	@SideOnly(Side.SERVER)
	@Mod.EventHandler
	@GradleSideOnly(GradleSide.SERVER)
	public static void serverLoad(FMLServerStartingEvent event) {
		try {
			ForgeVault.setupEconomy();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static Fuix getInstance() {
		return instance;
	}

	public static ServerSide getServer() {
		return (ServerSide) proxy;
	}

	public static ClientSide getClient() {
		return (ClientSide) proxy;
	}

	@Mod.EventHandler
	public void onConstruction(FMLConstructionEvent event) {
		proxy.onConstruction(event);
	}

	@Mod.EventHandler
	public void onInit(FMLInitializationEvent event) {
		proxy.onInit(event);
	}

	@Mod.EventHandler
	public void onPreInit(FMLPreInitializationEvent event) {
		proxy.onPreInit(event);
		FXNetworkRegistry.registerPacket(PacketNotification.class, Side.CLIENT);
		FXNetworkRegistry.registerPacket(ShopGuiToServer.class, Side.SERVER);
		FXNetworkRegistry.registerPacket(ShopGuiToClient.class, Side.CLIENT);
		FXNetworkRegistry.registerPacket(PacketDonateToClient.class, Side.CLIENT);
		FXNetworkRegistry.registerPacket(PacketInventoryToClient.class, Side.CLIENT);
		FXNetworkRegistry.registerPacket(PacketInventoryToServer.class, Side.SERVER);
		FXNetworkRegistry.registerPacket(PacketUpdateMoney.class, Side.CLIENT);
	}

	@Mod.EventHandler
	public void onPostInit(FMLPostInitializationEvent event) {
		proxy.onPostInit(event);
	}

	@SideOnly(Side.SERVER)
	@Mod.EventHandler
	@GradleSideOnly(GradleSide.SERVER)
	public void onServerStarting(FMLServerStartingEvent e) {
		commandManager = new CommandManager(e);
		commandManager.registerCommandExecutor(new CommandFXReload());
		commandManager.registerCommandExecutor(new CommandShop());
		commandManager.registerCommandExecutor(new CommandDonate());

		getServer().onServerStarting(e);
	}
}
