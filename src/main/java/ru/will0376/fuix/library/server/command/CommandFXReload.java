package ru.will0376.fuix.library.server.command;

import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import ru.justagod.cutter.GradleSide;
import ru.justagod.cutter.GradleSideOnly;
import ru.will0376.fuix.library.config.ConfigUtils;
import ru.will0376.fuix.library.util.PermissionsUtils;

@SideOnly(Side.SERVER)
@GradleSideOnly(GradleSide.SERVER)
public final class CommandFXReload extends CommandExecutor {
   @CommandHandler(name = "fx_reload", group = "fuix", aliases = {})
   public void fuix(ICommandSender sender, String[] args) throws CommandExecuteException {
      if (sender instanceof EntityPlayerMP && !PermissionsUtils.hasPex((EntityPlayerMP) sender, "fuix.mod.lib.reload")) {
         sender.sendMessage(new TextComponentString(TextFormatting.RED + "# Нет прав на использование команды!"));
      } else {

         for (String o : ConfigUtils.reloadAllConfigs()) {
            sender.sendMessage(new TextComponentString(o + " config has been reloaded"));
         }

      }
   }
}
