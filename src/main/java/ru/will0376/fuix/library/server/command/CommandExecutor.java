package ru.will0376.fuix.library.server.command;

import com.google.common.primitives.Doubles;
import net.minecraft.command.NumberInvalidException;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly(Side.SERVER)
public abstract class CommandExecutor {
	public static double func_175769_b(double base, String input, int min, int max, boolean centerBlock) throws CommandExecuteException, NumberInvalidException {
		boolean var6 = input.startsWith("~");
		if (var6 && Double.isNaN(base)) {
			throw new CommandExecuteException(String.format("commands.generic.num.invalid", base));
		} else {
			double var7 = var6 ? base : 0.0D;
			if (!var6 || input.length() > 1) {
				boolean var9 = input.contains(".");
				if (var6) {
					input = input.substring(1);
				}

				var7 += parseDouble(input);
				if (!var9 && !var6 && centerBlock) {
					var7 += 0.5D;
				}
			}

			if (min != 0 || max != 0) {
				if (var7 < (double) min) {
					throw new CommandExecuteException(String.format("commands.generic.double.tooSmall", var7, min));
				}

				if (var7 > (double) max) {
					throw new CommandExecuteException(String.format("commands.generic.double.tooBig", var7, max));
				}
			}

			return var7;
		}
	}

	public static int parseInt(String input) throws NumberInvalidException {
		try {
			return Integer.parseInt(input);
		} catch (NumberFormatException var2) {
			throw new NumberInvalidException("commands.generic.num.invalid", input);
		}
	}

	public static int parseInt(String input, int min) throws NumberInvalidException {
		return parseInt(input, min, Integer.MAX_VALUE);
	}

	public static int parseInt(String input, int min, int max) throws NumberInvalidException {
		int var3 = parseInt(input);
		if (var3 < min) {
			throw new NumberInvalidException("commands.generic.num.tooSmall", var3, min);
		} else if (var3 > max) {
			throw new NumberInvalidException("commands.generic.num.tooBig", var3, max);
		} else {
			return var3;
		}
	}

	public static double parseDouble(String input) throws NumberInvalidException {
		try {
			double var1 = Double.parseDouble(input);
			if (!Doubles.isFinite(var1)) {
				throw new NumberInvalidException("commands.generic.num.invalid", input);
			} else {
				return var1;
			}
		} catch (NumberFormatException var3) {
			throw new NumberInvalidException("commands.generic.num.invalid", input);
		}
	}

	public static double parseDouble(String input, double min) throws NumberInvalidException {
		return parseDouble(input, min, Double.MAX_VALUE);
	}

	public static double parseDouble(String input, double min, double max) throws NumberInvalidException {
		double var5 = parseDouble(input);
		if (var5 < min) {
			throw new NumberInvalidException("commands.generic.double.tooSmall", var5, min);
		} else if (var5 > max) {
			throw new NumberInvalidException("commands.generic.double.tooBig", var5, max);
		} else {
			return var5;
		}
	}

	public int getInt(String argument) throws CommandExecuteException {
		try {
			return Integer.parseInt(argument);
		} catch (Exception var3) {
			throw new CommandExecuteException(String.format("Аргумент %s должен быть числом", argument));
		}
	}
}
