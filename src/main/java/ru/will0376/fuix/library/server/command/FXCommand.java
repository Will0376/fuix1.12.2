package ru.will0376.fuix.library.server.command;

import com.google.common.collect.Lists;
import net.minecraft.command.ICommand;
import net.minecraft.command.ICommandSender;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentString;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import javax.annotation.Nullable;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;

@SideOnly(Side.SERVER)
public class FXCommand implements ICommand {
	private final String name;
	private final String group;
	private final CommandExecutor handler;
	private final Method handlerMethod;
	private final HashMap<String, FXSubCommand> subCommands;
	private final String[] tabCompletion;
	private final String[] aliases;

	public FXCommand(CommandExecutor handler, Method handlerMethod, CommandHandler commandInfo, String[] tabCompletion) {
		this.handler = handler;
		this.handlerMethod = handlerMethod;
		this.subCommands = new HashMap();
		this.name = commandInfo.name();
		this.group = commandInfo.group();
		this.tabCompletion = tabCompletion;
		this.aliases = commandInfo.aliases();
	}

	public static List<String> getListOfStringsMatchingLastWord(String s1, String... strings) {
		ArrayList<String> arraylist = new ArrayList<>();
		for (String s2 : strings) {
			if (doesStringStartWith(s1, s2)) {
				arraylist.add(s2);
			}
		}

		return arraylist;
	}

	public static boolean doesStringStartWith(String p_71523_0_, String p_71523_1_) {
		return p_71523_1_.regionMatches(true, 0, p_71523_0_, 0, p_71523_0_.length());
	}

	protected void registerSubCommandHandler(String name, FXSubCommand handler) {
		this.subCommands.put(name, handler);
	}

	public String getName() {
		return this.name;
	}

	public String getUsage(ICommandSender p_71518_1_) {
		return null;
	}

	public List<String> getAliases() {
		ArrayList<String> list = Lists.newArrayList();
		list.addAll(Arrays.asList(this.aliases));
		return list;
	}

	public void execute(MinecraftServer server, ICommandSender sender, String[] args) {
		try {
			Method handlerMethod = null;
			CommandExecutor handler = null;
			if (args.length > 0 && this.subCommands.containsKey(args[0])) {
				handlerMethod = this.subCommands.get(args[0]).handlerMethod;
				handler = this.subCommands.get(args[0]).handler;
				String[] subArgs = new String[args.length - 1];
				System.arraycopy(args, 1, subArgs, 0, subArgs.length);
				args = subArgs;
			} else {
				handlerMethod = this.handlerMethod;
				handler = this.handler;
			}

			handlerMethod.invoke(handler, sender, args);
		} catch (Exception var6) {
			if (var6.getCause() instanceof CommandExecuteException) {
				sender.sendMessage(new TextComponentString("\u00a74" + var6.getCause().getMessage()));
			} else {
				var6.printStackTrace();
			}
		}

	}

	public boolean checkPermission(MinecraftServer server, ICommandSender sender) {
		return true;
	}

	public boolean isUsernameIndex(String[] p_82358_1_, int p_82358_2_) {
		return false;
	}

	public List<String> getTabCompletions(MinecraftServer server, ICommandSender sender, String[] args, @Nullable BlockPos targetPos) {
		Method handlerMethod = null;
		CommandExecutor handler = null;
		String[] tabCompletion = null;
		if (args.length > 1 && this.subCommands.containsKey(args[0])) {
			handlerMethod = this.subCommands.get(args[0]).handlerMethod;
			handler = this.subCommands.get(args[0]).handler;
			tabCompletion = this.subCommands.get(args[0]).tabCompletion;
			String[] subArgs = new String[args.length - 1];
			System.arraycopy(args, 1, subArgs, 0, subArgs.length);
			args = subArgs;
		} else {
			handlerMethod = this.handlerMethod;
			handler = this.handler;
			tabCompletion = this.tabCompletion;
		}

		ArrayList<String> completions = new ArrayList<>();
		boolean empty = args[args.length - 1].isEmpty();
		if (args.length <= tabCompletion.length) {
			String tab = tabCompletion[args.length - 1];
			String last = args[args.length - 1].toLowerCase();
			if (tab.equalsIgnoreCase("<player>")) {
				completions.addAll(getListOfStringsMatchingLastWord(last, this.getPlayers()));
			} else {
				String testValue;
				if (tab.startsWith("[") && tab.endsWith("]")) {
					try {
						Method tabHandler = handlerMethod.getDeclaringClass()
								.getMethod(tab.substring(1, tab.length() - 1), ICommandSender.class, String[].class);
						if (tabHandler.getReturnType().equals(List.class)) {
							Iterator var20 = ((List) tabHandler.invoke(handler, sender, args)).iterator();

							while (true) {
								String value;
								do {
									if (!var20.hasNext()) {
										return completions;
									}

									value = (String) var20.next();
									testValue = value.toLowerCase();
								} while (!empty && !testValue.startsWith(last));

								completions.add(value);
							}
						}
					} catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException var15) {
						var15.printStackTrace();
					}
				} else {
					String[] var10 = tab.split("\\|");
					int var11 = var10.length;

					for (String s : var10) {
						testValue = s;
						String testValue2 = testValue.toLowerCase();
						if (empty || testValue2.startsWith(last)) {
							completions.add(testValue);
						}
					}
				}
			}
		}

		return completions;
	}

	protected String[] getPlayers() {
		return FMLCommonHandler.instance()
				.getMinecraftServerInstance()
				.getPlayerList()
				.getPlayers()
				.toArray(new String[0]);
	}

	@Override
	public int compareTo(ICommand o) {
		return 0;
	}
}
