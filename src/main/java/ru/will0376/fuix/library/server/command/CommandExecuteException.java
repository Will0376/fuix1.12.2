package ru.will0376.fuix.library.server.command;


import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly(Side.SERVER)
public class CommandExecuteException extends Exception {
	private static final long serialVersionUID = -1674769027758878225L;

	public CommandExecuteException() {
	}

	public CommandExecuteException(String message) {
		super(message);
	}

	public CommandExecuteException(Throwable t) {
		super(t);
	}
}
