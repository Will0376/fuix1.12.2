package ru.will0376.fuix.library.server.command;

import net.minecraft.command.ICommandManager;
import net.minecraft.command.ICommandSender;
import net.minecraft.command.ServerCommandManager;
import net.minecraft.server.MinecraftServer;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.event.FMLServerStartingEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import ru.justagod.cutter.GradleSide;
import ru.justagod.cutter.GradleSideOnly;

import java.lang.reflect.Method;
import java.util.Arrays;

@SideOnly(Side.SERVER)
@GradleSideOnly(GradleSide.SERVER)
public class CommandManager {
   private final ICommandManager command;

   public CommandManager(FMLServerStartingEvent plugin) {
      MinecraftServer server = FMLCommonHandler.instance().getMinecraftServerInstance();
      this.command = server.getCommandManager();
   }

   private boolean registerCommand(FXCommand command) {
      ServerCommandManager serverCommand = (ServerCommandManager) this.command;
      serverCommand.registerCommand(command);
      return command != null;
   }

   private FXCommand getCommand(String patern) {
      ServerCommandManager serverCommand = (ServerCommandManager) this.command;
      FXCommand icommand = (FXCommand) serverCommand.getCommands().get(patern);
      return icommand;
   }

   public void registerCommandExecutor(CommandExecutor executor) {
      Class cls = executor.getClass();
      Method[] var3 = cls.getDeclaredMethods();
      int var4 = var3.length;

      int var5;
      Method method;
      CommandTabCompletion tabInfo;
      FXCommand parent;
      for (var5 = 0; var5 < var4; ++var5) {
         method = var3[var5];
         CommandHandler commandInfo = method.getAnnotation(CommandHandler.class);
         tabInfo = method.getAnnotation(CommandTabCompletion.class);
         if (commandInfo != null) {
            if (!method.getReturnType().equals(Void.TYPE)) {
               throw new CommandException("Incorrect return type for command method " + method.getName() + " in " + cls.getName());
            }

            if (!Arrays.equals(method.getParameterTypes(), new Class[]{ICommandSender.class, String[].class})) {
               throw new CommandException("Incorrect arguments for command method " + method.getName() + " in " + cls.getName());
            }

            if (!Arrays.equals(method.getExceptionTypes(), new Class[]{CommandExecuteException.class})) {
               throw new CommandException("Incorrect exceptions for command method " + method.getName() + " in " + cls.getName());
            }

            parent = new FXCommand(executor, method, commandInfo, tabInfo == null ? new String[0] : tabInfo.value());
            if (!this.registerCommand(parent)) {
               throw new CommandException("Failed to register command for method " + method.getName() + " in " + cls.getName());
            }
         }
      }

      var3 = cls.getDeclaredMethods();
      var4 = var3.length;

      for (var5 = 0; var5 < var4; ++var5) {
         method = var3[var5];
         SubCommandHandler subCommandInfo = method.getAnnotation(SubCommandHandler.class);
         tabInfo = method.getAnnotation(CommandTabCompletion.class);
         if (subCommandInfo != null) {
            if (!method.getReturnType().equals(Void.TYPE)) {
               throw new CommandException("Incorrect return type for command method " + method.getName() + " in " + cls.getName());
            }

            if (!Arrays.equals(method.getParameterTypes(), new Class[]{ICommandSender.class, String[].class})) {
               throw new CommandException("Incorrect arguments for command method " + method.getName() + " in " + cls.getName());
            }

            if (!Arrays.equals(method.getExceptionTypes(), new Class[]{CommandExecuteException.class})) {
               throw new CommandException("Incorrect exceptions for command method " + method.getName() + " in " + cls.getName());
            }

            parent = this.getCommand(subCommandInfo.parent());
            if (parent == null) {
               throw new CommandException("Attempted to register sub-command of " + subCommandInfo.parent() + " before main handler.");
            }

            parent.registerSubCommandHandler(subCommandInfo.name(), new FXSubCommand(executor, method, tabInfo == null ? new String[0] : tabInfo
                    .value()));
         }
      }

   }
}
