package ru.will0376.fuix.library.server.command;

import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import java.lang.reflect.Method;

@SideOnly(Side.SERVER)
public class FXSubCommand {
	public CommandExecutor handler;
	public Method handlerMethod;
	public String[] tabCompletion;

	public FXSubCommand(CommandExecutor handler, Method handlerMethod, String[] tabCompletion) {
		this.handler = handler;
		this.handlerMethod = handlerMethod;
		this.tabCompletion = tabCompletion;
	}
}
