package ru.will0376.fuix.library.server;

import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import ru.will0376.fuix.library.config.*;

@SideOnly(Side.SERVER)
@Config(name = "FuixLibrary")
public class ServerConfig {
	@ConfigBoolean(comment = "Включение DEBUG режима")
	public static boolean debug = false;
	@ConfigBoolean(comment = "Разрешить все привелегии всем")
	public static boolean debugPermissions = false;

	@ConfigInt(category = "main")
	public static int server_id = 1;

	/**
	 *
	 **/

	@ConfigString(category = "database", comment = "localhost")
	public static String address = "localhost";

	@ConfigInt(category = "database")
	public static int port = 3306;

	@ConfigString(category = "database")
	public static String username = "";

	@ConfigString(category = "database")
	public static String password = "";

	@ConfigString(category = "database")
	public static String basename = "Shop";

	/**
	 *
	 **/

	@ConfigString(category = "database_tables")
	public static String catalogTable = "fx_game_shop";

	@ConfigString(category = "database_tables")
	public static String cartTable = "fx_game_shop_cart";

	@ConfigString(category = "database_tables")
	public static String enchantTable = "fx_game_shop_enchant";

	static {
		ConfigUtils.readConfig(ServerConfig.class);
	}
}
