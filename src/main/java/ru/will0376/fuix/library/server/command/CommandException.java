package ru.will0376.fuix.library.server.command;


import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly(Side.SERVER)
public class CommandException extends RuntimeException {
	private static final long serialVersionUID = -1674769027758878225L;

	public CommandException(String message) {
		super(message);
	}
}
