package ru.will0376.fuix.library.server.data;

import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

@SideOnly(Side.SERVER)
public final class Database implements AutoCloseable {
   public static final int TIMEOUT = 5000;
   private static final int MAX_POOL_SIZE = 3;
   private final String address;
   private final int port;
   private final String username;
   private final String password;
   private final String basename;
   private DataSource source;

   public Database(String address, int port, String username, String password, String basename) {
      this.address = address;
      this.port = port;
      this.username = username;
      this.password = password;
      this.basename = basename;
   }

   public synchronized void close() {
   }

   public synchronized Connection getConnection() throws SQLException {
      if (this.source == null) {
         MysqlDataSource mysqlSource = new MysqlDataSource();
         mysqlSource.setCharacterEncoding("UTF-8");
         mysqlSource.setUseSSL(false);
         mysqlSource.setPrepStmtCacheSize(250);
         mysqlSource.setPrepStmtCacheSqlLimit(2048);
         mysqlSource.setCachePrepStmts(true);
         mysqlSource.setUseServerPrepStmts(true);
         mysqlSource.setCacheServerConfiguration(true);
         mysqlSource.setUseLocalSessionState(true);
         mysqlSource.setRewriteBatchedStatements(true);
         mysqlSource.setMaintainTimeStats(false);
         mysqlSource.setUseUnbufferedInput(false);
         mysqlSource.setUseReadAheadInput(false);
         mysqlSource.setTcpNoDelay(true);
         mysqlSource.setServerName(this.address);
         mysqlSource.setPortNumber(this.port);
         mysqlSource.setUser(this.username);
         mysqlSource.setPassword(this.password);
         mysqlSource.setDatabaseName(this.basename);
         this.source = mysqlSource;
      }

      return this.source.getConnection();
   }
}
