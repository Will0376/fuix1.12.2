package ru.will0376.fuix.library.init;

import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.event.*;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import ru.justagod.cutter.GradleSide;
import ru.justagod.cutter.GradleSideOnly;
import ru.justagod.cutter.invoke.Invoke;
import ru.will0376.fuix.Fuix;
import ru.will0376.fuix.donate.network.PacketUpdateMoney;
import ru.will0376.fuix.library.config.ConfigUtils;
import ru.will0376.fuix.library.server.ServerConfig;
import ru.will0376.fuix.library.util.ISideCommon;
import ru.will0376.fuix.library.util.Scheduler;

import java.io.IOException;


public class ServerSide implements ISideCommon {
   @GradleSideOnly(GradleSide.SERVER)
   public ru.will0376.fuix.shop.server.ServerDataHelper dataHelper;
   @GradleSideOnly(GradleSide.SERVER)
   public ru.will0376.fuix.donate.server.ServerDataHelper dbHandler;

   @SideOnly(Side.SERVER)
   @GradleSideOnly(GradleSide.SERVER)
   public static void onUpdateMoney(EntityPlayerMP entityPlayer) {
      new PacketUpdateMoney(Fuix.getServer().dbHandler.getMoney(entityPlayer)).sendTo(entityPlayer);
   }

   public void onConstruction(FMLConstructionEvent event) {
   }

   public void onInit(FMLInitializationEvent event) {
      Invoke.server(() -> {
         MinecraftForge.EVENT_BUS.register(this);
         FMLCommonHandler.instance().bus().register(this);
         dataHelper = new ru.will0376.fuix.shop.server.ServerDataHelper();
         dataHelper.onLoadCatalog();
         dataHelper.onLoadEnchant();
      });
   }

   public void onPreInit(FMLPreInitializationEvent event) {
      ConfigUtils.readConfig(ServerConfig.class);
   }

   public void onPostInit(FMLPostInitializationEvent event) {
   }

   @SideOnly(Side.SERVER)
   public void onServerStarting(FMLServerStartingEvent e) {
      Invoke.server(() -> {
         dbHandler = new ru.will0376.fuix.donate.server.ServerDataHelper();
      });
   }

   @SideOnly(Side.SERVER)
   @GradleSideOnly(GradleSide.SERVER)
   @SubscribeEvent
   public void onServerTick(TickEvent.ServerTickEvent event) throws IOException {
      if (event.phase == TickEvent.Phase.START) {
         Scheduler.server().tick();
      }
      dbHandler = new ru.will0376.fuix.donate.server.ServerDataHelper();
   }
}
