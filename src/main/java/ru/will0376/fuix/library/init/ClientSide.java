package ru.will0376.fuix.library.init;

import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.event.FMLConstructionEvent;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import ru.will0376.fuix.gui.client.ClientEvents;
import ru.will0376.fuix.library.client.ClientConfig;
import ru.will0376.fuix.library.client.event.RenderEvent;
import ru.will0376.fuix.library.client.notification.style.NotificationError;
import ru.will0376.fuix.library.client.notification.style.NotificationInfo;
import ru.will0376.fuix.library.client.notification.style.NotificationWarning;
import ru.will0376.fuix.library.config.ConfigUtils;
import ru.will0376.fuix.library.util.ISideCommon;
import ru.will0376.fuix.library.util.Scheduler;

public class ClientSide implements ISideCommon {
	private RenderEvent renderEvent;

	public void onConstruction(FMLConstructionEvent event) {
	}

	public void onInit(FMLInitializationEvent event) {
		MinecraftForge.EVENT_BUS.register(this);
		FMLCommonHandler.instance().bus().register(this);
		this.renderEvent = new RenderEvent();
		MinecraftForge.EVENT_BUS.register(this.renderEvent);
		FMLCommonHandler.instance().bus().register(this.renderEvent);
		RenderEvent.onRegisterNotificationStyle("ERROR", new NotificationError());
		RenderEvent.onRegisterNotificationStyle("INFO", new NotificationInfo());
		RenderEvent.onRegisterNotificationStyle("WARNING", new NotificationWarning());
	}

	public void onPreInit(FMLPreInitializationEvent event) {
		ConfigUtils.readConfig(ClientConfig.class);
		ClientEvents clientEvents = new ClientEvents();
		FMLCommonHandler.instance().bus().register(clientEvents);
		MinecraftForge.EVENT_BUS.register(clientEvents);
	}

	public void onPostInit(FMLPostInitializationEvent event) {
	}

	@SubscribeEvent
	@SideOnly(Side.CLIENT)
	public void onClientTickEvent(TickEvent.ClientTickEvent event) {
		if (event.phase == TickEvent.Phase.START) {
			Scheduler.client().tick();
		}

	}

	public RenderEvent getRenderEvent() {
		return this.renderEvent;
	}
}
