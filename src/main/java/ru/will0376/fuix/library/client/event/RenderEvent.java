package ru.will0376.fuix.library.client.event;

import net.minecraft.client.Minecraft;
import net.minecraftforge.client.event.RenderGameOverlayEvent.ElementType;
import net.minecraftforge.client.event.RenderGameOverlayEvent.Post;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import ru.will0376.fuix.library.client.notification.DrawNotification;
import ru.will0376.fuix.library.client.notification.Notification;
import ru.will0376.fuix.library.client.notification.style.NotificationInfo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class RenderEvent {
   private static final HashMap<String, DrawNotification> notificationStyles = new HashMap<String, DrawNotification>();
   private static final List<Notification> notification = new ArrayList<>();

   public static void onRegisterNotificationStyle(String name, DrawNotification style) {
      if (notificationStyles.containsKey(name)) {
         System.err.println(name + " уже зарегистрирован!");
      } else {
         notificationStyles.put(name, style);
      }

   }

   public static DrawNotification getNotificationStyle(String name) {
      return notificationStyles.containsKey(name) ? notificationStyles.get(name) : new NotificationInfo();
   }

   @SubscribeEvent
   @SideOnly(Side.CLIENT)
   public void onRender(Post event) {
      if (event.getType() == ElementType.ALL && Minecraft.getMinecraft().currentScreen == null && notification.size() > 0) {
         this.onNotificationDraw(notification.get(0));
      }

   }

   @SubscribeEvent
   @SideOnly(Side.CLIENT)
   public void onRenderGui(net.minecraftforge.client.event.GuiScreenEvent.DrawScreenEvent.Post event) {
      if (notification.size() > 0) {
         this.onNotificationDraw(notification.get(0));
      }

   }

   public void onNotificationDraw(Notification notification) {
      notification.getDrawNotification().onDraw(notification);
   }

   @SubscribeEvent
   @SideOnly(Side.CLIENT)
   public void onClientTickEvent(TickEvent.ClientTickEvent event) {
      if (event.phase == TickEvent.Phase.START && notification.size() > 0) {
         if ((notification.get(0)).time > 0) {
            --(notification.get(0)).time;
         } else {
            notification.remove(0);
         }
      }

   }

   public void setNotification(Notification notification) {
      if (notification.isReplace() && RenderEvent.notification.size() > 0) {
         RenderEvent.notification.set(0, notification);
      } else {
         RenderEvent.notification.add(notification);
      }

   }
}
