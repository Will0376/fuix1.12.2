package ru.will0376.fuix.library.client.ui.primitive;

public class Point3D extends Point {
	public int z;

	public Point3D(int x, int y, int z) {
		super(x, y);
		this.z = z;
	}

	public Point3D clone() {
		return new Point3D(super.x, super.y, this.z);
	}

	public Point3D add(Point3D point) {
		Point3D result = (Point3D) super.add(point);
		result.z += point.z;
		return result;
	}

	public String toString() {
		return "(" + super.x + ", " + super.y + ", " + this.z + ")";
	}

	public Point3D sub(Point3D point) {
		Point3D result = (Point3D) super.sub(point);
		result.z -= point.z;
		return result;
	}
}
