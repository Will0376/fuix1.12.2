package ru.will0376.fuix.library.client.ui.util;

import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;

public class TessellatorWrapper {
	BufferBuilder buffer;
	Tessellator tessellator;

	public TessellatorWrapper() {
		tessellator = Tessellator.getInstance();
		buffer = tessellator.getBuffer();
	}

	public static TessellatorWrapper getInstance() {
		return new TessellatorWrapper();
	}

	public void startDrawingQuads() {
		buffer.begin(7, DefaultVertexFormats.POSITION_TEX);
	}

	public void addVertex(double x, double y, double z) {
		buffer.addVertexData(new int[]{(int) x, (int) y, (int) z});
	}

	public void draw() {
		tessellator.draw();
	}

	public void addVertexWithUV(double x, double y, double z, double u, double v) {
		buffer.pos(x, y, z).tex(u, v).endVertex();
	}

	public void setColorRGBA_F(float r, float g, float b, float a) {
		buffer.color(r, g, b, a);
	}
}
