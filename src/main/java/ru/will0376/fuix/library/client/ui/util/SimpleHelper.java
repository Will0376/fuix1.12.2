package ru.will0376.fuix.library.client.ui.util;

import net.minecraft.client.model.ModelRenderer;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.MathHelper;
import net.minecraftforge.fml.client.FMLClientHandler;
import org.lwjgl.opengl.GL11;

import java.awt.*;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.util.HashMap;

public class SimpleHelper {
   private static final HashMap<String, int[]> colors = new HashMap<>();
   private static final int[] WHITE = new int[]{255, 255, 255};

   public static int[] getRGB(String rgb) {
      if (rgb == null) {
         return WHITE;
      } else {
         String[] split = rgb.split("-");
         if (rgb.length() == 13 && split.length == 2) {
            int[] c1 = getRGBColor(split[0]);
            int[] c2 = getRGBColor(split[1]);
            return colorTransition(c1, c2, 5000);
         } else {
            return getRGBColor(rgb);
         }
      }
   }

   public static int[] getRGBColor(String rgb) {
      if (rgb != null && rgb.length() == 6) {
         if (colors.containsKey(rgb)) {
            return colors.get(rgb);
         } else {
            int[] ret = new int[3];

            try {
               for (int i = 0; i < 3; ++i) {
                  ret[i] = Integer.parseInt(rgb.substring(i * 2, i * 2 + 2), 16);
               }

               colors.put(rgb, ret);
               return ret;
            } catch (Exception var3) {
               return WHITE;
            }
         }
      } else {
         return WHITE;
      }
   }

   public static int colorTransition(int color1, int color2, int range) {
      long diff = System.currentTimeMillis() % (long) range;
      int r1 = color1 >> 16 & 255;
      int g1 = color1 >> 8 & 255;
      int b1 = color1 & 255;
      int r2 = color2 >> 16 & 255;
      int g2 = color2 >> 8 & 255;
      int b2 = color2 & 255;
      int red = (int) ((1L - diff / (long) range) * (long) r1 + (long) r2 - (long) r1);
      int green = (int) ((1L - diff / (long) range) * (long) g1 + (long) g2 - (long) g1);
      int blue = (int) ((1L - diff / (long) range) * (long) b1 + (long) b2 - (long) b1);
      return -16777216 | (red & 255) << 16 | (green & 255) << 8 | (blue & 255) << 0;
   }

   public static int[] colorTransition(int[] c1, int[] c2, int range) {
      long time = System.currentTimeMillis();
      if (time / (long) range % 2L == 0L) {
         int[] temp = c1;
         c1 = c2;
         c2 = temp;
      }

      long diff = time % (long) range;
      double ratio = (double) diff / (double) range;
      int red = (int) Math.abs(ratio * (double) c1[0] + (1.0D - ratio) * (double) c2[0]);
      int green = (int) Math.abs(ratio * (double) c1[1] + (1.0D - ratio) * (double) c2[1]);
      int blue = (int) Math.abs(ratio * (double) c1[2] + (1.0D - ratio) * (double) c2[2]);
      return new int[]{red, green, blue};
   }

   public static void processMotion(long time, float range) {
      processMotion(time, 4000L, range);
   }

   public static void processMotion(long time, long interval, float range) {
      long t = time % (interval * 2L);
      float anim = (float) t / (float) interval;
      float sin = MathHelper.sin(3.14F * anim);
      GL11.glTranslatef(0.0F, sin * range, 0.0F);
   }

   public static void processRotation(long time, int range) {
      processRotation(time, range, false);
   }

   public static void processRotation(long time, int range, boolean rev) {
      long t = time % (long) range;
      float anim = (float) t / (float) range;
      if (rev) {
         anim = -anim;
      }

      GL11.glRotatef(360.0F * anim, 0.0F, 1.0F, 0.0F);
   }

   public static void processRotationZ(long time, long interval, float range) {
      long t = time % (interval * 2L);
      float anim = (float) t / (float) interval;
      float sin = MathHelper.sin(3.14F * anim);
      GL11.glRotatef(sin * range, 0.0F, 0.0F, 1.0F);
   }

   public static void processRotationX(long time, long interval, float range) {
      long t = time % (interval * 2L);
      float anim = (float) t / (float) interval;
      float sin = MathHelper.sin(3.14F * anim);
      GL11.glRotatef(sin * range, 1.0F, 0.0F, 0.0F);
   }

   public static float interpolateRotation(float p_77034_1_, float p_77034_2_, float p_77034_3_) {
      float f3;
      for (f3 = p_77034_2_ - p_77034_1_; f3 < -180.0F; f3 += 360.0F) {
      }

      while (f3 >= 180.0F) {
         f3 -= 360.0F;
      }

      return p_77034_1_ + p_77034_3_ * f3;
   }

   public static void postRender(ModelRenderer biped, float p_78794_1_) {
      if (!biped.isHidden && biped.showModel) {
         if (biped.rotateAngleX == 0.0F && biped.rotateAngleY == 0.0F && biped.rotateAngleZ == 0.0F) {
            if (biped.rotationPointX != 0.0F || biped.rotationPointY != 0.0F || biped.rotationPointZ != 0.0F) {
               GL11.glTranslatef(biped.rotationPointX * p_78794_1_, biped.rotationPointY * p_78794_1_, biped.rotationPointZ * p_78794_1_);
            }
         } else {
            GL11.glTranslatef(biped.rotationPointX * p_78794_1_, biped.rotationPointY * p_78794_1_, biped.rotationPointZ * p_78794_1_);
            if (biped.rotateAngleZ != 0.0F) {
               GL11.glRotatef(biped.rotateAngleZ * 57.295776F, 0.0F, 0.0F, 1.0F);
            }

            if (biped.rotateAngleY != 0.0F) {
               GL11.glRotatef(-biped.rotateAngleY * 57.295776F, 0.0F, 1.0F, 0.0F);
            }

            if (biped.rotateAngleX != 0.0F) {
               GL11.glRotatef(-biped.rotateAngleX * 57.295776F, 1.0F, 0.0F, 0.0F);
            }
         }
      }

   }

   public static void bindTexture(ResourceLocation loc) {
      FMLClientHandler.instance().getClient().renderEngine.bindTexture(loc);
   }

   public static double _iD(double current, double prev, float pt) {
      return prev + (current - prev) * (double) pt;
   }

   public static float _iF(float current, float prev, float pt) {
      return prev + (current - prev) * pt;
   }

   public static float[] getColor(int color) {
      float a = (float) (color >> 24 & 255) / 255.0F;
      float r = (float) (color >> 16 & 255) / 255.0F;
      float g = (float) (color >> 8 & 255) / 255.0F;
      float b = (float) (color & 255) / 255.0F;
      return new float[]{r, g, b, a};
   }

   public static int getAlpha(int color) {
      return color >> 24 & 255;
   }

   public static float getAlphaF(int color) {
      return (float) getAlpha(color) / 255.0F;
   }

   public static int setAlpha(int color, float alpha) {
      int tr = (int) ((float) (color >> 24 & 255) * alpha);
      return tr << 24 | color & 16777215;
   }

   public static int setAlpha(int color, int alpha) {
      return setAlpha(color, (float) alpha / 255.0F);
   }

   public static int adjustAlpha(int dest, float alpha) {
      int destalpha = dest >> 24 & 255;
      destalpha = (int) ((float) destalpha * alpha);
      return destalpha << 24 | dest & 16777215;
   }

   public static int adjustAlpha(int dest, int alpha) {
      return adjustAlpha(dest, (float) alpha / 255.0F);
   }

   public static void openUrl(String url) {
      try {
         Desktop d = Desktop.getDesktop();
         d.browse(new URI(String.format(url, URLEncoder.encode("WTF", "UTF8"))));
      } catch (IOException var2) {
         var2.printStackTrace();
      } catch (URISyntaxException var3) {
         var3.printStackTrace();
      }

   }

   public static float clamp(float value, float minValue, float maxValue) {
      return Math.max(minValue, Math.min(maxValue, value));
   }

   public static int clamp(int value, int minValue, int maxValue) {
      return Math.max(minValue, Math.min(maxValue, value));
   }
}
