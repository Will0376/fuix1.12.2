package ru.will0376.fuix.library.client.ui.drawable;

import ru.will0376.fuix.library.client.ui.GuiComponent;
import ru.will0376.fuix.library.client.ui.util.DrawHelper;

import java.awt.*;

public class ButtonDrawable extends Drawable {
   public static ButtonDrawable GREEN = new ButtonDrawable(new Color(141, 198, 63));
   public static ButtonDrawable BLUE = new ButtonDrawable(new Color(28, 156, 230));
   public static ButtonDrawable RED = new ButtonDrawable(new Color(202, 55, 62));
   public static ButtonDrawable ORANGE = new ButtonDrawable(new Color(246, 185, 57));
   public static ButtonDrawable PURPLE = new ButtonDrawable(new Color(131, 96, 158));
   private final Color color;

   public ButtonDrawable() {
      this.color = new Color(141, 198, 63);
   }

   public ButtonDrawable(Color color) {
      this.color = color;
   }

   public void onDraw(GuiComponent component, Drawable.State state) {
      Rectangle rec = component.getAbsoluteBounds();
      DrawHelper.drawRectRounded(rec.x, rec.y, rec.width, rec.height, 1.0D, (new Color(this.color.getRed(), this.color.getGreen(), this.color
              .getBlue(), state == Drawable.State.HOVER ? 230 : 255)).hashCode());
      DrawHelper.drawRectRounded(rec.x, (double) rec.y + 0.5D, rec.width, rec.height, 1.0D, (new Color(this.color.getRed(), this.color
              .getGreen(), this.color.getBlue(), state == Drawable.State.PRESSED ? 60 : 155)).hashCode());
      if (state == Drawable.State.DISABLE) {
         DrawHelper.drawRectRounded(rec.x, rec.y, rec.width, rec.height, 1.0D, (new Color(255, 255, 255, 100)).hashCode());
      }

   }
}
