package ru.will0376.fuix.library.client.ui.widget;

import net.minecraft.util.ResourceLocation;
import ru.will0376.fuix.library.client.ui.GuiComponent;
import ru.will0376.fuix.library.client.ui.MouseAction;
import ru.will0376.fuix.library.client.ui.Window;
import ru.will0376.fuix.library.client.ui.util.DrawHelper;

import java.awt.*;

public class ImageButton extends GuiComponent {
	private final Rectangle imageBounds;
	private final String modid;
	private final String texture;
	private Runnable callback = null;

	public ImageButton(String id, Rectangle bounds, Rectangle imageBounds, String modid, String texture, Window window) {
		super(id, bounds, window);
		this.setEnabled(true);
		this.imageBounds = imageBounds;
		this.modid = modid;
		this.texture = texture;
	}

	protected void onDraw(int x, int y) {
		double posX = (double) this.getAbsoluteBounds().x + (double) this.getBounds().width / 2.0D - (double) this.imageBounds.width / 2.0D;
		double posY = (double) this.getAbsoluteBounds().y + (double) this.getBounds().height / 2.0D - (double) this.imageBounds.height / 2.0D;
		if (this.imageBounds.getX() > 0.0D) {
			posX = (double) this.getAbsoluteBounds().x + this.imageBounds.getX();
		}

		if (this.imageBounds.getY() > 0.0D) {
			posY = (double) this.getAbsoluteBounds().y + this.imageBounds.getY();
		}

		DrawHelper.draw(new ResourceLocation(this.modid, this.texture), posX, posY, this.imageBounds.getWidth(), this.imageBounds
				.getHeight());
	}

	public void onHandleMouse(MouseAction mouseAction) {
		if (mouseAction.getActionType().equals(MouseAction.ActionType.Click) && this.getAbsoluteBounds()
				.contains(mouseAction.getPosX(), mouseAction.getPosY()) && this.callback != null) {
			this.callback.run();
		}

	}

	public Runnable getCallback() {
		return this.callback;
	}

	public ImageButton setCallback(Runnable callback) {
		this.callback = callback;
		return this;
	}

	protected void onUpdate(int x, int y) {
	}
}
