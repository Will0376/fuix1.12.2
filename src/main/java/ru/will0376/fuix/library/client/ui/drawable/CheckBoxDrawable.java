package ru.will0376.fuix.library.client.ui.drawable;

import ru.will0376.fuix.library.client.ui.GuiComponent;
import ru.will0376.fuix.library.client.ui.util.DrawHelper;

import java.awt.*;

public class CheckBoxDrawable extends Drawable {
   public void onDraw(GuiComponent component, Drawable.State state) {
      Rectangle rec = component.getAbsoluteBounds();
      if (state != Drawable.State.HOVER && state != Drawable.State.PRESSED) {
         DrawHelper.drawRectRounded(rec.x, rec.y, rec.getWidth(), rec.getHeight(), 1.0D, (new Color(255, 255, 255, 40)).hashCode());
      } else {
         DrawHelper.drawRectRounded(rec.x, rec.y, rec.getWidth(), rec.getHeight(), 1.0D, (new Color(255, 255, 255, 50)).hashCode());
      }

   }
}
