package ru.will0376.fuix.library.client;


import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import ru.will0376.fuix.library.config.Config;
import ru.will0376.fuix.library.config.ConfigBoolean;
import ru.will0376.fuix.library.config.ConfigString;
import ru.will0376.fuix.library.config.ConfigUtils;

@SideOnly(Side.CLIENT)
@Config(name = "FuixLibrary")
public class ClientConfig {
	@ConfigBoolean(comment = "Включение DEBUG режима")
	public static boolean debug = false;
	@ConfigBoolean(name = "uuid", comment = "Запрашивать скин по UUID", category = "skin")
	public static boolean skinUUID = false;
	@ConfigString(name = "url", comment = "Ссылка на скин", category = "skin")
	public static String skinURL = "http://site.ru/skins/{PLAYER}.png";

	static {
		ConfigUtils.readConfig(ClientConfig.class);
	}
}
