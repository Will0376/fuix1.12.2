package ru.will0376.fuix.library.client.ui.widget;

import ru.will0376.fuix.library.client.ui.GuiComponent;
import ru.will0376.fuix.library.client.ui.MouseAction;
import ru.will0376.fuix.library.client.ui.Window;
import ru.will0376.fuix.library.client.ui.util.FontHelper;

import java.awt.*;

public class TextView extends GuiComponent {
   public static final String LOG_TAG = "TextView";
   public static final boolean DEBUG_EXTRACT = false;
   private int fontColor = 5592405;
   private float fontSize = 1.0F;
   private FontHelper.Align align;
   private boolean isShadow;
   private String textValue;
   private Runnable callback;

   public TextView(String id, Rectangle bounds, Window window) {
      super(id, bounds, window);
      this.align = FontHelper.Align.LEFT;
      this.isShadow = false;
      this.textValue = "TextView";
      this.callback = null;
      this.setEnabled(false);
   }

   protected void onDraw(int x, int y) {
      double xPos = x + this.getBounds().x;
      if (this.align.equals(FontHelper.Align.CENTER)) {
         xPos = (double) (x + this.getBounds().x) + (double) this.getBounds().width / 2.0D;
      }

      if (this.align.equals(FontHelper.Align.RIGHT)) {
         xPos = x + this.getBounds().x + this.getBounds().width;
      }

      FontHelper.draw(this.textValue, xPos, (double) this.getAbsoluteBounds().y + (double) this.getAbsoluteBounds().height / 2.0D, this.fontColor, this.align, this.fontSize);
   }

   protected void onUpdate(int x, int y) {
   }

   public void onHandleMouse(MouseAction mouseAction) {
      if (mouseAction.getActionType().equals(MouseAction.ActionType.Click) && this.getAbsoluteBounds()
              .contains(mouseAction.getPosX(), mouseAction.getPosY()) && this.callback != null) {
         this.callback.run();
      }

   }

   public Runnable getCallback() {
      return this.callback;
   }

   public TextView setCallback(Runnable callback) {
      this.callback = callback;
      return this;
   }

   public String getText() {
      return this.textValue;
   }

   public TextView setText(String text) {
      this.textValue = text;
      return this;
   }

   public int getFontColor() {
      return this.fontColor;
   }

   public TextView setFontColor(int fontColor) {
      this.fontColor = fontColor;
      return this;
   }

   public float getFontSize() {
      return this.fontSize;
   }

   public TextView setFontSize(float fontSize) {
      this.fontSize = fontSize;
      return this;
   }

   public FontHelper.Align getAlign() {
      return this.align;
   }

   public TextView setAlign(FontHelper.Align align) {
      this.align = align;
      return this;
   }

   public boolean isShadow() {
      return this.isShadow;
   }

   public TextView setShadow(boolean shadow) {
      this.isShadow = shadow;
      return this;
   }
}
