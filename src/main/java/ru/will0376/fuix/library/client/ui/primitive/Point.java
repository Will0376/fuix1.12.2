package ru.will0376.fuix.library.client.ui.primitive;

public class Point {
	public int x;
	public int y;

	public Point(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public Point clone() {
		return new Point(this.x, this.y);
	}

	public String toString() {
		return "(" + this.x + ", " + this.y + ")";
	}

	public Point add(Point point) {
		Point result = this.clone();
		result.x += point.x;
		result.y += point.y;
		return result;
	}

	public Point sub(Point point) {
		Point result = this.clone();
		result.x -= point.x;
		result.y -= point.y;
		return result;
	}

	public void max(Point o) {
		this.x = Math.max(this.x, o.x);
		this.y = Math.max(this.y, o.y);
	}
}
