package ru.will0376.fuix.library.client.ui.util;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.client.renderer.RenderItem;
import net.minecraft.client.resources.DefaultPlayerSkin;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.client.FMLClientHandler;
import org.lwjgl.opengl.GL11;
import ru.will0376.fuix.library.client.ClientConfig;

import java.awt.*;

public class DrawHelper {
   public static RenderItem renderItem = Minecraft.getMinecraft().getRenderItem();

   public static void drawRect(double x, double y, double w, double h, int color) {
      float f3 = (float) (color >> 24 & 255) / 255.0F;
      float f = (float) (color >> 16 & 255) / 255.0F;
      float f1 = (float) (color >> 8 & 255) / 255.0F;
      float f2 = (float) (color & 255) / 255.0F;
      TessellatorWrapper tessellator = TessellatorWrapper.getInstance();
      GL11.glEnable(3042);
      GL11.glDisable(3553);
      OpenGlHelper.glBlendFunc(770, 771, 1, 0);
      GL11.glColor4f(f, f1, f2, f3);
      tessellator.startDrawingQuads();
      tessellator.addVertex(x, y + h, 0.0D);
      tessellator.addVertex(x + w, y + h, 0.0D);
      tessellator.addVertex(x + w, y, 0.0D);
      tessellator.addVertex(x, y, 0.0D);
      tessellator.draw();
      GL11.glEnable(3553);
      GL11.glDisable(3042);
   }

   public static void draw(ResourceLocation image, double x, double y, double w, double h) {
      FMLClientHandler.instance().getClient().renderEngine.bindTexture(image);
      GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
      draw(x, y, w, h);
   }

   public static void draw(double x, double y, double w, double h) {
      TessellatorWrapper tessellator = TessellatorWrapper.getInstance();
      tessellator.startDrawingQuads();
      tessellator.addVertexWithUV(x, y + h, 0.0D, 0.0D, 1.0D);
      tessellator.addVertexWithUV(x + w, y + h, 0.0D, 1.0D, 1.0D);
      tessellator.addVertexWithUV(x + w, y, 0.0D, 1.0D, 0.0D);
      tessellator.addVertexWithUV(x, y, 0.0D, 0.0D, 0.0D);
      tessellator.draw();
   }

   public static void drawRectRounded(double posX, double posY, double width, double height, double roundRaius, int color) {
      int segments = 40;
      GL11.glDisable(3553);
      GL11.glPushMatrix();
      GL11.glEnable(3042);
      GL11.glAlphaFunc(516, 0.0F);
      float f3 = (float) (color >> 24 & 255) / 255.0F;
      float f = (float) (color >> 16 & 255) / 255.0F;
      float f1 = (float) (color >> 8 & 255) / 255.0F;
      float f2 = (float) (color & 255) / 255.0F;
      GL11.glColor4f(f, f1, f2, f3);
      GL11.glBegin(9);

      for (int i = 0; i < segments; ++i) {
         double rad = -6.283185307179586D * (double) i / (double) segments;
         double x = Math.cos(rad);
         double y = Math.sin(rad);
         int stupidoCalculation = (int) ((float) i / 10.0F) + 1;
         double x1 = x * roundRaius + posX + roundRaius + width - roundRaius * 2.0D;
         double y1 = y * roundRaius + posY + roundRaius + height - roundRaius * 2.0D;
         if (stupidoCalculation == 1) {
            GL11.glVertex2d(x1, y * roundRaius + posY + roundRaius);
         }

         if (stupidoCalculation == 2) {
            GL11.glVertex2d(x * roundRaius + posX + roundRaius, y * roundRaius + posY + roundRaius);
         }

         if (stupidoCalculation == 3) {
            GL11.glVertex2d(x * roundRaius + posX + roundRaius, y1);
         }

         if (stupidoCalculation == 4) {
            GL11.glVertex2d(x1, y1);
         }
      }

      GL11.glEnd();
      GL11.glDisable(3042);
      GL11.glPopMatrix();
      GL11.glEnable(3553);
      GL11.glColor3f(1.0F, 1.0F, 1.0F);
   }

   public static void drawGradientRect(double x, double y, double w, double h, int color1, int color2) {
      float f = (float) (color1 >> 24 & 255) / 255.0F;
      float f2 = (float) (color1 >> 16 & 255) / 255.0F;
      float f3 = (float) (color1 >> 8 & 255) / 255.0F;
      float f4 = (float) (color1 & 255) / 255.0F;
      float f5 = (float) (color2 >> 24 & 255) / 255.0F;
      float f6 = (float) (color2 >> 16 & 255) / 255.0F;
      float f7 = (float) (color2 >> 8 & 255) / 255.0F;
      float f8 = (float) (color2 & 255) / 255.0F;
      GL11.glDisable(3553);
      GL11.glEnable(3042);
      GL11.glDisable(3008);
      GL11.glBlendFunc(770, 771);
      GL11.glShadeModel(7425);
      TessellatorWrapper tessellator = TessellatorWrapper.getInstance();
      tessellator.startDrawingQuads();
      tessellator.setColorRGBA_F(f2, f3, f4, f);
      tessellator.addVertex(w, y, 0.0D);
      tessellator.setColorRGBA_F(f2, f3, f4, f);
      tessellator.addVertex(x, y, 0.0D);
      tessellator.setColorRGBA_F(f6, f7, f8, f5);
      tessellator.addVertex(x, h, 0.0D);
      tessellator.setColorRGBA_F(f6, f7, f8, f5);
      tessellator.addVertex(w, h, 0.0D);
      tessellator.draw();
      GL11.glShadeModel(7424);
      GL11.glDisable(3042);
      GL11.glEnable(3008);
      GL11.glEnable(3553);
   }

   public static void drawGradientRect(double x, double y, double w, double h, int color1, int color2, int color3, int color4, double tr) {
      float a1 = (float) ((double) ((float) (color1 >> 24 & 255) / 255.0F) * tr);
      float r1 = (float) (color1 >> 16 & 255) / 255.0F;
      float g1 = (float) (color1 >> 8 & 255) / 255.0F;
      float b1 = (float) (color1 & 255) / 255.0F;
      float a2 = (float) ((double) ((float) (color2 >> 24 & 255) / 255.0F) * tr);
      float r2 = (float) (color2 >> 16 & 255) / 255.0F;
      float g2 = (float) (color2 >> 8 & 255) / 255.0F;
      float b2 = (float) (color2 & 255) / 255.0F;
      float a3 = (float) ((double) ((float) (color3 >> 24 & 255) / 255.0F) * tr);
      float r3 = (float) (color3 >> 16 & 255) / 255.0F;
      float g3 = (float) (color3 >> 8 & 255) / 255.0F;
      float b3 = (float) (color3 & 255) / 255.0F;
      float a4 = (float) ((double) ((float) (color4 >> 24 & 255) / 255.0F) * tr);
      float r4 = (float) (color4 >> 16 & 255) / 255.0F;
      float g4 = (float) (color4 >> 8 & 255) / 255.0F;
      float b4 = (float) (color4 & 255) / 255.0F;
      GL11.glDisable(3553);
      GL11.glEnable(3042);
      GL11.glDisable(3008);
      GL11.glBlendFunc(770, 771);
      GL11.glShadeModel(7425);
      TessellatorWrapper tessellator = TessellatorWrapper.getInstance();
      tessellator.startDrawingQuads();
      tessellator.setColorRGBA_F(r2, g2, b2, a2);
      tessellator.addVertex(w, y, 0.0D);
      tessellator.setColorRGBA_F(r1, g1, b1, a1);
      tessellator.addVertex(x, y, 0.0D);
      tessellator.setColorRGBA_F(r3, g3, b3, a3);
      tessellator.addVertex(x, h, 0.0D);
      tessellator.setColorRGBA_F(r4, g4, b4, a4);
      tessellator.addVertex(w, h, 0.0D);
      tessellator.draw();
      GL11.glShadeModel(7424);
      GL11.glDisable(3042);
      GL11.glEnable(3008);
      GL11.glEnable(3553);
   }

   public static void bindPlayerTexture(String player) {
      TextureHelper.downloadTexture(player, ClientConfig.skinURL.replace("{PLAYER}", player));
      TextureHelper.bindUrlTexture(player, DefaultPlayerSkin.getDefaultSkinLegacy());
   }

   public static void drawPlayerAvatar(double posX, double posY, double size, EntityPlayer entityPlayer) {
      drawPlayerAvatar(posX, posY, size, entityPlayer.getUniqueID().toString(), entityPlayer.getName());
   }

   public static void drawPlayerAvatar(double posX, double posY, double size, String uuid, String name) {
      String player = ClientConfig.skinUUID ? uuid : name;
      bindPlayerTexture(player);
      boolean b = TextureHelper.getImageHeight(player) == TextureHelper.getImageWidth(player);
      GL11.glPushMatrix();
      GL11.glEnable(3042);
      GL11.glAlphaFunc(516, 0.0F);
      GL11.glColor3f(1.0F, 1.0F, 1.0F);
      GL11.glBegin(9);
      double texturePosX = 0.125D;
      double texturePosY = 1.0D / (b ? 8.0D : 4.0D);
      double textureWidth = 0.125D;
      double textureHeight = 1.0D / (b ? 8.0D : 4.0D);
      GL11.glTexCoord2d(texturePosX + textureWidth, texturePosY);
      GL11.glVertex2d(posX + size, posY);
      GL11.glTexCoord2d(texturePosX, texturePosY);
      GL11.glVertex2d(posX, posY);
      GL11.glTexCoord2d(texturePosX, texturePosY + textureHeight);
      GL11.glVertex2d(posX, posY + size);
      GL11.glTexCoord2d(texturePosX + textureWidth, texturePosY + textureHeight);
      GL11.glVertex2d(posX + size, posY + size);
      GL11.glEnd();
      GL11.glDisable(3042);
      GL11.glPopMatrix();
      GL11.glColor3f(1.0F, 1.0F, 1.0F);
   }

   public static void scissorStart(double posX, double posY, double width, double height) {
      double MCHeight = (new ScaledResolution(Minecraft.getMinecraft())).getScaledHeight();
      double sf = (new ScaledResolution(Minecraft.getMinecraft())).getScaleFactor();
      double calcPosY = (double) Minecraft.getMinecraft().displayHeight - posY * sf;
      GL11.glEnable(3089);
      GL11.glScissor((int) (posX * sf), (int) (calcPosY - height * sf), (int) (width * sf), (int) (height * sf));
   }

   public static void scissorFinish() {
      GL11.glDisable(3089);
   }

   public static void drawItem(ItemStack itemStack, double x, double y, double size) {
      GL11.glEnable(3042);
      OpenGlHelper.glBlendFunc(770, 771, 1, 0);
      GL11.glDisable(3008);
      GL11.glPushMatrix();
      GL11.glEnable(2929);
      GL11.glTranslated(x + 3.0D, y + 3.0D, 0.0D);
      GL11.glScaled(0.0625D, 0.0625D, 1.0D);
      GL11.glScaled(size - 6.0D, size - 6.0D, 1.0D);
      GL11.glDisable(2896);
      renderItem.renderItemAndEffectIntoGUI(itemStack, 0, 0);
      renderItem.renderItemOverlayIntoGUI(Minecraft.getMinecraft().fontRenderer, itemStack, 0, 0, "");
      //TODO: Check
      GL11.glEnable(2896);
      GL11.glDisable(2929);
      GL11.glEnable(3042);
      GL11.glAlphaFunc(516, 0.0F);
      GL11.glPopMatrix();
      GL11.glDisable(2896);
      GL11.glEnable(3042);
      GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
      GL11.glDisable(2896);
      GL11.glBlendFunc(770, 771);
      GL11.glEnable(3008);
   }

   public static void drawTooltip(double posX, double posY, String[] text) {
      if (text.length > 0) {
         posY += 8.0D;
         double width = 0.0D;
         String[] var7 = text;
         int var8 = text.length;

         int i;
         for (i = 0; i < var8; ++i) {
            String s = var7[i];
            width = Math.max(FontHelper.getWidth(s, 1.0F), width);
         }

         width += 8.0D;
         double height = FontHelper.getHeight(1.0F) * (double) text.length;
         height += 4.0D;
         drawRectRounded(posX, posY, width, height, 0.5D, (new Color(255, 255, 255, 140)).hashCode());
         drawRectRounded(posX + 0.3D, posY + 0.3D, width - 0.6D, height - 0.6D, 0.5D, (new Color(0, 0, 0, 160)).hashCode());

         for (i = 0; i < text.length; ++i) {
            FontHelper.draw(text[i], posX + 4.0D, posY + 2.0D + FontHelper.getHeight(1.0F) * (double) i, i == 0 ? 16777215 : 13421772, 1.0F);
         }

      }
   }
}
