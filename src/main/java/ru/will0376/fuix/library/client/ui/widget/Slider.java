package ru.will0376.fuix.library.client.ui.widget;

import ru.will0376.fuix.library.client.ui.GuiComponent;
import ru.will0376.fuix.library.client.ui.MouseAction;
import ru.will0376.fuix.library.client.ui.Window;
import ru.will0376.fuix.library.client.ui.util.DrawHelper;
import ru.will0376.fuix.library.client.ui.util.SimpleHelper;

import java.awt.*;

public class Slider extends GuiComponent {
   public boolean isPressed;
   private Color colorLine = new Color(159, 159, 159);
   private Color colorLineSelect = new Color(69, 90, 100);
   private Color colorRegulator = new Color(255, 255, 255, 100);
   private int minValue = 0;
   private int value;
   private int maxValue;
   private float position;
   private int lineHeight;
   private int lineRound;
   private int regulatorWidth;
   private int regulatorRound;
   private ChangeListener onChange;

   public Slider(String id, Rectangle bounds, Window window) {
      super(id, bounds, window);
      this.value = this.minValue;
      this.maxValue = 10;
      this.isPressed = false;
      this.position = 0.0F;
      this.lineHeight = 2;
      this.lineRound = 1;
      this.regulatorWidth = 3;
      this.regulatorRound = 1;
      this.onChange = null;
   }

   protected void onDraw(int x, int y) {
      this.value = Math.max(this.minValue, Math.min(this.maxValue, this.value));
      DrawHelper.drawRectRounded(this.getAbsoluteBounds().x, this.getAbsoluteBounds().y + this.getBounds().height / 2 - this.lineHeight / 2, this
              .getBounds().width, this.lineHeight, this.lineRound, this.colorLine.hashCode());
      DrawHelper.drawRectRounded(this.getAbsoluteBounds().x, this.getAbsoluteBounds().y + this.getBounds().height / 2 - this.lineHeight / 2, this.position + (float) this.regulatorWidth, this.lineHeight, this.lineRound, this.colorLineSelect
              .hashCode());
      DrawHelper.drawRectRounded((float) this.getAbsoluteBounds().x + this.position, this.getAbsoluteBounds().y, this.regulatorWidth, this
              .getBounds()
              .getHeight(), this.regulatorRound, this.colorRegulator.hashCode());
   }

   protected void handleMouse(MouseAction mouseAction) {
      if (mouseAction.getActionType().equals(MouseAction.ActionType.Moved) || mouseAction.getActionType()
              .equals(MouseAction.ActionType.Click)) {
         float range = (float) (this.maxValue - this.minValue + 1);
         this.position = (float) (mouseAction.getPosX() - this.regulatorWidth - this.getAbsoluteBounds().x);
         this.position = (float) ((int) SimpleHelper.clamp(this.position, 0.0F, (float) (this.getAbsoluteBounds().width - this.regulatorWidth)));
         this.value = (int) (range / (float) (this.getBounds().width - 6) * this.position - (float) this.minValue + (float) (this.minValue * 2));
         this.value = Math.max(this.minValue, Math.min(this.maxValue, this.value));
         if (this.getChangeListener() != null) {
            this.getChangeListener().onChange(this.value);
         }
      }

      if (mouseAction.getActionType().equals(MouseAction.ActionType.Click)) {
         this.isPressed = true;
      }

      if (mouseAction.getActionType().equals(MouseAction.ActionType.Click)) {
         this.isPressed = false;
      }

   }

   public void onUpdate(int x, int y) {
   }

   public int getValue() {
      return this.value;
   }

   public Slider setRange(int min, int max) {
      this.minValue = min;
      this.maxValue = max;
      return this;
   }

   public Slider setColorLine(Color colorLine, Color colorLineSelect) {
      this.colorLine = colorLine;
      this.colorLineSelect = colorLineSelect;
      return this;
   }

   public Slider setColorRegulator(Color colorRegulator) {
      this.colorRegulator = colorRegulator;
      return this;
   }

   public Slider setSizeRegulator(int regulatorWidth, int regulatorRound) {
      this.regulatorWidth = regulatorWidth;
      this.regulatorRound = regulatorRound;
      return this;
   }

   public Slider setSizeLine(int lineHeight, int lineRound) {
      this.lineHeight = this.regulatorWidth;
      this.lineRound = lineRound;
      return this;
   }

   public Slider setClickListener(ChangeListener onChange) {
      this.onChange = onChange;
      return this;
   }

   public ChangeListener getChangeListener() {
      return this.onChange;
   }

   @FunctionalInterface
   public interface ChangeListener {
      void onChange(int var1);
   }
}
