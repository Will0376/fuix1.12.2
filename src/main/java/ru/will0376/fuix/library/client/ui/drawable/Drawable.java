package ru.will0376.fuix.library.client.ui.drawable;


import ru.will0376.fuix.library.client.ui.GuiComponent;

public class Drawable {
	public void onDraw(GuiComponent component, State state) {
	}

	public enum State {
		DEFAULT,
		HOVER,
		PRESSED,
		SELECTED,
		FOCUSED,
		DISABLE
	}
}
