package ru.will0376.fuix.library.client.notification.style;

import ru.will0376.fuix.library.client.notification.DrawNotification;
import ru.will0376.fuix.library.client.notification.Notification;
import ru.will0376.fuix.library.client.ui.util.DrawHelper;
import ru.will0376.fuix.library.client.ui.util.FontHelper;
import ru.will0376.fuix.library.client.ui.util.Monitor;

import java.awt.*;

public class NotificationInfo extends DrawNotification {
   public void onDraw(Notification notification) {
      int width = (int) FontHelper.getWidth(notification.text, 1.0F) + 10;
      Rectangle rec = new Rectangle(Monitor.getWidth() / 2 - width / 2, 4, width, 14);
      DrawHelper.drawRectRounded(rec.x, rec.y, rec.width, rec.height, 0.5D, (new Color(255, 255, 255, 80)).hashCode());
      DrawHelper.drawRectRounded((double) rec.x + 0.3D, (double) rec.y + 0.3D, (double) rec.width - 0.6D, (double) rec.height - 0.6D, 0.5D, (new Color(0, 0, 0, 120))
              .hashCode());
      FontHelper.draw(notification.text, rec.getX() + 5.0D, rec.getY() + 3.0D, (new Color(255, 255, 255)).hashCode(), 1.0F);
      Rectangle rec1 = new Rectangle(rec.x, rec.y + rec.height + 1, rec.width, 2);
      DrawHelper.drawRectRounded(rec1.x, rec1.y, rec1.width, rec1.height, 0.5D, (new Color(255, 255, 255, 80)).hashCode());
      DrawHelper.drawRectRounded((double) rec1.x + 0.3D, (double) rec1.y + 0.3D, (double) rec1.width - 0.6D, (double) rec1.height - 0.6D, 0.5D, (new Color(0, 0, 0, 120))
              .hashCode());
      long progress = 100 * notification.time / notification.maxTime;
      long progress1 = progress * (long) rec1.width / 100L;
      Color color = new Color(255, 74, 13, 160);
      if (progress > 30L) {
         color = new Color(255, 211, 0, 160);
      }

      if (progress > 70L) {
         color = new Color(123, 255, 0, 160);
      }

      DrawHelper.drawRectRounded((double) rec1.x + 0.3D, (double) rec1.y + 0.3D, (double) progress1 - 0.6D, (double) rec1.height - 0.6D, 0.5D, color
              .hashCode());
   }
}
