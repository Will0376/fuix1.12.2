package ru.will0376.fuix.library.client.ui.widget;

import ru.will0376.fuix.library.client.ui.GuiComponent;
import ru.will0376.fuix.library.client.ui.MouseAction;
import ru.will0376.fuix.library.client.ui.Window;
import ru.will0376.fuix.library.client.ui.drawable.ButtonDrawable;
import ru.will0376.fuix.library.client.ui.util.FontHelper;
import ru.will0376.fuix.library.client.ui.widget.interfaces.IScrollableComponent;

import java.awt.*;

public class NumStep extends GuiComponent implements IScrollableComponent {
   private final boolean isPressed = false;
   protected StepNumberListener onClick;
   private Button btnPlus;
   private Button btnMinus;
   private int value = 1;
   private int step = 1;
   private int max = 10;
   private int sleepUpdate = 0;

   public NumStep(String id, Rectangle bounds, Window window) {
      super(id, bounds, window);
   }

   public void onInit(int x, int y) {
      this.addComponent(this.btnPlus = (Button) (new Button(this.getId() + "#btnPlus", new Rectangle(0, 0), this.getWindow()))
              .setText("+")
              .setCallback(() -> {
                 if (NumStep.this.value + NumStep.this.step >= NumStep.this.max) {
                    NumStep.this.value = NumStep.this.max;
                 } else {
                    NumStep.this.value += NumStep.this.step;
                 }

                 if (NumStep.this.getClickListener() != null) {
                    NumStep.this.getClickListener().onStepNumber(NumStep.this.value);
                 }

              })
              .setDrawable(ButtonDrawable.PURPLE));
      this.addComponent(this.btnMinus = (Button) (new Button(this.getId() + "#btnMinus", new Rectangle(0, 0), this.getWindow()))
              .setText("-")
              .setCallback(() -> {
                 if (NumStep.this.value - NumStep.this.step <= 0) {
                    NumStep.this.value = NumStep.this.step;
                 } else {
                    NumStep.this.value -= NumStep.this.step;
                 }

                 if (NumStep.this.getClickListener() != null) {
                    NumStep.this.getClickListener().onStepNumber(NumStep.this.value);
                 }

              })
              .setDrawable(ButtonDrawable.PURPLE));
   }

   protected void onDraw(int x, int y) {
      int posX = this.getAbsoluteBounds().x;
      int posY = this.getAbsoluteBounds().y;
      int width = this.getBounds().width;
      int height = this.getBounds().height;
      FontHelper.draw(String.valueOf(this.value), (double) posX + (double) width / 2.0D, (double) posY + (double) height / 2.0D, (new Color(255, 255, 255))
              .hashCode(), FontHelper.Align.CENTER, 1.0F);
      posX = this.getBounds().x;
      posY = this.getBounds().y;
      width = this.getBounds().width;
      height = this.getBounds().height;
      this.btnPlus.setBounds(new Rectangle(posX + width - 8, posY, 8, height));
      this.btnMinus.setBounds(new Rectangle(posX, posY, 8, height));
   }

   protected void onUpdate(int x, int y) {
      if (!this.btnPlus.isPressed() && !this.btnMinus.isPressed()) {
         this.sleepUpdate = 0;
      } else {
         ++this.sleepUpdate;
      }

      if (this.btnPlus.isPressed() && this.sleepUpdate > 5) {
         this.btnPlus.getCallback().run();
      }

      if (this.btnMinus.isPressed() && this.sleepUpdate > 5) {
         this.btnMinus.getCallback().run();
      }

   }

   public void handleMouse(MouseAction mouseAction) {
   }

   public void setScrollOffset(int offset) {
      if (offset < 0) {
         this.btnMinus.getCallback().run();
      } else if (offset > 0) {
         this.btnPlus.getCallback().run();
      }

   }

   public NumStep setTuning(int step, int max) {
      this.step = step;
      this.max = max;
      return this;
   }

   public int getValue() {
      return this.value;
   }

   public NumStep setValue(int value) {
      this.value = value;
      return this;
   }

   public int getStep() {
      return this.step;
   }

   public NumStep setStep(int step) {
      this.step = step;
      return this;
   }

   public int getMax() {
      return this.max;
   }

   public NumStep setMax(int max) {
      this.max = max;
      return this;
   }

   public StepNumberListener getClickListener() {
      return this.onClick;
   }

   public NumStep setClickListener(StepNumberListener onClicked) {
      this.onClick = onClicked;
      return this;
   }

   @FunctionalInterface
   public interface StepNumberListener {
      void onStepNumber(int var1);
   }
}
