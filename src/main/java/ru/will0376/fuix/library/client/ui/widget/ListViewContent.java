package ru.will0376.fuix.library.client.ui.widget;

import ru.will0376.fuix.library.client.ui.GuiComponent;
import ru.will0376.fuix.library.client.ui.MouseAction;
import ru.will0376.fuix.library.client.ui.util.DrawHelper;
import ru.will0376.fuix.library.client.ui.util.SimpleHelper;
import ru.will0376.fuix.library.client.ui.widget.interfaces.IScrollableComponent;

import java.awt.*;

public class ListViewContent extends GuiComponent implements IScrollableComponent {
   private final ListView listView;
   private int sidePadding = 0;
   private int offset = 0;
   private boolean isPressed = false;
   private int position = 0;
   private int elementsPadding = 2;
   private Color color1 = null;
   private Color color2 = null;

   public ListViewContent(String id, ListView listView) {
      super(id, new Rectangle(0, 0, 10, 10), listView.getWindow());
      this.listView = listView;
   }

   protected void onDraw(int x, int y) {
      int posX = this.getAbsoluteBounds().x;
      int posY = this.getAbsoluteBounds().y;
      int width = this.getBounds().width;
      int height = this.getBounds().height;
      if (this.getColor1() != null) {
         DrawHelper.drawRect(posX, posY, width, (double) height + 0.3D, this.getColor1().hashCode());
      }

      if (this.getColor2() != null) {
         DrawHelper.drawRect(posX, posY, width, (double) height + 0.3D, this.getColor2().hashCode());
      }

      this.position = SimpleHelper.clamp(this.position, 0, this.getMaxContentPosition());
      int reposed = this.reposeItems(x, y);
      this.getBounds().x = this.sidePadding + this.getParent().getBounds().x;
      this.getBounds().width = this.getWindow().getWidth() - this.sidePadding * 2;
      int zeroPos = this.listView.getBounds().y;
      this.getBounds().height = reposed + this.position + this.elementsPadding * 2;
      this.getBounds().y = -this.position + zeroPos;
      if (this.getParent() != null) {
      }

      this.getBounds().width = (int) ((float) this.getParent().getBounds().width - (float) this.sidePadding * 1.5F);
      if (!this.listView.getListViewSlider().isHidden()) {
         Rectangle var10000 = this.getBounds();
         var10000.width -= 2;
      }

   }

   public void setPositionFromContent(int position) {
   }

   public void setPositionFromSlider(int pos, int maxSliderValue) {
      this.position = (int) ((float) this.getMaxContentPosition() / (float) maxSliderValue * (float) pos);
   }

   public int getMaxContentPosition() {
      return this.getBounds().height - this.listView.getBounds().height;
   }

   protected int reposeItems(int x, int y) {
      int lastPos = this.getAbsoluteBounds().y - y;
      int lastHeight = this.sidePadding;
      int maxWidth = (int) ((float) this.getBounds().width - (float) this.sidePadding / 2.0F);
      int lastWidth = 0;

      for (int i = 0; i < this.listView.size(); ++i) {
         ListViewItem item = (ListViewItem) this.listView.getItemsList().get(i);
         item.setParent(this.listView);
         int width = item.getBounds().width + this.elementsPadding;
         if (maxWidth >= lastWidth + width) {
            item.getBounds().x = this.getAbsoluteBounds().x - x + lastWidth;
            item.getBounds().y = lastPos;
            lastWidth += width;
         } else {
            item.getBounds().x = this.getAbsoluteBounds().x - x;
            item.getBounds().y = lastPos + lastHeight;
            lastWidth = 0 + width;
         }

         item.setEnabled(true);
         item.setRenderId(i);
         lastPos = item.getBounds().y;
         lastHeight = item.getBounds().height + this.elementsPadding;
         item.setVisible(item.getAbsoluteBounds().intersects(this.listView.getAbsoluteBounds()));
      }

      return lastPos + lastHeight == this.getAbsoluteBounds().y - y + this.elementsPadding ? 1 + this.getAbsoluteBounds().y - y : lastPos + lastHeight - this.elementsPadding * 2 + this.sidePadding / 2 - this.listView
              .getBounds().y;
   }

   public int getElementsPadding() {
      return this.elementsPadding;
   }

   public ListViewContent setElementsPadding(int elementsPadding) {
      this.elementsPadding = elementsPadding;
      return this;
   }

   protected void onUpdate(int x, int y) {
   }

   protected void handleMouse(MouseAction mouseAction) {
      if (mouseAction.getActionType().equals(MouseAction.ActionType.Click)) {
         this.offset = mouseAction.getPosY() - this.getAbsoluteBounds().y;
         this.isPressed = true;
      }

      if ((mouseAction.getActionType().equals(MouseAction.ActionType.Moved) || mouseAction.getActionType()
              .equals(MouseAction.ActionType.Click)) && this.isPressed) {
         this.position = mouseAction.getPosY() - this.listView.getAbsoluteBounds().y - this.offset;
         this.position = -this.position;
         this.listView.getListViewSlider().updateSliderPosition(this.position);
      }

      if (mouseAction.getActionType().equals(MouseAction.ActionType.Click)) {
         this.isPressed = false;
      }

   }

   public void setScrollOffset(int offset) {
      this.position = (int) ((float) this.position - (float) offset / 10.0F);
      this.listView.getListViewSlider().updateSliderPosition(this.position);
   }

   public int getSidePadding() {
      return this.sidePadding;
   }

   public ListViewContent setSidePadding(int sidePadding) {
      this.sidePadding = sidePadding;
      return this;
   }

   public Color getColor1() {
      return this.color1;
   }

   public void setColor1(Color color1) {
      this.color1 = color1;
   }

   public Color getColor2() {
      return this.color2;
   }

   public void setColor2(Color color2) {
      this.color2 = color2;
   }
}
