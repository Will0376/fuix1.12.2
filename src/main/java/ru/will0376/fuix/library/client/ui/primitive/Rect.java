package ru.will0376.fuix.library.client.ui.primitive;

public class Rect {
	public int left;
	public int top;
	public int right;
	public int bottom;

	public Rect(int left, int top, int right, int bottom) {
		this.left = left;
		this.top = top;
		this.right = right;
		this.bottom = bottom;
	}

	public Rect(Point leftTop, Point rightBottom) {
		this.left = leftTop.x;
		this.top = leftTop.y;
		this.right = rightBottom.x;
		this.bottom = rightBottom.y;
	}

	public Rect() {
		this(0, 0, 0, 0);
	}

	public Rect clone() {
		return new Rect(this.left, this.top, this.right, this.bottom);
	}

	public Point getSize() {
		return new Point(this.right - this.left, this.bottom - this.top);
	}

	public Point coords() {
		return new Point(this.left, this.top);
	}

	public String toString() {
		return "[(" + this.left + ", " + this.top + ") \u2014 (" + this.right + ", " + this.bottom + ")]";
	}

	public boolean contains(Point point) {
		return point.x > this.left && point.x < this.right && point.y > this.top && point.y < this.bottom;
	}

	public Rect move(Point position) {
		Rect result = this.clone();
		result.left += position.x;
		result.top += position.y;
		result.bottom += position.y;
		result.right += position.x;
		return result;
	}

	public Rect and(Rect other) {
		Rect result = new Rect(0, 0, 0, 0);
		result.left = Math.max(this.left, other.left);
		result.top = Math.max(this.top, other.top);
		result.right = Math.min(this.right, other.right);
		result.bottom = Math.min(this.bottom, other.bottom);
		return result.left <= result.right && result.top <= result.bottom ? result : null;
	}

	public void setWidth(int width) {
		this.right = this.left + width;
	}

	public void setHeight(int height) {
		this.bottom = this.top + height;
	}
}
