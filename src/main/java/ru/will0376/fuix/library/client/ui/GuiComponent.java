package ru.will0376.fuix.library.client.ui;

import ru.will0376.fuix.library.client.ui.drawable.Drawable;

import java.awt.*;
import java.util.ArrayList;

public abstract class GuiComponent {
	private final Window window;
	protected String id;
	protected String[] tooltip;
	protected boolean enabled = true;
	private Drawable drawable = null;
	private Rectangle bounds;
	private boolean visible = true;
	private GuiComponent parent;
	private boolean ignoreScissor = false;
	private boolean alwaysOnTop = false;
	private boolean isPressed = false;

	public GuiComponent(String id, Rectangle bounds, Window window) {
		this.id = id;
		this.bounds = bounds;
		this.window = window;
	}

	public String getId() {
		return this.id;
	}

	public Rectangle getBounds() {
		return this.bounds;
	}

	public GuiComponent setBounds(Rectangle bounds) {
		this.bounds = bounds;
		return this;
	}

	public void draw(int x, int y) {
		if (this.drawable != null) {
			Drawable.State state = Drawable.State.DEFAULT;
			if (!this.enabled) {
				state = Drawable.State.DISABLE;
			} else if (this.isPressed) {
				state = Drawable.State.PRESSED;
			} else if (this.isFocused()) {
				state = Drawable.State.HOVER;
			}

			this.drawable.onDraw(this, state);
		}

		this.onDraw(x, y);
	}

	protected void handleMouse(MouseAction mouseAction) {
		if (mouseAction.getActionType().equals(MouseAction.ActionType.Click)) {
			this.isPressed = true;
		}

		if (mouseAction.getActionType().equals(MouseAction.ActionType.Click)) {
			this.isPressed = false;
		}

		this.onHandleMouse(mouseAction);
	}

	protected void handleMouseAction(MouseAction mouseAction) {
	}

	protected void onHandleMouse(MouseAction mouseAction) {
	}

	protected abstract void onDraw(int var1, int var2);

	public ArrayList<GuiComponent> getChildren() {
		return this.window.getComponentContainer()
				.getChildren(this)
				.size() == 0 ? new ArrayList<>() : this.window.getComponentContainer().getChildren(this);
	}

	public boolean isVisible() {
		return (this.parent == null || this.parent.isVisible()) && this.visible;
	}

	public GuiComponent setVisible(boolean visible) {
		this.visible = visible;
		return this;
	}

	public boolean isEnabled() {
		return this.isVisible() && this.enabled;
	}

	public GuiComponent setEnabled(boolean enabled) {
		this.enabled = enabled;
		return this;
	}

	public boolean isFocused() {
		return this.window.getComponentContainer().isFocused(this);
	}

	public boolean isPressed() {
		return this.isPressed;
	}

	public GuiComponent getParent() {
		return this.parent;
	}

	public GuiComponent setParent(GuiComponent parent) {
		this.parent = parent;
		return this;
	}

	public void addComponentSingle(GuiComponent guiComponent) {
		guiComponent.setParent(this);
		this.window.getComponentContainer().addComponent(guiComponent);
	}

	public void addComponent(GuiComponent... components) {
		for (GuiComponent component : components) {
			this.addComponentSingle(component);
		}

	}

	public Rectangle getAbsoluteBounds() {
		Rectangle rectangle = new Rectangle(this.bounds);
		rectangle.setLocation(rectangle.x + this.window.getZeroPoint().x, rectangle.y + this.window.getZeroPoint().y);
		return rectangle;
	}

	public boolean hasParent() {
		return this.parent != null;
	}

	private GuiComponent getAbsParentRec(GuiComponent guiComponent) {
		return guiComponent.hasParent() ? this.getAbsParentRec(guiComponent.getParent()) : guiComponent;
	}

	public GuiComponent getAbsoluteParent() {
		return this.hasParent() ? this.getAbsParentRec(this.getParent()) : null;
	}

	public String toString() {
		return "GuiComponent (" + this.getClass().getSimpleName() + ") id = " + this.getId();
	}

	public Window getWindow() {
		return this.window;
	}

	public void update(int x, int y) {
		this.onUpdate(x, y);
	}

	public void init() {
		this.onInit(0, 0);
	}

	public String[] getToolTip() {
		return this.tooltip;
	}

	public GuiComponent setToolTip(String[] ttp) {
		this.tooltip = ttp;
		return this;
	}

	public void onInit(int x, int y) {
	}

	protected abstract void onUpdate(int var1, int var2);

	public boolean isIgnoreScissor() {
		return this.ignoreScissor;
	}

	public GuiComponent setIgnoreScissor(boolean ignoreScissor) {
		this.ignoreScissor = ignoreScissor;
		return this;
	}

	public boolean isAlwaysOnTop() {
		return this.alwaysOnTop;
	}

	public GuiComponent setAlwaysOnTop(boolean alwaysOnTop) {
		this.alwaysOnTop = alwaysOnTop;
		return this;
	}

	public Drawable getDrawable() {
		return this.drawable;
	}

	public GuiComponent setDrawable(Drawable drawable) {
		this.drawable = drawable;
		return this;
	}
}
