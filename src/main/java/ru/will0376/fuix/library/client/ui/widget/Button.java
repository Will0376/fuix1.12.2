package ru.will0376.fuix.library.client.ui.widget;

import ru.will0376.fuix.library.client.ui.Window;
import ru.will0376.fuix.library.client.ui.drawable.ButtonDrawable;
import ru.will0376.fuix.library.client.ui.util.FontHelper;

import java.awt.*;

public class Button extends TextView {
   public Button(String id, Rectangle bounds, Window window) {
      super(id, bounds, window);
      this.setEnabled(true);
      this.setFontColor(16777215);
      this.setAlign(FontHelper.Align.CENTER);
      this.setDrawable(new ButtonDrawable());
   }

   protected void onDraw(int x, int y) {
      super.onDraw(x, y);
   }

   protected void onUpdate(int x, int y) {
   }
}
