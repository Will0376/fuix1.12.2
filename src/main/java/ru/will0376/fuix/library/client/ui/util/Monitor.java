package ru.will0376.fuix.library.client.ui.util;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.ScaledResolution;

import java.awt.*;
import java.lang.reflect.Field;

public class Monitor {
   private static final ScaledResolution scaledResolution;

   static {
      scaledResolution = new ScaledResolution(Minecraft.getMinecraft());
   }

   public static int getWidth() {
      ScaledResolution scaledResolution = new ScaledResolution(Minecraft.getMinecraft());
      return (int) scaledResolution.getScaledWidth_double();
   }

   public static int getHeight() {
      ScaledResolution scaledResolution = new ScaledResolution(Minecraft.getMinecraft());
      return (int) scaledResolution.getScaledHeight_double() + 1;
   }

   public static float getDisplayScale() {
      GraphicsEnvironment env = GraphicsEnvironment.getLocalGraphicsEnvironment();
      GraphicsDevice device = env.getDefaultScreenDevice();

      try {
         Field field = device.getClass().getDeclaredField("scale");
         field.setAccessible(true);
         Object scale = field.get(device);
         if (scale instanceof Integer) {
            return (float) (Integer) scale;
         }
      } catch (Exception ignored) {
      }

      Float scaleFactor = (Float) Toolkit.getDefaultToolkit().getDesktopProperty("apple.awt.contentScaleFactor");
      return scaleFactor != null ? scaleFactor : 1.0F;
   }

   public static int scale(int x) {
      ScaledResolution r = getScaledResolution();
      return x * r.getScaleFactor();
   }

   public static ScaledResolution getScaledResolution() {
      return scaledResolution;
   }
}
