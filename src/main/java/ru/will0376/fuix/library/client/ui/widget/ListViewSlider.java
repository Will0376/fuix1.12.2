package ru.will0376.fuix.library.client.ui.widget;

import ru.will0376.fuix.library.client.ui.GuiComponent;
import ru.will0376.fuix.library.client.ui.MouseAction;
import ru.will0376.fuix.library.client.ui.util.DrawHelper;
import ru.will0376.fuix.library.client.ui.util.SimpleHelper;

import java.awt.*;

public class ListViewSlider extends GuiComponent {
   private final ListView listView;
   private int size = 3;
   private int offset = 0;
   private boolean isPressed = false;
   private boolean isHidden = false;
   private float position = 0.0F;
   private float sliderHeight = 0.0F;
   private float sliderPosition = 0.0F;
   private Color colorBgSlider = new Color(255, 255, 255, 180);
   private Color colorSlider = new Color(255, 165, 0);

   public ListViewSlider(String id, ListView listView) {
      super(id, new Rectangle(0, 0, 0, 0), listView.getWindow());
      this.listView = listView;
   }

   protected void onDraw(int x, int y) {
      if (this.getParent() != null) {
         if (this.getParent() instanceof ListView) {
            this.getBounds().setBounds(this.getParent().getBounds());
            this.getBounds().width = this.size;
            this.getBounds().x = this.getParent().getBounds().width - this.size + this.getParent().getBounds().x;
            this.sliderHeight = (float) this.getBounds().height;
            int contentHeight = this.listView.getListViewContent().getBounds().height;
            int listHeight = this.listView.getBounds().height;
            this.isHidden = contentHeight > listHeight;
            if (this.isHidden) {
               this.sliderHeight = (float) ((int) ((double) listHeight / (double) contentHeight * (double) listHeight));
               this.sliderHeight = SimpleHelper.clamp(this.sliderHeight, 2.0F, (float) this.getBounds().height);
            }

            this.sliderPosition = this.position - (float) this.offset;
            this.sliderPosition = (float) ((int) SimpleHelper.clamp(this.sliderPosition, 0.0F, (float) this.getBounds().height - this.sliderHeight));
            if (this.isHidden) {
               DrawHelper.drawRect(this.getBounds().x + x + 1, this.getBounds().y + y, this.size - 1, this.getBounds().height, this.colorBgSlider
                       .hashCode());
               DrawHelper.drawRect(this.getBounds().x + x + 1, (float) (this.getBounds().y + y) + this.sliderPosition, this.size - 1, this.sliderHeight, this.colorSlider
                       .hashCode());
            } else {
               DrawHelper.drawRect(this.getBounds().x + x + 1, this.getBounds().y + y, this.size - 1, this.getBounds().height, this.colorBgSlider
                       .hashCode());
            }

         }
      }
   }

   public boolean isHidden() {
      return !this.isHidden;
   }

   public void updateSliderPosition(int contentPos) {
      float maxSliderValue = (float) this.getAbsoluteBounds().height - this.sliderHeight;
      float maxContentValue = (float) this.listView.getListViewContent().getMaxContentPosition();
      this.position = maxSliderValue / maxContentValue * (float) contentPos + (float) this.offset;
   }

   protected void onUpdate(int x, int y) {
   }

   protected void handleMouse(MouseAction mouseAction) {
      if (mouseAction.getActionType().equals(MouseAction.ActionType.Click)) {
         Rectangle sliderBounds = new Rectangle(this.getAbsoluteBounds().x, (int) ((float) this.getAbsoluteBounds().y + this.sliderPosition), this
                 .getAbsoluteBounds().width, (int) this.sliderHeight);
         if (sliderBounds.contains(mouseAction.getPosX(), mouseAction.getPosY())) {
            this.offset = mouseAction.getPosY() - sliderBounds.y;
            this.isPressed = true;
         }
      }

      if ((mouseAction.getActionType().equals(MouseAction.ActionType.Moved) || mouseAction.getActionType()
              .equals(MouseAction.ActionType.Click)) && this.isPressed) {
         this.position = (float) (mouseAction.getPosY() - this.getAbsoluteBounds().y);
         this.position = (float) ((int) SimpleHelper.clamp(this.position, (float) this.offset, (float) this.getAbsoluteBounds().height - this.sliderHeight + (float) this.offset));
         this.listView.getListViewContent()
                 .setPositionFromSlider((int) this.position - this.offset, (int) ((float) this.getAbsoluteBounds().height - this.sliderHeight));
      }

      if (mouseAction.getActionType().equals(MouseAction.ActionType.Click)) {
         this.isPressed = false;
      }

   }

   public ListViewSlider setColorSlider(Color colorBgSlider, Color colorSlider) {
      this.colorBgSlider = colorBgSlider;
      this.colorSlider = colorSlider;
      return this;
   }

   public ListViewSlider setSize(int size) {
      this.size = size;
      return this;
   }
}
