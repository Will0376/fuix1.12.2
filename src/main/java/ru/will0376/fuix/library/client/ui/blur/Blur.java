package ru.will0376.fuix.library.client.ui.blur;

import com.google.common.base.Throwables;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiChat;
import net.minecraft.client.renderer.EntityRenderer;
import net.minecraft.client.resources.IResourcePack;
import net.minecraft.client.resources.SimpleReloadableResourceManager;
import net.minecraft.client.shader.Shader;
import net.minecraft.client.shader.ShaderGroup;
import net.minecraft.client.shader.ShaderLinkHelper;
import net.minecraft.client.shader.ShaderUniform;
import net.minecraft.client.util.JsonException;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.event.GuiOpenEvent;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent;
import net.minecraftforge.fml.relauncher.ReflectionHelper;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import org.apache.commons.lang3.ArrayUtils;

import javax.annotation.Nonnull;
import java.lang.reflect.Field;
import java.util.Iterator;
import java.util.List;

@SideOnly(Side.CLIENT)
public final class Blur {
	public static Blur instance;
	public static int radius;
	@Nonnull
	private final ShaderResourcePack dummyPack = new ShaderResourcePack();
	public String[] blurExclusions;
	public Field _listShaders;
	public long start;
	public int fadeTime;

	public Blur() {
		instance = this;
	}

	public static int getBackgroundColor(boolean second) {
		int color = second ? 75000000 : 75000000;
		int a = color >>> 24;
		int r = color >> 16 & 255;
		int b = color >> 8 & 255;
		int g = color & 255;
		float prog = instance.getProgress();
		a = (int) ((float) a * prog);
		r = (int) ((float) r * prog);
		g = (int) ((float) g * prog);
		b = (int) ((float) b * prog);
		return a << 24 | r << 16 | b << 8 | g;
	}

	public void preInit(FMLPreInitializationEvent event) {
		instance = this;
		MinecraftForge.EVENT_BUS.register(this);
		FMLCommonHandler.instance().bus().register(this);
		((List<IResourcePack>) ReflectionHelper.getPrivateValue(Minecraft.class, Minecraft.getMinecraft(), "field_110449_ao", "defaultResourcePacks"))
				.add(this.dummyPack);
		((SimpleReloadableResourceManager) Minecraft.getMinecraft()
				.getResourceManager()).registerReloadListener(this.dummyPack);
		this.saveConfig();
	}

	private void saveConfig() {
		this.blurExclusions = new String[]{GuiChat.class.getName()};
		this.fadeTime = 200;
		if (12 != Blur.radius) {
			Blur.radius = 12;
			this.dummyPack.onResourceManagerReload(Minecraft.getMinecraft().getResourceManager());
			if (Minecraft.getMinecraft().world != null) {
				Minecraft.getMinecraft().entityRenderer.stopUseShader();
			}
		}

	}

	@SubscribeEvent
	public void onGuiChange(GuiOpenEvent event) {
		try {
			if (this._listShaders == null) {
				this._listShaders = ReflectionHelper.findField(ShaderGroup.class, new String[]{"field_148031_d",
						"listShaders"});
			}

			if (Minecraft.getMinecraft().world != null && ShaderLinkHelper.getStaticShaderLinkHelper() != null) {
				EntityRenderer er = Minecraft.getMinecraft().entityRenderer;
				if (!er.isShaderActive() && event.getGui() != null && !ArrayUtils.contains(this.blurExclusions, event.getGui()
						.getClass()
						.getName())) {
					Minecraft mc = Minecraft.getMinecraft();

					try {
						er.shaderGroup = new ShaderGroup(mc.getTextureManager(), mc.getResourceManager(), mc.getFramebuffer(), new ResourceLocation("shaders/post/fade_in_blur.json"));
					} catch (JsonException var5) {
						var5.printStackTrace();
					}

					er.updateShaderGroupSize(mc.displayWidth, mc.displayHeight);
					this.start = System.currentTimeMillis();
				} else if (er.isShaderActive() && event.getGui() == null) {
					er.stopUseShader();
				}
			}
		} catch (Exception var6) {
			var6.printStackTrace();
		}

	}

	public void onViewBlur(boolean status) {
		try {
			if (this._listShaders == null) {
				this._listShaders = ReflectionHelper.findField(ShaderGroup.class, new String[]{"field_148031_d",
						"listShaders"});
			}

			if (Minecraft.getMinecraft().world != null && ShaderLinkHelper.getStaticShaderLinkHelper() != null) {
				EntityRenderer er = Minecraft.getMinecraft().entityRenderer;
				if (!er.isShaderActive() && status) {
					Minecraft mc = Minecraft.getMinecraft();

					try {
						er.shaderGroup = new ShaderGroup(mc.getTextureManager(), mc.getResourceManager(), mc.getFramebuffer(), new ResourceLocation("shaders/post/fade_in_blur.json"));
					} catch (JsonException var5) {
						var5.printStackTrace();
					}

					er.updateShaderGroupSize(mc.displayWidth, mc.displayHeight);
					this.start = System.currentTimeMillis();
				} else if (er.isShaderActive() && !status) {
					er.stopUseShader();
				}
			}
		} catch (Exception var6) {
			var6.printStackTrace();
		}

	}

	private float getProgress() {
		return Math.min((float) (System.currentTimeMillis() - this.start) / (float) this.fadeTime, 1.0F);
	}

	@SubscribeEvent
	public void onRenderTick(TickEvent.RenderTickEvent event) {
		if (event.phase == TickEvent.Phase.END && Minecraft.getMinecraft().currentScreen != null && Minecraft.getMinecraft().entityRenderer
				.isShaderActive()) {
			ShaderGroup sg = Minecraft.getMinecraft().entityRenderer.getShaderGroup();

			try {
				List e = (List) this._listShaders.get(sg);
				Iterator var4 = e.iterator();

				while (var4.hasNext()) {
					Shader s = (Shader) var4.next();
					ShaderUniform su = s.getShaderManager().getShaderUniform("Progress");
					if (su != null) {
						su.set(this.getProgress());
					}
				}
			} catch (Exception var7) {
				Throwables.propagate(var7);
			}
		}

	}
}
