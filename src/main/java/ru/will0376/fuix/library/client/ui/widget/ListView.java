package ru.will0376.fuix.library.client.ui.widget;

import ru.will0376.fuix.library.client.ui.GuiComponent;
import ru.will0376.fuix.library.client.ui.Window;
import ru.will0376.fuix.library.client.ui.util.ArrayListObserver;

import java.awt.*;
import java.util.ArrayList;
import java.util.Iterator;

public class ListView extends GuiComponent {
   private final ListViewSlider listViewSlider;
   private final ListViewContent listViewContent;
   private ArrayList listViewItems = new ArrayList();
   private ArrayListObserver arrayListObserver;
   private Callback onCallback = null;
   private int counter = 0;

   public ListView(String id, Rectangle bounds, Window window, ArrayListObserver observer) {
      super(id, bounds, window);
      if (observer != null) {
         this.arrayListObserver = observer;
         this.arrayListObserver.setListView(this);
      }

      this.listViewSlider = new ListViewSlider(this.getId() + "_slider", this);
      this.listViewContent = new ListViewContent(this.getId() + "_content", this);
   }

   public ListView(String id, Rectangle bounds, Window window) {
      super(id, bounds, window);
      this.listViewSlider = new ListViewSlider(this.getId() + "_slider", this);
      this.listViewContent = new ListViewContent(this.getId() + "_content", this);
   }

   public void onInit(int x, int y) {
      this.addComponentSingle(this.listViewSlider);
      this.addComponentSingle(this.listViewContent);
   }

   protected void onDraw(int x, int y) {
   }

   public void updateList(ArrayList arrayList) {
      if (this.arrayListObserver != null) {
         this.arrayListObserver.UpdateChanges(arrayList);
      }

   }

   protected void onUpdate(int x, int y) {
   }

   public int size() {
      return this.listViewItems.size();
   }

   public ListView addItem() {
      return this;
   }

   public ListView addItem(ListViewItem listViewItem) {
      if (!this.getWindow().getComponentContainer().hasComponent(this.listViewSlider.getId())) {
         this.addComponent(this.listViewSlider);
         this.addComponent(this.listViewContent);
      }

      listViewItem.setId(this.getId() + "_item_" + this.counter);
      listViewItem.setListView(this);
      ++this.counter;
      this.listViewItems.add(listViewItem);
      this.listViewContent.addComponentSingle(listViewItem);
      return this;
   }

   public ListView removeAll() {
      Iterator var1 = this.listViewItems.iterator();

      while (var1.hasNext()) {
         ListViewItem listViewItem = (ListViewItem) var1.next();
         this.getWindow().getComponentContainer().removeComponent(listViewItem.getId());
         this.listViewItems.remove(listViewItem);
      }

      return this;
   }

   public ListView removeItem(int id) {
      if (this.size() > 0 && id >= 0 && id <= this.size() - 1) {
         this.getWindow().getComponentContainer().removeComponent(((ListViewItem) this.listViewItems.get(id)).getId());
         this.listViewItems.remove(id);
         return this;
      } else {
         return this;
      }
   }

   public void setElementsPadding(int elementsPadding) {
      this.listViewContent.setElementsPadding(elementsPadding);
   }

   public ArrayList getItemsList() {
      return this.listViewItems;
   }

   public ListView setArrayList(ArrayList arrayList) {
      this.listViewItems = arrayList;
      return this;
   }

   public ListViewContent getListViewContent() {
      return this.listViewContent;
   }

   public ListViewSlider getListViewSlider() {
      return this.listViewSlider;
   }

   public ArrayListObserver getObserver() {
      return this.arrayListObserver;
   }

   public ListView setObserver(ArrayListObserver observer) {
      if (this.arrayListObserver == null) {
         this.arrayListObserver = observer;
         this.arrayListObserver.setListView(this);
      }

      return this;
   }

   public Callback getCallback() {
      return this.onCallback;
   }

   public ListView setCallback(Callback onCallback) {
      this.onCallback = onCallback;
      return this;
   }

   public interface Callback {
      void onUpdate(int var1, Object var2);
   }
}
