package ru.will0376.fuix.library.client.ui;

public class MouseAction {
	private final int mouseButton;
	private final int posX;
	private final int posY;
	private final ActionType actionType;
	private long HoldTime = 0L;

	public MouseAction(int mouseButton, int posX, int posY, ActionType actionType) {
		this.mouseButton = mouseButton;
		this.posX = posX;
		this.posY = posY;
		this.actionType = actionType;
	}

	public int getMouseButton() {
		return this.mouseButton;
	}

	public int getPosX() {
		return this.posX;
	}

	public int getPosY() {
		return this.posY;
	}

	public ActionType getActionType() {
		return this.actionType;
	}

	public long getHoldTime() {
		return this.HoldTime;
	}

	public MouseAction setHoldTime(long holdTime) {
		this.HoldTime = holdTime;
		return this;
	}

	public enum ActionType {
		Click,
		Moved,
		Release
	}
}
