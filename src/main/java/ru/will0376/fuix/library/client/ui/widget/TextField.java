package ru.will0376.fuix.library.client.ui.widget;

import net.minecraft.client.Minecraft;
import ru.will0376.fuix.library.client.ui.Window;
import ru.will0376.fuix.library.client.ui.*;
import ru.will0376.fuix.library.client.ui.drawable.TextFieldDrawable;
import ru.will0376.fuix.library.client.ui.util.FontHelper;
import ru.will0376.fuix.library.client.ui.widget.interfaces.IKeyboardReader;

import java.awt.*;

public class TextField extends GuiComponent implements IKeyboardReader {
   private final GuiTextField guiTextField;
   private final int textHintColor = 16777215;
   private String hintText = "";
   private boolean isChanged = false;
   private int textColor = 16777215;
   private int paddingHorizontal = 4;
   private boolean onlyNumbers = false;

   public TextField(String id, Rectangle bounds, Window window) {
      super(id, bounds, window);
      int var10004 = bounds.x + this.paddingHorizontal;
      int var10006 = bounds.width - this.paddingHorizontal * 2;
      this.guiTextField = new GuiTextField(Minecraft.getMinecraft().fontRenderer, var10004, bounds.y, var10006, bounds.height);
      this.guiTextField.setText("");
      this.guiTextField.setEnabled(true);
      this.guiTextField.setVisible(true);
      this.guiTextField.setEnableBackgroundDrawing(false);
      this.guiTextField.setMaxStringLength(100);
      this.setDrawable(new TextFieldDrawable());
   }

   public void handleKeyboard(KeyboardAction action) {
      if (!action.getActionType().equals(KeyboardAction.KeyActionType.Release)) {
         if (this.isOnlyNumbers()) {
            if (!Character.isLetter(action.getChar())) {
               this.guiTextField.textboxKeyTyped(action.getChar(), action.getKeyCode());
            }
         } else {
            this.guiTextField.textboxKeyTyped(action.getChar(), action.getKeyCode());
         }

         this.isChanged = true;
      }
   }

   protected void onDraw(int x, int y) {
      int width = this.getBounds().width;
      int height = this.getBounds().height;
      this.guiTextField.xPosition = x + this.getBounds().x + this.paddingHorizontal;
      this.guiTextField.yPosition = y + this.getBounds().y + height / 2 - Minecraft.getMinecraft().fontRenderer.FONT_HEIGHT / 2;
      this.guiTextField.width = width - this.paddingHorizontal * 2;
      this.guiTextField.height = height;
      if (this.guiTextField.getText().isEmpty() && this.getWindow()
              .getComponentContainer()
              .getKeyboardFocusedElement() != this) {
         FontHelper.draw(this.hintText, this.guiTextField.xPosition, this.guiTextField.yPosition, this.textHintColor, 1.0F);
      }

      this.guiTextField.setTextColor(this.textColor);
      this.guiTextField.drawTextBox();
      this.guiTextField.setFocused(this.getWindow().getComponentContainer().getKeyboardFocusedElement() == this);
   }

   protected void onUpdate(int x, int y) {
      this.guiTextField.updateCursorCounter();
   }

   protected void handleMouse(MouseAction mouseAction) {
      if (mouseAction.getActionType().equals(MouseAction.ActionType.Click)) {
         this.guiTextField.mouseClicked(mouseAction.getPosX(), mouseAction.getPosY(), 0);
      }

   }

   public boolean getIsChanged() {
      if (this.isChanged) {
         this.isChanged = false;
         return true;
      } else {
         return false;
      }
   }

   public String getText() {
      return this.guiTextField.getText();
   }

   public TextField setText(String text) {
      this.guiTextField.setText(text);
      return this;
   }

   public TextField setTextColor(int color) {
      this.guiTextField.setTextColor(color);
      return this;
   }

   public TextField setHintTextColor(int color) {
      this.textColor = color;
      return this;
   }

   public String getHintText() {
      return this.hintText;
   }

   public TextField setHintText(String hintText) {
      this.hintText = hintText;
      return this;
   }

   public TextField setMaxStringLength(int length) {
      this.guiTextField.setMaxStringLength(length);
      return this;
   }

   public TextField setPaddingHorizontal(int paddingHorizontal) {
      this.paddingHorizontal = paddingHorizontal;
      return this;
   }

   public boolean isOnlyNumbers() {
      return this.onlyNumbers;
   }

   public TextField setOnlyNumbers(boolean onlyNumbers) {
      this.onlyNumbers = onlyNumbers;
      return this;
   }
}
