package ru.will0376.fuix.library.client.ui.util;

import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import org.lwjgl.opengl.GL11;

import java.nio.FloatBuffer;

@SideOnly(Side.CLIENT)
public class GlStateManager {
	private static final AlphaState alphaState = new AlphaState();
	private static final BooleanState lightingState = new BooleanState(2896);
	private static final BooleanState[] lightState = new BooleanState[8];
	private static final ColorMaterialState colorMaterialState = new ColorMaterialState();
	private static final BlendState blendState = new BlendState();
	private static final DepthState depthState = new DepthState();
	private static final FogState fogState = new FogState();
	private static final CullState cullState = new CullState();
	private static final PolygonOffsetState polygonOffsetState = new PolygonOffsetState();
	private static final ColorLogicState colorLogicState = new ColorLogicState();
	private static final TexGenState texGenState = new TexGenState();
	private static final ClearState clearState = new ClearState();
	private static final StencilState stencilState = new StencilState();
	private static final BooleanState normalizeState = new BooleanState(2977);
	private static final TextureState[] textureState = new TextureState[8];
	private static final BooleanState rescaleNormalState = new BooleanState(32826);
	private static final ColorMask colorMaskState = new ColorMask();
	private static final Color colorState = new Color();
	private static int activeTextureUnit = 0;
	private static int activeShadeModel = 7425;

	static {
		int j;
		for (j = 0; j < 8; ++j) {
			lightState[j] = new BooleanState(16384 + j);
		}

		for (j = 0; j < 8; ++j) {
			textureState[j] = new TextureState();
		}

	}

	public static void pushAttrib() {
		GL11.glPushAttrib(8256);
	}

	public static void popAttrib() {
		GL11.glPopAttrib();
	}

	public static void disableAlpha() {
		alphaState.alphaTest.setDisabled();
	}

	public static void enableAlpha() {
		alphaState.alphaTest.setEnabled();
	}

	public static void alphaFunc(int func, float ref) {
		if (func != alphaState.func || ref != alphaState.ref) {
			alphaState.func = func;
			alphaState.ref = ref;
			GL11.glAlphaFunc(func, ref);
		}

	}

	public static void enableLighting() {
		lightingState.setEnabled();
	}

	public static void disableLighting() {
		lightingState.setDisabled();
	}

	public static void enableLight(int light) {
		lightState[light].setEnabled();
	}

	public static void disableLight(int light) {
		lightState[light].setDisabled();
	}

	public static void enableColorMaterial() {
		colorMaterialState.colorMaterial.setEnabled();
	}

	public static void disableColorMaterial() {
		colorMaterialState.colorMaterial.setDisabled();
	}

	public static void colorMaterial(int face, int mode) {
		if (face != colorMaterialState.face || mode != colorMaterialState.mode) {
			colorMaterialState.face = face;
			colorMaterialState.mode = mode;
			GL11.glColorMaterial(face, mode);
		}

	}

	public static void disableDepth() {
		depthState.depthTest.setDisabled();
	}

	public static void enableDepth() {
		depthState.depthTest.setEnabled();
	}

	public static void depthFunc(int depthFunc) {
		if (depthFunc != depthState.depthFunc) {
			depthState.depthFunc = depthFunc;
			GL11.glDepthFunc(depthFunc);
		}

	}

	public static void depthMask(boolean flagIn) {
		if (flagIn != depthState.maskEnabled) {
			depthState.maskEnabled = flagIn;
			GL11.glDepthMask(flagIn);
		}

	}

	public static void disableBlend() {
		blendState.blend.setDisabled();
	}

	public static void enableBlend() {
		blendState.blend.setEnabled();
	}

	public static void blendFunc(int srcFactor, int dstFactor) {
		if (srcFactor != blendState.srcFactor || dstFactor != blendState.dstFactor) {
			blendState.srcFactor = srcFactor;
			blendState.dstFactor = dstFactor;
			GL11.glBlendFunc(srcFactor, dstFactor);
		}

	}

	public static void tryBlendFuncSeparate(int srcFactor, int dstFactor, int srcFactorAlpha, int dstFactorAlpha) {
		if (srcFactor != blendState.srcFactor || dstFactor != blendState.dstFactor || srcFactorAlpha != blendState.srcFactorAlpha || dstFactorAlpha != blendState.dstFactorAlpha) {
			blendState.srcFactor = srcFactor;
			blendState.dstFactor = dstFactor;
			blendState.srcFactorAlpha = srcFactorAlpha;
			blendState.dstFactorAlpha = dstFactorAlpha;
			OpenGlHelper.glBlendFunc(srcFactor, dstFactor, srcFactorAlpha, dstFactorAlpha);
		}

	}

	public static void enableFog() {
		fogState.fog.setEnabled();
	}

	public static void disableFog() {
		fogState.fog.setDisabled();
	}

	public static void setFog(int param) {
		if (param != fogState.mode) {
			fogState.mode = param;
			GL11.glFogi(2917, param);
		}

	}

	public static void setFogDensity(float param) {
		if (param != fogState.density) {
			fogState.density = param;
			GL11.glFogf(2914, param);
		}

	}

	public static void setFogStart(float param) {
		if (param != fogState.start) {
			fogState.start = param;
			GL11.glFogf(2915, param);
		}

	}

	public static void setFogEnd(float param) {
		if (param != fogState.end) {
			fogState.end = param;
			GL11.glFogf(2916, param);
		}

	}

	public static void enableCull() {
		cullState.cullFace.setEnabled();
	}

	public static void disableCull() {
		cullState.cullFace.setDisabled();
	}

	public static void cullFace(int mode) {
		if (mode != cullState.mode) {
			cullState.mode = mode;
			GL11.glCullFace(mode);
		}

	}

	public static void enablePolygonOffset() {
		polygonOffsetState.polygonOffsetFill.setEnabled();
	}

	public static void disablePolygonOffset() {
		polygonOffsetState.polygonOffsetFill.setDisabled();
	}

	public static void doPolygonOffset(float factor, float units) {
		if (factor != polygonOffsetState.factor || units != polygonOffsetState.units) {
			polygonOffsetState.factor = factor;
			polygonOffsetState.units = units;
			GL11.glPolygonOffset(factor, units);
		}

	}

	public static void enableColorLogic() {
		colorLogicState.colorLogicOp.setEnabled();
	}

	public static void disableColorLogic() {
		colorLogicState.colorLogicOp.setDisabled();
	}

	public static void colorLogicOp(int opcode) {
		if (opcode != colorLogicState.opcode) {
			colorLogicState.opcode = opcode;
			GL11.glLogicOp(opcode);
		}

	}

	public static void enableTexGenCoord(TexGen p_179087_0_) {
		texGenCoord(p_179087_0_).textureGen.setEnabled();
	}

	public static void disableTexGenCoord(TexGen p_179100_0_) {
		texGenCoord(p_179100_0_).textureGen.setDisabled();
	}

	public static void texGen(TexGen texGen, int param) {
		TexGenCoord glstatemanager$texgencoord = texGenCoord(texGen);
		if (param != glstatemanager$texgencoord.param) {
			glstatemanager$texgencoord.param = param;
			GL11.glTexGeni(glstatemanager$texgencoord.coord, 9472, param);
		}

	}

	public static void texGen(TexGen p_179105_0_, int pname, FloatBuffer params) {
		GL11.glTexGen(texGenCoord(p_179105_0_).coord, pname, params);
	}

	private static TexGenCoord texGenCoord(TexGen p_179125_0_) {
		switch (p_179125_0_) {
			case S:
				return texGenState.s;
			case T:
				return texGenState.t;
			case R:
				return texGenState.r;
			case Q:
				return texGenState.q;
			default:
				return texGenState.s;
		}
	}

	public static void setActiveTexture(int texture) {
		if (activeTextureUnit != texture - OpenGlHelper.defaultTexUnit) {
			activeTextureUnit = texture - OpenGlHelper.defaultTexUnit;
			OpenGlHelper.setActiveTexture(texture);
		}

	}

	public static void enableTexture2D() {
		textureState[activeTextureUnit].texture2DState.setEnabled();
	}

	public static void disableTexture2D() {
		textureState[activeTextureUnit].texture2DState.setDisabled();
	}

	public static int generateTexture() {
		return GL11.glGenTextures();
	}

	public static void deleteTexture(int texture) {
		GL11.glDeleteTextures(texture);
		TextureState[] var1 = textureState;
		int var2 = var1.length;

		for (int var3 = 0; var3 < var2; ++var3) {
			TextureState glstatemanager$texturestate = var1[var3];
			if (glstatemanager$texturestate.textureName == texture) {
				glstatemanager$texturestate.textureName = -1;
			}
		}

	}

	public static void bindTexture(int texture) {
		if (texture != textureState[activeTextureUnit].textureName) {
			textureState[activeTextureUnit].textureName = texture;
			GL11.glBindTexture(3553, texture);
		}

	}

	public static void enableNormalize() {
		normalizeState.setEnabled();
	}

	public static void disableNormalize() {
		normalizeState.setDisabled();
	}

	public static void shadeModel(int mode) {
		if (mode != activeShadeModel) {
			activeShadeModel = mode;
			GL11.glShadeModel(mode);
		}

	}

	public static void enableRescaleNormal() {
		rescaleNormalState.setEnabled();
	}

	public static void disableRescaleNormal() {
		rescaleNormalState.setDisabled();
	}

	public static void viewport(int x, int y, int width, int height) {
		GL11.glViewport(x, y, width, height);
	}

	public static void colorMask(boolean red, boolean green, boolean blue, boolean alpha) {
		if (red != colorMaskState.red || green != colorMaskState.green || blue != colorMaskState.blue || alpha != colorMaskState.alpha) {
			colorMaskState.red = red;
			colorMaskState.green = green;
			colorMaskState.blue = blue;
			colorMaskState.alpha = alpha;
			GL11.glColorMask(red, green, blue, alpha);
		}

	}

	public static void clearDepth(double depth) {
		if (depth != clearState.depth) {
			clearState.depth = depth;
			GL11.glClearDepth(depth);
		}

	}

	public static void clearColor(float red, float green, float blue, float alpha) {
		if (red != clearState.color.red || green != clearState.color.green || blue != clearState.color.blue || alpha != clearState.color.alpha) {
			clearState.color.red = red;
			clearState.color.green = green;
			clearState.color.blue = blue;
			clearState.color.alpha = alpha;
			GL11.glClearColor(red, green, blue, alpha);
		}

	}

	public static void clear(int mask) {
		GL11.glClear(mask);
	}

	public static void matrixMode(int mode) {
		GL11.glMatrixMode(mode);
	}

	public static void loadIdentity() {
		GL11.glLoadIdentity();
	}

	public static void pushMatrix() {
		GL11.glPushMatrix();
	}

	public static void popMatrix() {
		GL11.glPopMatrix();
	}

	public static void getFloat(int pname, FloatBuffer params) {
		GL11.glGetFloat(pname, params);
	}

	public static void ortho(double left, double right, double bottom, double top, double zNear, double zFar) {
		GL11.glOrtho(left, right, bottom, top, zNear, zFar);
	}

	public static void rotate(float angle, float x, float y, float z) {
		GL11.glRotatef(angle, x, y, z);
	}

	public static void scale(float x, float y, float z) {
		GL11.glScalef(x, y, z);
	}

	public static void scale(double x, double y, double z) {
		GL11.glScaled(x, y, z);
	}

	public static void translate(float x, float y, float z) {
		GL11.glTranslatef(x, y, z);
	}

	public static void translate(double x, double y, double z) {
		GL11.glTranslated(x, y, z);
	}

	public static void multMatrix(FloatBuffer matrix) {
		GL11.glMultMatrix(matrix);
	}

	public static void color(float colorRed, float colorGreen, float colorBlue, float colorAlpha) {
		if (colorRed != colorState.red || colorGreen != colorState.green || colorBlue != colorState.blue || colorAlpha != colorState.alpha) {
			colorState.red = colorRed;
			colorState.green = colorGreen;
			colorState.blue = colorBlue;
			colorState.alpha = colorAlpha;
			GL11.glColor4f(colorRed, colorGreen, colorBlue, colorAlpha);
		}

	}

	public static void color(float colorRed, float colorGreen, float colorBlue) {
		color(colorRed, colorGreen, colorBlue, 1.0F);
	}

	public static void resetColor() {
		colorState.red = colorState.green = colorState.blue = colorState.alpha = -1.0F;
	}

	public static void callList(int list) {
		GL11.glCallList(list);
	}

	@SideOnly(Side.CLIENT)
	public enum TexGen {
		S,
		T,
		R,
		Q
	}

	@SideOnly(Side.CLIENT)
	static class TextureState {
		public BooleanState texture2DState;
		public int textureName;

		private TextureState() {
			this.texture2DState = new BooleanState(3553);
			this.textureName = 0;
		}

		// $FF: synthetic method
		TextureState(Object x0) {
			this();
		}
	}

	@SideOnly(Side.CLIENT)
	static class TexGenState {
		public TexGenCoord s;
		public TexGenCoord t;
		public TexGenCoord r;
		public TexGenCoord q;

		private TexGenState() {
			this.s = new TexGenCoord(8192, 3168);
			this.t = new TexGenCoord(8193, 3169);
			this.r = new TexGenCoord(8194, 3170);
			this.q = new TexGenCoord(8195, 3171);
		}

		// $FF: synthetic method
		TexGenState(Object x0) {
			this();
		}
	}

	@SideOnly(Side.CLIENT)
	static class TexGenCoord {
		public BooleanState textureGen;
		public int coord;
		public int param = -1;

		public TexGenCoord(int p_i46254_1_, int p_i46254_2_) {
			this.coord = p_i46254_1_;
			this.textureGen = new BooleanState(p_i46254_2_);
		}
	}

	@SideOnly(Side.CLIENT)
	static class StencilState {
		public StencilFunc field_179078_a;
		public int field_179076_b;
		public int field_179077_c;
		public int field_179074_d;
		public int field_179075_e;

		private StencilState() {
			this.field_179078_a = new StencilFunc();
			this.field_179076_b = -1;
			this.field_179077_c = 7680;
			this.field_179074_d = 7680;
			this.field_179075_e = 7680;
		}

		// $FF: synthetic method
		StencilState(Object x0) {
			this();
		}
	}

	@SideOnly(Side.CLIENT)
	static class StencilFunc {
		public int field_179081_a;
		public int field_179079_b;
		public int field_179080_c;

		private StencilFunc() {
			this.field_179081_a = 519;
			this.field_179079_b = 0;
			this.field_179080_c = -1;
		}

		// $FF: synthetic method
		StencilFunc(Object x0) {
			this();
		}
	}

	@SideOnly(Side.CLIENT)
	static class PolygonOffsetState {
		public BooleanState polygonOffsetFill;
		public BooleanState polygonOffsetLine;
		public float factor;
		public float units;

		private PolygonOffsetState() {
			this.polygonOffsetFill = new BooleanState(32823);
			this.polygonOffsetLine = new BooleanState(10754);
			this.factor = 0.0F;
			this.units = 0.0F;
		}

		// $FF: synthetic method
		PolygonOffsetState(Object x0) {
			this();
		}
	}

	@SideOnly(Side.CLIENT)
	static class FogState {
		public BooleanState fog;
		public int mode;
		public float density;
		public float start;
		public float end;

		private FogState() {
			this.fog = new BooleanState(2912);
			this.mode = 2048;
			this.density = 1.0F;
			this.start = 0.0F;
			this.end = 1.0F;
		}

		// $FF: synthetic method
		FogState(Object x0) {
			this();
		}
	}

	@SideOnly(Side.CLIENT)
	static class DepthState {
		public BooleanState depthTest;
		public boolean maskEnabled;
		public int depthFunc;

		private DepthState() {
			this.depthTest = new BooleanState(2929);
			this.maskEnabled = true;
			this.depthFunc = 513;
		}

		// $FF: synthetic method
		DepthState(Object x0) {
			this();
		}
	}

	@SideOnly(Side.CLIENT)
	static class CullState {
		public BooleanState cullFace;
		public int mode;

		private CullState() {
			this.cullFace = new BooleanState(2884);
			this.mode = 1029;
		}

		// $FF: synthetic method
		CullState(Object x0) {
			this();
		}
	}

	@SideOnly(Side.CLIENT)
	static class ColorMaterialState {
		public BooleanState colorMaterial;
		public int face;
		public int mode;

		private ColorMaterialState() {
			this.colorMaterial = new BooleanState(2903);
			this.face = 1032;
			this.mode = 5634;
		}

		// $FF: synthetic method
		ColorMaterialState(Object x0) {
			this();
		}
	}

	@SideOnly(Side.CLIENT)
	static class ColorMask {
		public boolean red;
		public boolean green;
		public boolean blue;
		public boolean alpha;

		private ColorMask() {
			this.red = true;
			this.green = true;
			this.blue = true;
			this.alpha = true;
		}

		// $FF: synthetic method
		ColorMask(Object x0) {
			this();
		}
	}

	@SideOnly(Side.CLIENT)
	static class ColorLogicState {
		public BooleanState colorLogicOp;
		public int opcode;

		private ColorLogicState() {
			this.colorLogicOp = new BooleanState(3058);
			this.opcode = 5379;
		}

		// $FF: synthetic method
		ColorLogicState(Object x0) {
			this();
		}
	}

	@SideOnly(Side.CLIENT)
	static class Color {
		public float red = 1.0F;
		public float green = 1.0F;
		public float blue = 1.0F;
		public float alpha = 1.0F;

		public Color() {
		}

		public Color(float redIn, float greenIn, float blueIn, float alphaIn) {
			this.red = redIn;
			this.green = greenIn;
			this.blue = blueIn;
			this.alpha = alphaIn;
		}
	}

	@SideOnly(Side.CLIENT)
	static class ClearState {
		public double depth;
		public Color color;
		public int field_179204_c;

		private ClearState() {
			this.depth = 1.0D;
			this.color = new Color(0.0F, 0.0F, 0.0F, 0.0F);
			this.field_179204_c = 0;
		}

		// $FF: synthetic method
		ClearState(Object x0) {
			this();
		}
	}

	@SideOnly(Side.CLIENT)
	static class BooleanState {
		private final int capability;
		private boolean currentState = false;

		public BooleanState(int capabilityIn) {
			this.capability = capabilityIn;
		}

		public void setDisabled() {
			this.setState(false);
		}

		public void setEnabled() {
			this.setState(true);
		}

		public void setState(boolean state) {
			if (state != this.currentState) {
				this.currentState = state;
				if (state) {
					GL11.glEnable(this.capability);
				} else {
					GL11.glDisable(this.capability);
				}
			}

		}
	}

	@SideOnly(Side.CLIENT)
	static class BlendState {
		public BooleanState blend;
		public int srcFactor;
		public int dstFactor;
		public int srcFactorAlpha;
		public int dstFactorAlpha;

		private BlendState() {
			this.blend = new BooleanState(3042);
			this.srcFactor = 1;
			this.dstFactor = 0;
			this.srcFactorAlpha = 1;
			this.dstFactorAlpha = 0;
		}

		// $FF: synthetic method
		BlendState(Object x0) {
			this();
		}
	}

	@SideOnly(Side.CLIENT)
	static class AlphaState {
		public BooleanState alphaTest;
		public int func;
		public float ref;

		private AlphaState() {
			this.alphaTest = new BooleanState(3008);
			this.func = 519;
			this.ref = -1.0F;
		}

		// $FF: synthetic method
		AlphaState(Object x0) {
			this();
		}
	}
}
