package ru.will0376.fuix.library.client.ui;

import ru.will0376.fuix.library.client.ui.util.Monitor;

import java.awt.*;

public abstract class Window {
   private final ComponentContainer componentContainer;
   private int width;
   private int height;
   private ModalWindow modalWindow = null;

   public Window(int width, int height) {
      this.width = width;
      this.height = height;
      this.componentContainer = new ComponentContainer(this);
   }

   public void onViewModal(ModalWindow modalWindow) {
      this.modalWindow = modalWindow;
      if (this.modalWindow != null) {
         this.modalWindow.init();
      }

   }

   public void onHideModals() {
      this.modalWindow = null;
   }

   public ModalWindow getModalWindow() {
      return this.modalWindow;
   }

   public void onHandleMouse(MouseAction mouseAction) {
   }

   public boolean isViewModal() {
      return this.modalWindow != null;
   }

   public ComponentContainer getComponentContainer() {
      return this.componentContainer;
   }

   protected void addComponent(GuiComponent component) {
      component.setParent(null);
      this.componentContainer.addComponent(component);
   }

   protected GuiComponent getComponent(String id) {
      return this.componentContainer.getComponentById(id);
   }

   public void init() {
      this.onInit(this.getZeroPoint().x, this.getZeroPoint().y);
      this.componentContainer.init();
   }

   public void draw(float partialTicks) {
      this.onDraw();
      this.componentContainer.draw();
   }

   public void update() {
      this.componentContainer.update();
      this.onUpdate();
   }

   public Point getCenter() {
      return new Point(Monitor.getWidth() / 2, Monitor.getHeight() / 2);
   }

   public Point getZeroPoint() {
      return new Point(this.getCenter().x - this.width / 2, this.getCenter().y - this.height / 2);
   }

   public int getWidth() {
      return this.width;
   }

   public int getHeight() {
      return this.height;
   }

   public Window setSize(int width, int height) {
      this.width = width;
      this.height = height;
      return this;
   }

   protected abstract void onInit(int var1, int var2);

   protected abstract void onDraw();

   protected void onUpdate() {
   }

   public Rectangle getAbsoluteBounds() {
      Rectangle rectangle = new Rectangle(this.width, this.height);
      rectangle.setLocation(this.getZeroPoint().x, this.getZeroPoint().y);
      return rectangle;
   }
}
