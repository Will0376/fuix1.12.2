package ru.will0376.fuix.library.client.ui.util;

import com.google.common.collect.Maps;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.texture.DynamicTexture;
import net.minecraft.util.ResourceLocation;
import org.apache.commons.io.FileUtils;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.vector.Vector2f;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.atomic.AtomicInteger;

public class TextureHelper {
   private static final AtomicInteger threadDownloadCounter = new AtomicInteger(0);
   private static final Map images = Maps.newHashMap();
   private static final Map textureMap = Maps.newHashMap();
   private static final Map imageSizes = Maps.newHashMap();
   private static int toLoadCount = 0;

   public static void bindUrlTexture(String name) {
      bindUrlTexture(name, new ResourceLocation("fuixlibrary", "textures/gui/blank.png"));
   }

   public static void bindUrlTexture(String name, ResourceLocation blank) {
      if (textureMap.get(name) != null) {
         GL11.glBindTexture(3553, ((DynamicTexture) textureMap.get(name)).getGlTextureId());
      } else if (images.get(name) != null) {
         try {
            BufferedImage read = ImageIO.read((File) images.get(name));
            textureMap.put(name, new DynamicTexture(read));
            int width = read.getWidth();
            int height = read.getHeight();
            String filename = ((File) images.get(name)).getName();
            if (((File) images.get(name)).delete()) {
               System.out.println("File " + filename + " deleted!");
            }

            images.remove(name);
            Vector2f vec2 = new Vector2f((float) width, (float) height);
            imageSizes.put(name, vec2);
            System.out.println(vec2);
            System.out.println("Texture with key \"" + name + " \" loaded! Texture width: " + width + ", height: " + height);
         } catch (Exception var7) {
            var7.printStackTrace();
         }
      } else {
         Minecraft.getMinecraft().renderEngine.bindTexture(blank);
      }

   }

   public static int getImageWidth(String name) {
      return imageSizes.get(name) != null ? (int) ((Vector2f) imageSizes.get(name)).getX() : 6;
   }

   public static int getImageHeight(String name) {
      return imageSizes.get(name) != null ? (int) ((Vector2f) imageSizes.get(name)).getY() : 1;
   }

   public static void bindTexture(String name) {
      Minecraft.getMinecraft().renderEngine.bindTexture(new ResourceLocation("fuixlibrary", ""));
   }

   public static void clearTextureBuffer() {
      Iterator var0 = images.keySet().iterator();

      while (var0.hasNext()) {
         String key = (String) var0.next();
         ((File) images.get(key)).delete();
      }

      textureMap.clear();
      images.clear();
      toLoadCount = 0;
   }

   public static void update() throws Exception {
      Iterator var0 = images.entrySet().iterator();

      while (var0.hasNext()) {
         Entry entry = (Entry) var0.next();
         String name = (String) entry.getKey();
         if (textureMap.get(name) == null && images.get(name) != null) {
            try {
               System.out.println(name);
               textureMap.put(name, new DynamicTexture(ImageIO.read((File) images.get(name))));
               System.out.println(((File) images.get(name)).delete());
               images.remove(name);
            } catch (IOException var4) {
               System.out.println("GOVNOKOD PRIVETSTVUET TEBYA! :D");
            }
         }
      }

   }

   public static void downloadTexture(final String name, final String imageURL) {
      ++toLoadCount;
      Thread imageThread = new Thread("Texture Downloader #" + threadDownloadCounter.incrementAndGet()) {
         public void run() {
            HttpURLConnection httpurlconnection = null;
            if (!TextureHelper.textureMap.containsKey(name)) {
               try {
                  httpurlconnection = (HttpURLConnection) (new URL(imageURL)).openConnection();
                  httpurlconnection.setDoInput(true);
                  httpurlconnection.setDoOutput(false);
                  httpurlconnection.connect();
                  File f = File.createTempFile("urltex_", ".png");
                  FileUtils.copyInputStreamToFile(httpurlconnection.getInputStream(), f);
                  TextureHelper.images.put(name, f);
               } catch (Exception var3) {
               }
            }

         }
      };
      imageThread.setDaemon(true);
      imageThread.start();
   }

   public static int getLoadedPercents() {
      int pre = 0;

      for (Iterator var1 = textureMap.keySet().iterator(); var1.hasNext(); ++pre) {
         String key = (String) var1.next();
      }

      return (int) (100.0D / (double) toLoadCount * (double) pre);
   }
}
