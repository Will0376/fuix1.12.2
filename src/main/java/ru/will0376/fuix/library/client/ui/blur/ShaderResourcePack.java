package ru.will0376.fuix.library.client.ui.blur;

import com.google.common.collect.ImmutableSet;
import net.minecraft.client.resources.IResourceManager;
import net.minecraft.client.resources.IResourceManagerReloadListener;
import net.minecraft.client.resources.IResourcePack;
import net.minecraft.client.resources.data.IMetadataSection;
import net.minecraft.client.resources.data.MetadataSerializer;
import net.minecraft.client.resources.data.PackMetadataSection;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.TextComponentString;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import javax.annotation.Nullable;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

@SideOnly(Side.CLIENT)
public final class ShaderResourcePack implements IResourcePack, IResourceManagerReloadListener {
   private final Map loadedData = new HashMap();

   protected boolean validPath(ResourceLocation location) {
      return location.getPath().equals("minecraft") && location.getPath().startsWith("shaders/");
   }

   public InputStream getInputStream(ResourceLocation location) throws IOException {
      if (!this.validPath(location)) {
         throw new FileNotFoundException(location.toString());
      } else {
         String s = (String) this.loadedData.get(location);
         if (s == null) {
            InputStream in = Blur.class.getResourceAsStream("/" + location.getPath());
            StringBuilder data = new StringBuilder();
            Scanner scan = new Scanner(in);

            try {
               while (scan.hasNextLine()) {
                  data.append(scan.nextLine().replaceAll("@radius@", Integer.toString(Blur.radius))).append('\n');
               }
            } finally {
               scan.close();
            }

            s = data.toString();
            this.loadedData.put(location, s);
         }

         return new ByteArrayInputStream(s.getBytes());
      }
   }

   public boolean resourceExists(ResourceLocation location) {
      return this.validPath(location) && Blur.class.getResource("/" + location.getPath()) != null;
   }

   public Set getResourceDomains() {
      return ImmutableSet.of("minecraft");
   }

   @Nullable
   @Override
   public <T extends IMetadataSection> T getPackMetadata(MetadataSerializer metadataSerializer, String metadataSectionName) throws IOException {
      return (T) new PackMetadataSection(new TextComponentString("Blur's default shaders"), 3);
   }

   public BufferedImage getPackImage() throws IOException {
      throw new FileNotFoundException("pack.png");
   }

   public String getPackName() {
      return "Blur dummy resource pack";
   }

   public void onResourceManagerReload(IResourceManager resourceManager) {
      this.loadedData.clear();
   }
}
