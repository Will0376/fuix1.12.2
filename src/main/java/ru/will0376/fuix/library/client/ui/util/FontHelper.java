package ru.will0376.fuix.library.client.ui.util;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import org.lwjgl.opengl.GL11;

public class FontHelper {
   public static double getWidth(String str, float size) {
      double max = 0.0D;
      String[] var4 = str.split("\n");
      int var5 = var4.length;

      for (int var6 = 0; var6 < var5; ++var6) {
         String var10000 = var4[var6];
         double w = (float) Minecraft.getMinecraft().fontRenderer.getStringWidth(str) * size;
         if (w > max) {
            max = w;
         }
      }

      return max;
   }

   public static double getHeight(float size) {
      return (float) Minecraft.getMinecraft().fontRenderer.FONT_HEIGHT * size;
   }

   public static double getHeight(String str, float size) {
      return (float) Minecraft.getMinecraft().fontRenderer.FONT_HEIGHT * size * (float) str.split("\n").length;
   }

   public static void drawCentered(String str, double x, double y, float size) {
      drawCentered(str, x, y, -1, size);
   }

   public static void drawCentered(String str, double x, double y, int color, float size) {
      draw(str, x, y, color, Align.CENTER, size);
   }

   public static void drawCenteredX(String str, double x, double y, float size) {
      drawCenteredX(str, x, y, -1, size);
   }

   public static void drawCenteredX(String str, double x, double y, int color, float size) {
      draw(str, x, y, color, Align.CENTER_X, size);
   }

   public static void drawCenteredY(String str, double x, double y, float size) {
      drawCenteredY(str, x, y, -1, size);
   }

   public static void drawCenteredY(String str, double x, double y, int color, float size) {
      draw(str, x, y, color, Align.CENTER_Y, size);
   }

   public static void drawRight(String str, double x, double y, float size) {
      drawRight(str, x, y, -1, size);
   }

   public static void drawRight(String str, double x, double y, int color, float size) {
      draw(str, x, y, color, Align.RIGHT, size);
   }

   public static void draw(String str, double x, double y, float size) {
      draw(str, x, y, -1, size);
   }

   public static void draw(String str, double x, double y, int color, float size) {
      draw(str, x, y, color, Align.LEFT, size);
   }

   public static void draw(String str, double x, double y, int color, Align align, float size) {
      draw(str, x, y, color, align, size, false);
   }

   public static void draw(String str, double x, double y, int color, Align align, float size, boolean shadow) {
      if (str != null && str.length() != 0) {
         FontRenderer font = Minecraft.getMinecraft().fontRenderer;
         String[] var10 = str.split("\n");
         int var11 = var10.length;

         for (String s1 : var10) {
            GL11.glPushMatrix();
            switch (align) {
               case LEFT:
                  GL11.glTranslated(x, y, 0.0D);
                  break;
               case RIGHT:
                  GL11.glTranslated(x - getWidth(s1, size), y, 0.0D);
                  break;
               case CENTER_X:
                  GL11.glTranslated(x - getWidth(s1, size) / 2.0D, y, 0.0D);
                  break;
               case LEFT_CENTER_Y:
               case CENTER_Y:
                  GL11.glTranslated(x, y - getHeight(size) / 2.0D, 0.0D);
                  break;
               case CENTER:
                  GL11.glTranslated(x - getWidth(s1, size) / 2.0D, y - getHeight(size) / 2.0D, 0.0D);
                  break;
               case RIGHT_CENTER_Y:
                  GL11.glTranslated(x - getWidth(s1, size), y - getHeight(size) / 2.0D, 0.0D);
                  break;
               default:
                  GL11.glTranslated(x, y, 0.0D);
            }

            if (size < 0.0F || (double) size > 100.0D) {
               size = 1.0F;
            }

            GL11.glScalef(size, size, 1.0F);
            if (shadow) {
               font.drawStringWithShadow(s1, 0, 0, color);
            } else {
               font.drawString(s1, 0, 0, color);
            }

            GL11.glPopMatrix();
            y += size / 2.0F;
         }

      }
   }

   public enum Align {
      LEFT,
      RIGHT,
      LEFT_CENTER_Y,
      RIGHT_CENTER_Y,
      CENTER,
      CENTER_X,
      CENTER_Y
   }
}
