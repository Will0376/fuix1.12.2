package ru.will0376.fuix.library.client.ui.util;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.ScaledResolution;
import org.lwjgl.opengl.GL11;

public class ScissorTest {
   private static final int max = 400;
   private static final ScissorTest[] objects = new ScissorTest[400];
   private static int lastObject = -1;
   private int index;
   private int left;
   private int right;
   private int top;
   private int bottom;

   public ScissorTest(int x, int y, int width, int height) {
      ++lastObject;
      if (lastObject < 400) {
         this.index = lastObject;
         objects[this.index] = this;
         this.left = x;
         this.right = x + width - 1;
         this.top = y;
         this.bottom = y + height - 1;
         if (this.index > 0) {
            ScissorTest parent = objects[this.index - 1];
            if (this.left < parent.left) {
               this.left = parent.left;
            }

            if (this.right > parent.right) {
               this.right = parent.right;
            }

            if (this.top < parent.top) {
               this.top = parent.top;
            }

            if (this.bottom > parent.bottom) {
               this.bottom = parent.bottom;
            }
         }

         this.resume();
      } else {
         System.err.println("Scissor count limit reached: 400");
      }

   }

   private static void glScissor(int x, int y, int width, int height) {
      if (width < 0) {
         width = 0;
      }

      if (height < 0) {
         height = 0;
      }

      double sf = new ScaledResolution(Minecraft.getMinecraft()).getScaleFactor();
      double calcPosY = (double) Minecraft.getMinecraft().displayHeight - (double) y * sf;
      GL11.glScissor((int) ((double) x * sf), (int) (calcPosY - (double) height * sf), (int) ((double) width * sf), (int) ((double) height * sf));
   }

   private void resume() {
      glScissor(this.left, this.top, this.right - this.left + 1, this.bottom - this.top + 1);
      GL11.glEnable(3089);
   }

   public void destroy() {
      if (this.index < lastObject) {
         System.err.println("There are scissors below this one");
      }

      GL11.glDisable(3089);
      objects[this.index] = null;
      --lastObject;
      if (lastObject > -1) {
         objects[lastObject].resume();
      }

   }

   protected void finalize() {
      this.destroy();
   }
}
