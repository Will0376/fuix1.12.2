package ru.will0376.fuix.library.client.ui;

import java.awt.*;

public abstract class AnimatedComponent extends GuiComponent {
   private long animationCounter;

   public AnimatedComponent(String id, Rectangle bounds, Window window) {
      super(id, bounds, window);
   }

   protected long getAnimationCounter() {
      return this.animationCounter;
   }

   public void update(int x, int y) {
      super.update(x, y);
      ++this.animationCounter;
   }
}
