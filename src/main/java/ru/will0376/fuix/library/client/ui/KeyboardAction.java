package ru.will0376.fuix.library.client.ui;

public class KeyboardAction {
	private final boolean isShiftDown;
	private final boolean isCtrlDown;
	private final char c;
	private final int keyCode;
	private final KeyActionType actionType;

	public KeyboardAction(boolean isShiftDown, boolean isCtrlDown, char c, int keyCode, KeyActionType actionType) {
		this.isShiftDown = isShiftDown;
		this.isCtrlDown = isCtrlDown;
		this.c = c;
		this.keyCode = keyCode;
		this.actionType = actionType;
	}

	public boolean isShiftDown() {
		return this.isShiftDown;
	}

	public boolean isCtrlDown() {
		return this.isCtrlDown;
	}

	public char getChar() {
		return this.c;
	}

	public int getKeyCode() {
		return this.keyCode;
	}

	public KeyActionType getActionType() {
		return this.actionType;
	}

	public String toString() {
		return "KeyboardAction{isShiftDown=" + this.isShiftDown + ", isCtrlDown=" + this.isCtrlDown + ", c=" + this.c + ", keyCode=" + this.keyCode + ", actionType=" + this.actionType + '}';
	}

	public enum KeyActionType {
		Press,
		Release
	}
}
