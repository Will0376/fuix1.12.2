package ru.will0376.fuix.library.client.ui.widget;

import ru.will0376.fuix.library.client.ui.GuiComponent;
import ru.will0376.fuix.library.client.ui.Window;

import java.awt.*;

public class ImageView extends GuiComponent {
   public ImageView(String id, Rectangle bounds, Window window) {
      super(id, bounds, window);
   }

   protected void onDraw(int x, int y) {
   }

   protected void onUpdate(int x, int y) {
   }
}
