package ru.will0376.fuix.library.client.ui.widget.interfaces;

public interface IScrollableComponent {
	void setScrollOffset(int var1);
}
