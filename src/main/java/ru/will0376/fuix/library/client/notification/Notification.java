package ru.will0376.fuix.library.client.notification;


import ru.will0376.fuix.Fuix;
import ru.will0376.fuix.library.client.event.RenderEvent;

public class Notification {
	public String text = "";
	public int time = 0;
	public int maxTime = 0;
	private boolean replace = true;
	private DrawNotification drawNotification;

	public Notification(String text, int time, String style) {
		this.text = text;
		this.time = time * 20;
		this.maxTime = time * 20;
		Fuix.getClient().getRenderEvent();
		this.drawNotification = RenderEvent.getNotificationStyle(style);
	}

	public DrawNotification getDrawNotification() {
		return this.drawNotification;
	}

	public Notification setDrawNotification(DrawNotification drawNotification) {
		this.drawNotification = drawNotification;
		return this;
	}

	public boolean isReplace() {
		return this.replace;
	}

	public Notification setReplace(boolean replace) {
		this.replace = replace;
		return this;
	}
}
