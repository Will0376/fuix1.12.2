package ru.will0376.fuix.library.client.ui;

import java.awt.*;

public abstract class ModalWindow extends Window {
	private final Window mainWindow;
	private boolean visible;
	private boolean mouseClick = false;

	public ModalWindow(Window mainWindow, int width, int height) {
		super(width, height);
		this.mainWindow = mainWindow;
		this.visible = false;
	}

	public void onHandleMouse(MouseAction mouseAction) {
		Point mousePosition = this.getComponentContainer().getMousePosition();
		Rectangle rectangle = this.getAbsoluteBounds();
		rectangle.setLocation(rectangle.x, rectangle.y - 10);
		rectangle.setSize(rectangle.width, rectangle.height + 10);
		if (!rectangle.contains(mousePosition)) {
			if (mouseAction.getActionType().equals(MouseAction.ActionType.Click)) {
				this.mouseClick = true;
			} else if (mouseAction.getActionType().equals(MouseAction.ActionType.Click) && this.mouseClick) {
				this.mainWindow.onHideModals();
				this.mouseClick = false;
			}
		} else {
			this.mouseClick = false;
		}

	}

	public void onClose() {
		this.mainWindow.onHideModals();
	}

	public void onViewModal(ModalWindow modalWindow) {
		this.mainWindow.onViewModal(modalWindow);
	}

	public ModalWindow getModalWindow() {
		return this.mainWindow.getModalWindow();
	}

	public Window getMainWindow() {
		return this.mainWindow;
	}

	public boolean isViewModal() {
		return false;
	}

	public boolean isVisible() {
		return this.visible;
	}

	public void setVisible(boolean visible) {
		this.visible = visible;
	}
}
