package ru.will0376.fuix.library.client.ui.render;

import org.lwjgl.opengl.GL11;
import ru.will0376.fuix.library.client.ui.util.DrawHelper;
import ru.will0376.fuix.library.client.ui.util.GlStateManager;

import java.awt.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

public class ParticleEngine {
   private final Random random;
   private final List particles;
   private final boolean randomDespawn;

   public ParticleEngine() {
      this(false);
   }

   public ParticleEngine(boolean randomDespawn) {
      this.random = new Random();
      this.particles = new ArrayList();
      this.randomDespawn = randomDespawn;
   }

   public void render() {
      GlStateManager.enableBlend();
      GlStateManager.blendFunc(770, 771);
      GlStateManager.enableTexture2D();
      Iterator var1 = this.particles.iterator();

      while (var1.hasNext()) {
         Particle particle = (Particle) var1.next();
         particle.applyPhysics();
         GlStateManager.pushMatrix();
         float scale = particle.life / (float) this.getMaxLife() / 5.0F;
         GL11.glScalef(scale, scale, scale);
         int drawSize = 0;
         DrawHelper.drawRectRounded(particle.x * (1.0F / scale), particle.y * (1.0F / scale), 32 + drawSize, 32 + drawSize, 16.0D, (new Color(255, 255, 255, 140))
                 .hashCode());
         GlStateManager.popMatrix();
      }

   }

   private void renderTexture(int textureWidth, int textureHeight, float x, float y, float width, float height, float srcX, float srcY, float srcWidth, float srcHeight) {
      float renderSRCX = srcX / (float) textureWidth;
      float renderSRCY = srcY / (float) textureHeight;
      float renderSRCWidth = srcWidth / (float) textureWidth;
      float renderSRCHeight = srcHeight / (float) textureHeight;
      GL11.glBegin(4);
      GL11.glTexCoord2f(renderSRCX + renderSRCWidth, renderSRCY);
      GL11.glVertex2d(x + width, y);
      GL11.glTexCoord2f(renderSRCX, renderSRCY);
      GL11.glVertex2d(x, y);
      GL11.glTexCoord2f(renderSRCX, renderSRCY + renderSRCHeight);
      GL11.glVertex2d(x, y + height);
      GL11.glTexCoord2f(renderSRCX, renderSRCY + renderSRCHeight);
      GL11.glVertex2d(x, y + height);
      GL11.glTexCoord2f(renderSRCX + renderSRCWidth, renderSRCY + renderSRCHeight);
      GL11.glVertex2d(x + width, y + height);
      GL11.glTexCoord2f(renderSRCX + renderSRCWidth, renderSRCY);
      GL11.glVertex2d(x + width, y);
      GL11.glEnd();
   }

   public void updateParticles() {
      for (int i = 0; i < this.particles.size(); ++i) {
         Particle particle = (Particle) this.particles.get(i);
         particle.update();
         if (particle.life <= 0.0F) {
            this.particles.remove(particle);
         }
      }

   }

   public void spawnParticles(int spawnX, int spawnY, int dispurseX, int dispurseY, float velocity, float gravity) {
      int startX = spawnX + this.random.nextInt(dispurseX);
      int startY = spawnY + this.random.nextInt(dispurseY);
      Particle particle = new Particle((float) startX, (float) startY, velocity, gravity);
      this.particles.add(particle);
   }

   private int getMaxLife() {
      return 50;
   }

   private class Particle {
      float life;
      float x;
      float y;
      float motionX;
      float motionY;
      float gravity;

      private Particle(float x, float y, float velocity, float gravity) {
         this.x = x;
         this.y = y;
         this.motionX = ParticleEngine.this.random.nextFloat() * velocity;
         this.motionY = ParticleEngine.this.random.nextFloat() * velocity;
         if (ParticleEngine.this.random.nextBoolean()) {
            this.motionX = -this.motionX;
         }

         if (ParticleEngine.this.random.nextBoolean()) {
            this.motionY = -this.motionY;
         }

         this.life = (float) ParticleEngine.this.getMaxLife();
         this.gravity = gravity;
      }

      // $FF: synthetic method
      Particle(float x1, float x2, float x3, float x4, Object x5) {
         this(x1, x2, x3, x4);
      }

      private void applyPhysics() {
         this.x += this.motionX * 0.1F;
         this.y += this.motionY * 0.1F;
         this.motionX *= 0.99F;
         this.motionY *= 0.99F;
         this.y += this.gravity * 0.1F;
      }

      private void update() {
         if (ParticleEngine.this.randomDespawn) {
            if (ParticleEngine.this.random.nextBoolean()) {
               this.life -= ParticleEngine.this.random.nextFloat() * 2.0F;
            }
         } else {
            this.life -= ParticleEngine.this.random.nextFloat() * 2.0F;
         }

      }
   }
}
