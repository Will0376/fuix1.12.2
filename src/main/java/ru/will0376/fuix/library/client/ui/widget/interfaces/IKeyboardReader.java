package ru.will0376.fuix.library.client.ui.widget.interfaces;


import ru.will0376.fuix.library.client.ui.KeyboardAction;

public interface IKeyboardReader {
	void handleKeyboard(KeyboardAction var1);
}
