package ru.will0376.fuix.library.client.ui.drawable;

import net.minecraft.client.Minecraft;
import ru.will0376.fuix.library.client.ui.GuiComponent;
import ru.will0376.fuix.library.client.ui.util.DrawHelper;

import java.awt.*;

public class TextFieldDrawable extends Drawable {
   private Color lineColor = new Color(224, 255, 255);

   public TextFieldDrawable() {
   }

   public TextFieldDrawable(Color lineColor) {
      this.lineColor = lineColor;
   }

   public void onDraw(GuiComponent component, Drawable.State state) {
      Rectangle rec = component.getAbsoluteBounds();
      DrawHelper.drawRect(rec.x, rec.y + rec.height / 2 + Minecraft.getMinecraft().fontRenderer.FONT_HEIGHT / 2 + 2, rec.width, 0.5D, this.lineColor
              .hashCode());
   }
}
