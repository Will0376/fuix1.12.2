package ru.will0376.fuix.library.client.ui.widget;

import net.minecraft.client.Minecraft;
import net.minecraft.client.audio.PositionedSoundRecord;
import net.minecraft.init.SoundEvents;
import ru.will0376.fuix.library.client.ui.GuiComponent;
import ru.will0376.fuix.library.client.ui.MouseAction;
import ru.will0376.fuix.library.client.ui.Window;
import ru.will0376.fuix.library.client.ui.drawable.CheckBoxDrawable;
import ru.will0376.fuix.library.client.ui.util.DrawHelper;
import ru.will0376.fuix.library.client.ui.util.FontHelper;

import java.awt.*;

public class CheckBox extends GuiComponent {
   private final Runnable callback = new Runnable() {
      @Override
      public void run() {
         System.out.println("test");
      }
   };
   public boolean enabled = true;
   protected boolean isChecked = false;
   private String annotation;
   private int textColor = (new Color(255, 255, 255)).hashCode();
   private int color = (new Color(255, 255, 255, 160)).hashCode();

   public CheckBox(String id, Rectangle bounds, Window window) {
      super(id, bounds, window);
      this.setDrawable(new CheckBoxDrawable());
   }

   public CheckBox(String id, Rectangle bounds, Window window, String str) {
      super(id, bounds, window);
      this.setDrawable(new CheckBoxDrawable());
      this.annotation = str;
   }

   protected void onDraw(int x, int y) {
      int posX = this.getAbsoluteBounds().x;
      int posY = this.getAbsoluteBounds().y;
      int width = this.getBounds().width;
      int height = this.getBounds().height;
      if (this.isChecked) {
         DrawHelper.drawRectRounded((double) posX + 1.5D, (double) posY + 1.5D, width - 3, height - 3, 1.0D, this.color);
      }

      if (this.annotation != null) {
         FontHelper.draw(this.annotation, posX + width + 3, (double) posY + ((double) height / 2.0D - FontHelper.getHeight(1.0F) / 2.0D), this.textColor, 1.0F);
      }

   }

   protected void onUpdate(int x, int y) {
   }

   public void handleMouse(MouseAction mouseAction) {
      if (mouseAction.getActionType().equals(MouseAction.ActionType.Click) && this.getAbsoluteBounds()
              .contains(mouseAction.getPosX(), mouseAction.getPosY())) {
         Minecraft.getMinecraft().soundHandler.playSound(PositionedSoundRecord.getMasterRecord(SoundEvents.UI_BUTTON_CLICK, 1.0F));
         this.setChecked(!this.isChecked());
         this.callback.run();
         this.stateChanged();
      }

   }

   public boolean isChecked() {
      return this.isChecked;
   }

   public CheckBox setChecked(boolean ch) {
      this.isChecked = ch;
      return this;
   }

   protected void stateChanged() {
   }

   public CheckBox setColor(int color) {
      this.color = color;
      return this;
   }

   public CheckBox setTextColor(int color) {
      this.textColor = color;
      return this;
   }
}
