package ru.will0376.fuix.library.client.ui;

import net.minecraft.client.gui.GuiScreen;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GL11;

import java.io.IOException;

public class GuiScreenAdapter extends GuiScreen {
   private final Window window;

   public GuiScreenAdapter(Window window) {
      Keyboard.enableRepeatEvents(true);
      this.window = window;
      window.init();
   }

   public Window getWindow() {
      return this.window;
   }

   public void drawScreen(int i1, int i2, float partialTicks) {
      this.window.draw(partialTicks);
      if (this.window.getModalWindow() != null) {
         GL11.glPushMatrix();
         GL11.glScaled(1.0D, 1.0D, 2.0D);
         this.window.getModalWindow().draw(partialTicks);
         GL11.glPopMatrix();
      }

   }

   protected void keyTyped(char c1, int i1) throws IOException {
      super.keyTyped(c1, i1);
   }

   protected void mouseClicked(int x, int y, int button) {
      if (this.window.getModalWindow() != null) {
         this.window.getModalWindow()
                 .getComponentContainer()
                 .handleMouseAction(new MouseAction(button, x, y, MouseAction.ActionType.Click));
      } else {
         this.window.getComponentContainer()
                 .handleMouseAction(new MouseAction(button, x, y, MouseAction.ActionType.Click));
      }

   }

   protected void mouseMovedOrUp(int x, int y, int button) {
      if (this.window.getModalWindow() != null) {
         this.window.getModalWindow()
                 .getComponentContainer()
                 .handleMouseAction(new MouseAction(button, x, y, MouseAction.ActionType.Click));
      } else {
         this.window.getComponentContainer()
                 .handleMouseAction(new MouseAction(button, x, y, MouseAction.ActionType.Click));
      }

   }

   protected void mouseClickMove(int x, int y, int button, long holdTime) {
      if (this.window.getModalWindow() != null) {
         this.window.getModalWindow()
                 .getComponentContainer()
                 .handleMouseAction((new MouseAction(button, x, y, MouseAction.ActionType.Moved)).setHoldTime(holdTime));
      } else {
         this.window.getComponentContainer()
                 .handleMouseAction((new MouseAction(button, x, y, MouseAction.ActionType.Moved)).setHoldTime(holdTime));
      }

   }

   public void initGui() {
   }

   public void handleMouseInput() throws IOException {
      super.handleMouseInput();
      int dWheel = Mouse.getDWheel();
      if (Mouse.getDWheel() != dWheel) {
         if (this.window.getModalWindow() != null) {
            this.window.getModalWindow().getComponentContainer().handleWheel(dWheel);
         } else {
            this.window.getComponentContainer().handleWheel(dWheel);
         }
      }

   }

   public void handleKeyboardInput() throws IOException {
      super.handleKeyboardInput();
      if (this.window.getModalWindow() != null) {
         this.window.getModalWindow()
                 .getComponentContainer()
                 .keyboardAction(new KeyboardAction(isShiftKeyDown(), isCtrlKeyDown(), Keyboard.getEventCharacter(), Keyboard
                         .getEventKey(), Keyboard.getEventKeyState() ? KeyboardAction.KeyActionType.Press : KeyboardAction.KeyActionType.Release));
      } else {
         this.window.getComponentContainer()
                 .keyboardAction(new KeyboardAction(isShiftKeyDown(), isCtrlKeyDown(), Keyboard.getEventCharacter(), Keyboard
                         .getEventKey(), Keyboard.getEventKeyState() ? KeyboardAction.KeyActionType.Press : KeyboardAction.KeyActionType.Release));
      }

   }

   public void updateScreen() {
      if (this.window.getModalWindow() != null) {
         this.window.getModalWindow().update();
      } else {
         this.window.update();
      }

   }

   public boolean doesGuiPauseGame() {
      return false;
   }

   public void onGuiClosed() {
      Keyboard.enableRepeatEvents(false);
      super.onGuiClosed();
   }
}
