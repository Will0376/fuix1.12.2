package ru.will0376.fuix.library.client.ui.widget.interfaces;

import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import ru.will0376.fuix.library.client.ui.GuiComponent;

@SideOnly(Side.CLIENT)
public interface IActionListener {
	void actionPerformed(int var1, GuiComponent var2, Object... var3);
}
