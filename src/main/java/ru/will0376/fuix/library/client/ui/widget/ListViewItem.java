package ru.will0376.fuix.library.client.ui.widget;

import ru.will0376.fuix.library.client.ui.GuiComponent;

import java.awt.*;

public abstract class ListViewItem extends GuiComponent {
   private ListView listView;
   private int renderId = -1;

   public ListViewItem(int width, int height, ListView listView) {
      super("", new Rectangle(0, 0, width, height), listView.getWindow());
   }

   protected abstract void onItemDraw(int var1, int var2, int var3, int var4);

   protected void onDraw(int x, int y) {
      Point point = this.topLeft();
      this.onItemDraw(this.getBounds().x, this.getBounds().y, point.x, point.y);
   }

   private Point topLeft() {
      return new Point(this.getBounds().x + this.getBounds().width, this.getBounds().y);
   }

   public void setId(String id) {
      if (super.id.isEmpty()) {
         super.id = id;
      }

   }

   public ListView getListView() {
      return this.listView;
   }

   public ListViewItem setListView(ListView listView) {
      if (this.listView == null) {
         this.listView = listView;
      }

      return this;
   }

   public boolean isEnabled() {
      return super.isEnabled();
   }

   public int getRenderId() {
      return this.renderId;
   }

   public ListViewItem setRenderId(int renderId) {
      this.renderId = renderId;
      return this;
   }
}
