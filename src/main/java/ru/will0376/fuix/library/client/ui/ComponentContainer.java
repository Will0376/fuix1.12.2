package ru.will0376.fuix.library.client.ui;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.util.text.TextFormatting;
import org.lwjgl.input.Mouse;
import ru.will0376.fuix.library.client.ClientConfig;
import ru.will0376.fuix.library.client.ui.util.*;
import ru.will0376.fuix.library.client.ui.widget.interfaces.IKeyboardReader;
import ru.will0376.fuix.library.client.ui.widget.interfaces.IScrollableComponent;

import java.awt.*;
import java.util.ArrayList;
import java.util.LinkedHashMap;

public class ComponentContainer {
   private final LinkedHashMap<String, GuiComponent> components;
   private final Window window;
   private final Point mousePosition;
   public GuiComponent focusedComponent = null;
   private boolean holdCurrentFocus = false;
   private GuiComponent keyboardFocusedElement = null;
   private ScaledResolution sc;

   public ComponentContainer(Window window) {
      this.sc = new ScaledResolution(Minecraft.getMinecraft());
      this.mousePosition = new Point(0, 0);
      this.window = window;
      this.components = new LinkedHashMap<>();
   }

   public GuiComponent getComponentById(String id) {
      return !this.components.containsKey(id) ? null : this.components.get(id);
   }

   public void addComponent(GuiComponent component) {
      if (!this.components.containsKey(component.getId())) {
         this.components.put(component.getId(), component);
         component.init();
      } else {
         System.err.println("Fail with adding component, its already exists! Id = " + component.getId());
      }

   }

   public void removeComponent(GuiComponent component) {
      if (component != null) {
         this.removeComponent(component.id);
      }

   }

   public void removeComponent(String id) {
      if (!id.isEmpty()) {
         if (this.components.containsKey(id)) {
            for (int i = this.components.get(id).getChildren().size() - 1; i >= 0; --i) {
               this.components.remove(this.components.get(id).getChildren().get(i).getId());
            }

            this.components.remove(id);
         } else {
            System.err.println("Fail with removing component, its does not exists!");
         }

      }
   }

   public boolean hasComponent(String id) {
      return this.components.containsKey(id);
   }

   public boolean isFocused(GuiComponent component) {
      return this.focusedComponent != null && component.getId().equals(this.focusedComponent.getId());
   }

   public void applyFocusPoint(Point point) {
      this.focusedComponent = this.topComponentOnPoint(point);
   }

   public boolean isOnTop(GuiComponent component, Point cursorPosition) {
      return this.topComponentOnPoint(cursorPosition).equals(component);
   }

   public GuiComponent getParent(GuiComponent guiComponent) {
      return guiComponent.getParent();
   }

   private ArrayList<GuiComponent> getParents(ArrayList<GuiComponent> guiComponents, GuiComponent guiComponent) {
      ArrayList<GuiComponent> gc = new ArrayList<>(guiComponents);
      if (guiComponent.getParent() != null) {
         gc.add(guiComponent.getParent());
         return this.getParents(gc, guiComponent.getParent());
      } else {
         return gc;
      }
   }

   public ArrayList<GuiComponent> getParents(GuiComponent guiComponent) {
      return this.getParents(new ArrayList<>(), guiComponent);
   }

   public boolean guiComponentPointContainsParents(GuiComponent component, Point point) {
      ArrayList<GuiComponent> parents = this.getParents(component);
      Rectangle finRect = component.getAbsoluteBounds();

      for (GuiComponent parent : parents) {
         finRect = finRect.intersection(parent.getAbsoluteBounds());
      }

      return finRect.contains(point);
   }

   public ArrayList<GuiComponent> getChildren(GuiComponent component) {
      ArrayList<GuiComponent> returnList = new ArrayList<>();
      ArrayList<GuiComponent> guiComponents = new ArrayList<GuiComponent>(this.components.values());

      for (GuiComponent guiComponent : guiComponents) {
         if (guiComponent.hasParent() && guiComponent.getParent().getId().equals(component.getId())) {
            returnList.add(guiComponent);
            returnList.addAll(this.getChildren(guiComponent));
         }
      }

      return returnList;
   }

   public void putOnTop(String componentId) {
      if (this.components.containsKey(componentId)) {
         this.components.put(componentId, this.components.remove(componentId));
      }
   }

   public void handleWheel(int offset) {
      IScrollableComponent scrollableComponent = this.topScrollableOnPoint(this.mousePosition);
      if (scrollableComponent != null) {
         scrollableComponent.setScrollOffset(offset);
      }

   }

   public IScrollableComponent topScrollableOnPoint(Point point) {
      ArrayList<GuiComponent> guiComponentList = new ArrayList<>();
      ArrayList<GuiComponent> guiComponents = new ArrayList<>(this.components.values());

      for (GuiComponent guiComponent : guiComponents) {
         if (guiComponent.getAbsoluteBounds()
                 .contains(point) && guiComponent instanceof IScrollableComponent && guiComponent.isEnabled() && this.guiComponentPointContainsParents(guiComponent, point)) {
            guiComponentList.add(guiComponent);
         }
      }

      if (guiComponentList.size() == 0) {
         return null;
      } else {
         return (IScrollableComponent) guiComponentList.get(guiComponentList.size() - 1);
      }
   }

   public GuiComponent topComponentOnPoint(Point point) {
      Point zeroPoint = this.window.getZeroPoint();
      zeroPoint.y -= 22;
      Rectangle windowRect = new Rectangle(zeroPoint, new Dimension(this.window.getWidth(), this.window.getHeight() + 22));
      if (!windowRect.contains(point)) {
         return null;
      } else {
         ArrayList<GuiComponent> guiComponentList = new ArrayList<GuiComponent>();
         ArrayList<GuiComponent> guiComponents = new ArrayList<>(this.components.values());

         for (GuiComponent guiComponent : guiComponents) {
            if (guiComponent.getAbsoluteBounds()
                    .contains(point) && guiComponent.isEnabled() && this.guiComponentPointContainsParents(guiComponent, point)) {
               guiComponentList.add(guiComponent);
            }
         }

         if (guiComponentList.size() == 0) {
            return null;
         } else {
            return guiComponentList.get(guiComponentList.size() - 1);
         }
      }
   }

   public void handleMouseAction(MouseAction mouseAction) {
      this.window.onHandleMouse(mouseAction);

      if (mouseAction.getActionType().equals(MouseAction.ActionType.Click)) {
         this.keyboardFocusedElement = null;
         if (this.focusedComponent != null) {
            this.holdCurrentFocus = true;
            this.keyboardFocusedElement = this.focusedComponent;
         }
      }

      if (this.focusedComponent != null && this.holdCurrentFocus) {
         this.focusedComponent.handleMouse(mouseAction);
      }

      ArrayList<GuiComponent> guiComponents = new ArrayList<GuiComponent>(this.components.values());

      for (GuiComponent guiComponent : guiComponents) {
         if (guiComponent.isVisible()) {
            guiComponent.handleMouseAction(mouseAction);
         }
      }

      if (mouseAction.getActionType().equals(MouseAction.ActionType.Click)) {
         this.holdCurrentFocus = false;
      }

   }

   public void init() {
   }

   public void update() {
      ArrayList<GuiComponent> guiComponents = new ArrayList<>(this.components.values());

      int i;
      for (i = 0; i < guiComponents.size(); ++i) {
         if ((guiComponents.get(i)).isVisible()) {
            (guiComponents.get(i)).update(this.window.getZeroPoint().x, this.window.getZeroPoint().y);
         }
      }

      for (i = 0; i < guiComponents.size(); ++i) {
         if ((guiComponents.get(i)).isVisible() && (guiComponents.get(i)).isAlwaysOnTop()) {
            this.putOnTop((guiComponents.get(i)).getId());
            ArrayList<GuiComponent> children = (guiComponents.get(i)).getChildren();

            for (GuiComponent child : children) {
               this.putOnTop(child.getId());
            }
         }
      }

   }

   public void draw() {
      if (!this.holdCurrentFocus && !this.window.isViewModal()) {
         this.applyFocusPoint(new Point(this.getMousePosition().x, this.getMousePosition().y));
      }

      ArrayList<GuiComponent> guiComponents = new ArrayList<>(this.components.values());

      int i;
      for (i = 0; i < guiComponents.size(); ++i) {
         if ((guiComponents.get(i)).isVisible()) {
            ScissorTest scissorTest = null;
            if ((guiComponents.get(i)).getParent() != null) {
               Rectangle bounds = (guiComponents.get(i)).getAbsoluteParent().getAbsoluteBounds();
               bounds.x = SimpleHelper.clamp(bounds.x, this.window.getZeroPoint().x, this.window.getZeroPoint().x + this.window
                       .getWidth());
               bounds.y = SimpleHelper.clamp(bounds.y, this.window.getZeroPoint().y - 22, this.window.getZeroPoint().y + this.window
                       .getHeight());
               bounds.width = SimpleHelper.clamp(bounds.width, 0, this.window.getWidth());
               bounds.height = SimpleHelper.clamp(bounds.height, 0, this.window.getHeight() + 22);
               scissorTest = new ScissorTest(bounds.x, bounds.y, bounds.width, bounds.height);
            } else if ((guiComponents.get(i)).isIgnoreScissor()) {
               scissorTest = new ScissorTest(0, 0, Monitor.getWidth() + 2, Monitor.getHeight() + 2);
            } else {
               scissorTest = new ScissorTest(this.window.getZeroPoint().x, this.window.getZeroPoint().y - 22, this.window
                       .getWidth(), this.window.getHeight() + 22);
            }

            (guiComponents.get(i)).draw(this.window.getZeroPoint().x, this.window.getZeroPoint().y);
            scissorTest.destroy();
         }
      }

      if (this.focusedComponent != null && !this.holdCurrentFocus && this.focusedComponent.getToolTip() != null) {
         DrawHelper.drawTooltip(this.mousePosition.x, this.mousePosition.y, this.focusedComponent.getToolTip());
      }

      if (Minecraft.getMinecraft().gameSettings.showDebugInfo && !this.window.isViewModal() && ClientConfig.debug) {
         for (i = 0; i < guiComponents.size(); ++i) {
            int renderColor = (guiComponents.get(i)).equals(this.focusedComponent) ? 255 : 16711680;
            if ((guiComponents.get(i)).equals(this.keyboardFocusedElement)) {
               renderColor = 16711935;
            }

            if ((guiComponents.get(i)).equals(this.focusedComponent) && this.holdCurrentFocus) {
               renderColor = 65280;
            }

            String color = !(guiComponents.get(i)).isVisible() ? TextFormatting.RED + "" : TextFormatting.GREEN + "";
            String str = color + "oO" + TextFormatting.RESET + " : " + (guiComponents.get(i)).getId();
            if ((guiComponents.get(i)).getParent() != null) {
               str = str + TextFormatting.AQUA + " - " + (guiComponents.get(i)).getParent()
                       .getId() + " - " + TextFormatting.GOLD + (guiComponents.get(i)).getAbsoluteParent().getId();
            }

            str = str + TextFormatting.BOLD + "" + TextFormatting.YELLOW + " " + this.guiComponentPointContainsParents(guiComponents
                    .get(i), this.mousePosition) + "";
            FontHelper.draw(str, 2.0D, 5 * i + 2, (guiComponents.get(i)).equals(this.focusedComponent) ? 16755370 : 16777215, 1.0F);
            if ((guiComponents.get(i)).isVisible()) {
               DrawHelper.drawRect((float) (guiComponents.get(i)).getAbsoluteBounds().x + 0.7F, (float) (guiComponents.get(i))
                       .getAbsoluteBounds().y + 0.7F, (float) (guiComponents.get(i)).getAbsoluteBounds().width - 1.4F, (float) (guiComponents
                       .get(i)).getAbsoluteBounds().height - 1.4F, renderColor);
            }
         }
      }

   }

   public void keyboardAction(KeyboardAction action) {
      if (this.keyboardFocusedElement instanceof IKeyboardReader) {
         ((IKeyboardReader) this.keyboardFocusedElement).handleKeyboard(action);
      }

   }

   public Point getMousePosition() {
      this.sc = new ScaledResolution(Minecraft.getMinecraft());
      this.mousePosition.x = Mouse.getX() / this.sc.getScaleFactor();
      this.mousePosition.y = (Minecraft.getMinecraft().displayHeight - Mouse.getY()) / this.sc.getScaleFactor();
      return this.mousePosition;
   }

   public GuiComponent getKeyboardFocusedElement() {
      return this.keyboardFocusedElement;
   }

   public LinkedHashMap<? extends String, ? extends GuiComponent> getComponents() {
      return this.components;
   }
}
