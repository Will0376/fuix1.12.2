package ru.will0376.fuix.library.client.ui.widget;

import net.minecraft.util.ResourceLocation;
import ru.will0376.fuix.library.client.ui.GuiComponent;
import ru.will0376.fuix.library.client.ui.MouseAction;
import ru.will0376.fuix.library.client.ui.Window;
import ru.will0376.fuix.library.client.ui.util.DrawHelper;
import ru.will0376.fuix.library.client.ui.util.FontHelper;

import java.awt.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Spinner extends GuiComponent {
   private final List options = new ArrayList();
   public boolean isPressed = false;
   public boolean isActive = false;
   public ListView listView;
   private Runnable callback = () -> System.out.println("test");
   private Option selected = null;
   private int maxHeight = 130;
   private int elementHeight = 13;

   public Spinner(String id, List options, Rectangle bounds, Window window) {
      super(id, bounds, window);
      this.listView = new ListView(id + "ListView", new Rectangle(bounds.x, bounds.y + 14, this.getBounds().width, this.getMaxHeight()), window);
      Iterator var5 = options.iterator();

      while (var5.hasNext()) {
         Option option = (Option) var5.next();
         this.listView.addItem(new ElementOption(option, this.getBounds().width, this.getElementHeight(), this.listView));
      }

      this.listView.getListViewContent().setElementsPadding(0);
      this.listView.getListViewContent().setColor1(new Color(0, 0, 0, 220));
      this.listView.getListViewContent().setColor2(new Color(255, 255, 255, 90));
      if (options.size() > 0) {
         this.selected = (Option) options.get(0);
      }

   }

   public void onInit(int x, int y) {
      this.addComponentSingle(new ListViewSlider(super.id + "ListViewSlider", this.listView));
   }

   protected void onDraw(int x, int y) {
      int posX = this.getAbsoluteBounds().x;
      int posY = this.getAbsoluteBounds().y;
      int width = this.getBounds().width;
      int height = this.getBounds().height;
      this.listView.setVisible(this.isActive);
      String text = this.getSelected() == null ? "\u041d\u0435 \u0432\u044b\u0431\u0440\u0430\u043d\u043d\u043e" : this.getSelected()
              .getName();
      DrawHelper.drawRectRounded(posX, posY, width, height, 1.0D, (new Color(255, 255, 255, 100)).hashCode());
      DrawHelper.drawRectRounded((double) posX + 0.3D, (double) posY + 0.3D, (double) width - 0.6D, (double) height - 0.6D, 1.0D, (new Color(0, 0, 0, 80))
              .hashCode());
      FontHelper.draw(text, posX + 4, posY + 2, (new Color(255, 255, 255)).hashCode(), 1.0F);
      DrawHelper.draw(new ResourceLocation("fuixlibrary", this.isActive ? "textures/gui/icons/ic_up.png" : "textures/gui/icons/ic_down.png"), posX + width - 8, posY + height / 2 - 1, 3.0D, 2.0D);
   }

   protected void onUpdate(int x, int y) {
      ArrayList children = this.listView.getChildren();

      for (int j = 0; j < children.size(); ++j) {
         this.getWindow().getComponentContainer().putOnTop(((GuiComponent) children.get(j)).getId());
      }

   }

   protected void handleMouseAction(MouseAction mouseAction) {
      if (mouseAction.getActionType().equals(MouseAction.ActionType.Click) && !this.isFocused() && !this.isFocused()) {
         this.isActive = false;
      }

   }

   protected void handleMouse(MouseAction mouseAction) {
      if (mouseAction.getActionType().equals(MouseAction.ActionType.Click)) {
         this.isPressed = true;
         this.isActive = !this.isActive;
      }

      if (mouseAction.getActionType().equals(MouseAction.ActionType.Click)) {
         this.isPressed = false;
      }

   }

   public Runnable getCallback() {
      return this.callback;
   }

   public Spinner setCallback(Runnable callback) {
      this.callback = callback;
      return this;
   }

   public Option getSelected() {
      return this.selected;
   }

   public Spinner setSelected(Option selected) {
      this.selected = selected;
      return this;
   }

   public int getMaxHeight() {
      return this.maxHeight;
   }

   public Spinner setMaxHeight(int maxHeight) {
      this.maxHeight = maxHeight;
      return this;
   }

   public int getElementHeight() {
      return this.elementHeight;
   }

   public Spinner setElementHeight(int elementHeight) {
      this.elementHeight = elementHeight;
      return this;
   }

   public static class Option {
      private String value;
      private String name;

      public Option(String value, String name) {
         this.value = value;
         this.name = name;
      }

      public String getValue() {
         return this.value;
      }

      public Option setValue(String value) {
         this.value = value;
         return this;
      }

      public String getName() {
         return this.name;
      }

      public Option setName(String name) {
         this.name = name;
         return this;
      }
   }

   public class ElementOption extends ListViewItem {
      private final Option option;

      public ElementOption(Option option, int width, int height, ListView listView) {
         super(width, height, listView);
         this.option = option;
      }

      public void onInit(int x, int y) {
         this.setEnabled(true);
      }

      protected void onItemDraw(int x, int y, int topLeftX, int topLeftY) {
         int posX = this.getAbsoluteBounds().x;
         int posY = this.getAbsoluteBounds().y;
         int width = this.getBounds().width;
         int height = this.getBounds().height;
         int paddingRight = 0;
         if (!Spinner.this.listView.getListViewSlider().isHidden()) {
            paddingRight = 2;
         }

         if (Spinner.this.getSelected().getName().equalsIgnoreCase(this.option.getName())) {
            DrawHelper.drawRect(posX, posY, width - paddingRight, height, (new Color(255, 255, 255, 60)).hashCode());
         } else if (this.isFocused()) {
            DrawHelper.drawRect(posX, posY, width - paddingRight, height, (new Color(255, 255, 255, 60)).hashCode());
         }

         DrawHelper.drawRect(posX, posY + height, width - paddingRight, 0.3D, (new Color(255, 255, 255, 60)).hashCode());
         FontHelper.draw(this.option.getName(), posX + 4, posY + 2, (new Color(255, 255, 255)).hashCode(), 1.0F);
      }

      protected void onUpdate(int x, int y) {
      }

      protected void handleMouse(MouseAction mouseAction) {
         if (mouseAction.getActionType().equals(MouseAction.ActionType.Click) && this.getAbsoluteBounds()
                 .contains(mouseAction.getPosX(), mouseAction.getPosY())) {
            Spinner.this.isActive = !Spinner.this.isActive;
            Spinner.this.setSelected(this.option);
            Spinner.this.callback.run();
         }

      }
   }
}
