package ru.will0376.fuix.library.client.ui.util;

import ru.will0376.fuix.library.client.ui.widget.ListView;
import ru.will0376.fuix.library.client.ui.widget.ListViewItem;

import java.util.ArrayList;

public abstract class ArrayListObserver {
	public int prevHashCode;
	private ArrayList observableList;
	private ListView listView;

	public ArrayListObserver() {
		this.observableList = new ArrayList();
		this.prevHashCode = 0;
	}

	public ArrayListObserver(ArrayList observableList) {
		this.observableList = observableList;
		this.prevHashCode = 0;
	}

	public void UpdateChanges(ArrayList arrayList) {
		if (arrayList != null) {
			if (arrayList.size() != this.listView.size()) {
				while (arrayList.size() != this.listView.size()) {
					if (arrayList.size() > this.listView.size()) {
						this.addItem();
					}

					if (arrayList.size() < this.listView.size()) {
						this.removeItem();
					}
				}

				this.prevHashCode = arrayList.hashCode();
				this.observableList = arrayList;
			}
		}
	}

	public ListView getListView() {
		return this.listView;
	}

	public void setListView(ListView listView) {
		if (this.listView == null) {
			this.listView = listView;
		}

	}

	private void addItem() {
		this.listView.addItem(this.getListViewItem());
	}

	private void removeItem() {
		this.listView.removeItem(this.listView.size() - 1);
	}

	public abstract ListViewItem getListViewItem();

	public ArrayList getObservableList() {
		return this.observableList;
	}
}
