package ru.will0376.fuix.library.client.notification;

public abstract class DrawNotification {
	public abstract void onDraw(Notification var1);
}
