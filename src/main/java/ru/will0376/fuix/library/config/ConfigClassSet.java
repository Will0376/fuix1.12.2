package ru.will0376.fuix.library.config;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD})
public @interface ConfigClassSet {
	String name() default "";

	String category() default "general";

	String comment() default "";

	String oldName() default "";

	String oldCategory() default "";
}
