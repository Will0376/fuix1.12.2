package ru.will0376.fuix.library.config;

import gnu.trove.set.TIntSet;
import gnu.trove.set.hash.TIntHashSet;
import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.common.registry.ForgeRegistries;
import net.minecraftforge.registries.IForgeRegistry;
import org.apache.commons.lang3.StringUtils;
import ru.will0376.fuix.Fuix;
import ru.will0376.fuix.library.server.ServerConfig;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.*;

public final class ItemBlockList {
   private static final String[] DEFAULT_VALUES = new String[]{"minecraft:bedrock", "modid:block_name@meta"};
   private static final char SEPARATOR = '@';
   private static final int ALL_META = -1;
   private final Set<String> rawSet;
   private final Map items;
   private final Map blocks;
   private boolean loaded;

   public ItemBlockList() {
      this(false);
   }

   public ItemBlockList(boolean initWithDefaultValues) {
      this.rawSet = new HashSet();
      this.items = new HashMap();
      this.blocks = new HashMap();
      this.loaded = true;
      if (initWithDefaultValues) {
         this.addRaw(Arrays.asList(DEFAULT_VALUES));
      }

   }

   private static boolean put(Map map, Object key, int value) {
      TIntSet set = (TIntSet) map.get(key);
      if (set == null) {
         map.put(key, set = new TIntHashSet());
      }

      return set.add(value);
   }

   private static boolean contains(Map map, Object key, int value) {
      TIntSet set = (TIntSet) map.get(key);
      return set != null && (set.contains(value) || set.contains(-1));
   }

   private static int safeParseInt(String s) {
      try {
         return Integer.parseInt(s);
      } catch (Throwable var2) {
         return -1;
      }
   }

   public void clear() {
      this.loaded = true;
      this.items.clear();
      this.blocks.clear();
      this.rawSet.clear();
   }

   public Set<String> getRaw() {
      return Collections.unmodifiableSet(this.rawSet);
   }

   public void addRaw(@Nonnull Collection<String> strings) {
      this.loaded = false;
      this.items.clear();
      this.blocks.clear();
      this.rawSet.addAll(strings);
   }

   public boolean isEmpty() {
      return this.items.isEmpty() && this.blocks.isEmpty();
   }

   public boolean contains(@Nullable ItemStack stack) {
      return stack != null && this.contains(stack.getItem(), stack.getItemDamage());
   }

   public boolean contains(@Nonnull Item item, int meta) {
      this.load();
      return item instanceof ItemBlock && this.contains(((ItemBlock) item).getBlock(), meta) || contains(this.items, item, meta);
   }

   public boolean contains(@Nonnull Block block, int meta) {
      this.load();
      return contains(this.blocks, block, meta);
   }

   private void load() {
      if (!this.loaded) {
         this.loaded = true;

         IForgeRegistry<Item> itemRegistry = ForgeRegistries.ITEMS;
         IForgeRegistry<Block> blockRegistry = ForgeRegistries.BLOCKS;
         Iterator var3 = this.rawSet.iterator();

         while (true) {
            String name;
            Item item;
            Block block;
            do {
               do {
                  do {
                     String[] parts;
                     do {
                        do {
                           String s;
                           do {
                              if (!var3.hasNext()) {
                                 return;
                              }

                              s = (String) var3.next();
                              s = s.trim();
                           } while (s.isEmpty());

                           parts = StringUtils.split(s, '@');
                        } while (parts == null);
                     } while (parts.length <= 0);

                     name = parts[0];
                     int meta = parts.length > 1 ? safeParseInt(parts[1]) : -1;
                     item = Item.getByNameOrId(name);/*(Item)itemRegistry.getObject(name);*/
                     if (item != null) {
                        put(this.items, item, meta);
                     }

                     block = Block.getBlockFromName(name);
                     if (block != null && block != Blocks.AIR) {
                        put(this.blocks, block, meta);
                     }
                  } while (!ServerConfig.debug);
               } while (item != null);
            } while (block != null && block != Blocks.AIR);

            Fuix.LOGGER.warn("Item/block {} not found", new Object[]{name});
         }
      }
   }
}
