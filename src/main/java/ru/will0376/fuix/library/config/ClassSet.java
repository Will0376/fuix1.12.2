package ru.will0376.fuix.library.config;

import com.google.common.base.Preconditions;
import ru.will0376.fuix.Fuix;
import ru.will0376.fuix.library.server.ServerConfig;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.stream.Collectors;

public final class ClassSet implements Iterable {
   private final Set<Class<?>> classes = new HashSet<>();
   private final Class<?> baseClass;

   public ClassSet(@Nonnull Class baseClass) {
      this.baseClass = baseClass;
      Preconditions.checkArgument(baseClass != Class.class, "baseClass must not be java.lang.Class");
   }

   public boolean isEmpty() {
      return this.classes.isEmpty();
   }

   public boolean contains(@Nullable Object instance) {
      return instance != null && this.contains(instance.getClass());
   }

   public boolean contains(@Nonnull Class clazz) {
      return this.contains(clazz, true);
   }

   public boolean contains(@Nullable Object instance, boolean checkHierarchy) {
      return instance != null && this.contains(instance.getClass(), checkHierarchy);
   }

   public boolean contains(@Nonnull Class clazz, boolean checkHierarchy) {
      if (this.baseClass.isAssignableFrom(clazz)) {
         if (this.classes.contains(clazz)) {
            return true;
         }

         if (checkHierarchy) {

            for (Class<?> value : this.classes) {
               if (value.isAssignableFrom(clazz)) {
                  return true;
               }
            }
         }
      }

      return false;
   }

   public Iterator iterator() {
      return this.classes.iterator();
   }

   public void clear() {
      this.classes.clear();
   }

   public boolean add(@Nonnull Class<?> clazz) {
      return this.baseClass.isAssignableFrom(clazz) && this.classes.add(clazz);
   }

   public void addRaw(@Nonnull Collection<String> classNames) {
      for (String className : classNames) {
         try {
            Class<?> clazz = Class.forName(className);
            if (this.baseClass.isAssignableFrom(clazz)) {
               this.add(clazz);
            } else if (ServerConfig.debug) {
               Fuix.LOGGER.warn("Class {} is not assignable from {}", new Object[]{className,
                       this.baseClass.getName()});
            }
         } catch (ClassNotFoundException var5) {
            if (ServerConfig.debug) {
               Fuix.LOGGER.warn("Class {} not found", new Object[]{className});
            }
         }
      }

   }

   public Set<String> getRaw() {
      return this.classes.stream().map(Class::getName).collect(Collectors.toSet());
   }
}
