package ru.will0376.fuix.library.config;

import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import net.minecraftforge.common.config.ConfigCategory;
import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.common.config.Property;
import ru.will0376.fuix.Fuix;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.File;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.*;

public final class ConfigUtils {
   private static final String PACKAGE_DEFAULT = "default";
   private static final Set<Class<?>> LOADED_CONFIG_CLASSES = new HashSet<>();

   public static Set<String> reloadAllConfigs() {
      Set<String> configNames = new TreeSet<>();

      for (Class<?> loadedConfigClass : LOADED_CONFIG_CLASSES) {

         try {
            readConfig(loadedConfigClass, true);
            configNames.add(getConfigName(loadedConfigClass));
         } catch (Throwable var4) {
         }
      }

      return configNames;
   }

   @Nonnull
   public static Collection<String> readStringCollection(@Nonnull Configuration cfg, @Nonnull String name, @Nonnull String category, @Nonnull String comment, @Nonnull Collection def) {
      String[] temp = cfg.getStringList(name, category, (String[]) def.toArray(new String[0]), comment);
      def.clear();
      Collections.addAll(def, temp);
      return def;
   }

   @Nonnull
   public static Configuration getConfig(@Nonnull Class configClass) {
      return getConfig(getConfigName(configClass));
   }

   @Nonnull
   public static Configuration getConfig(@Nonnull String cfgName) {
      Configuration cfg = new Configuration(new File(Fuix.CFGDIR, cfgName + ".cfg"));
      cfg.load();
      return cfg;
   }

   public static void readConfig(@Nonnull Class<?> configClass) {
      readConfig(configClass, false);
   }

   public static void readConfig(@Nonnull Class<?> configClass, @Nonnull String configName) {
      readConfig(configClass, configName, false);
   }

   public static void readConfig(@Nonnull Class<?> configClass, boolean reload) {
      readConfig(configClass, getConfigName(configClass), reload);
   }

   public static void readConfig(@Nonnull Class<?> configClass, @Nonnull String configName, boolean reload) {
      if (LOADED_CONFIG_CLASSES.add(configClass) || reload) {
         Configuration cfg = getConfig(configName);

         try {
            Field[] var4 = configClass.getDeclaredFields();
            int var5 = var4.length;

            for (Field field : var4) {
               try {
                  readConfigProperty(cfg, field);
               } catch (Throwable var9) {
                  Fuix.LOGGER.error("Failed reading property {} in config {}", new Object[]{field,
                          cfg.getConfigFile().getName(), var9});
               }
            }
         } catch (Throwable var10) {
            Fuix.LOGGER.error("Failed reading config {}", new Object[]{cfg.getConfigFile().getName(), var10});
         }

         cfg.save();
      }
   }

   private static void readConfigProperty(@Nonnull Configuration cfg, @Nonnull Field field) throws IllegalAccessException {
      if (Modifier.isStatic(field.getModifiers())) {
         field.setAccessible(true);
         Annotation[] var2 = field.getDeclaredAnnotations();
         int var3 = var2.length;

         for (Annotation declaredAnnotation : var2) {
            Class<?> annotationType = declaredAnnotation.annotationType();
            String name;
            if (annotationType == ConfigBoolean.class) {
               checkType(field, Boolean.TYPE);
               checkNotFinal(field);
               ConfigBoolean annotation = (ConfigBoolean) declaredAnnotation;
               name = annotation.name().isEmpty() ? field.getName() : annotation.name();
               tryMoveProperty(cfg, name, annotation.category(), annotation.oldName(), annotation.oldCategory());
               boolean defaultValue = field.getBoolean(null);
               boolean value = cfg.getBoolean(name, annotation.category(), defaultValue, annotation.comment());
               field.setBoolean(null, value);
            } else if (annotationType == ConfigFloat.class) {
               checkType(field, Float.TYPE);
               checkNotFinal(field);
               ConfigFloat annotation = (ConfigFloat) declaredAnnotation;
               name = annotation.name().isEmpty() ? field.getName() : annotation.name();
               tryMoveProperty(cfg, name, annotation.category(), annotation.oldName(), annotation.oldCategory());
               float defaultValue = field.getFloat(null);
               float value = cfg.getFloat(name, annotation.category(), defaultValue, annotation.min(), annotation.max(), annotation
                       .comment());
               field.setFloat(null, value);
            } else if (annotationType == ConfigInt.class) {
               checkType(field, Integer.TYPE);
               checkNotFinal(field);
               ConfigInt annotation = (ConfigInt) declaredAnnotation;
               name = annotation.name().isEmpty() ? field.getName() : annotation.name();
               tryMoveProperty(cfg, name, annotation.category(), annotation.oldName(), annotation.oldCategory());
               int defaultValue = field.getInt(null);
               int value = cfg.getInt(name, annotation.category(), defaultValue, annotation.min(), annotation.max(), annotation
                       .comment());
               field.setInt(null, value);
            } else {
               String valueName;
               if (annotationType == ConfigString.class) {
                  checkType(field, String.class);
                  checkNotFinal(field);
                  ConfigString annotation = (ConfigString) declaredAnnotation;
                  name = annotation.name().isEmpty() ? field.getName() : annotation.name();
                  tryMoveProperty(cfg, name, annotation.category(), annotation.oldName(), annotation.oldCategory());
                  String defaultValue = (String) field.get(null);
                  valueName = cfg.getString(name, annotation.category(), defaultValue, annotation.comment());
                  field.set(null, valueName);
               } else {
                  Set<String> values;
                  if (annotationType == ConfigItemBlockList.class) {
                     checkType(field, ItemBlockList.class);
                     ConfigItemBlockList annotation = (ConfigItemBlockList) declaredAnnotation;
                     name = annotation.name().isEmpty() ? field.getName() : annotation.name();
                     tryMoveProperty(cfg, name, annotation.category(), annotation.oldName(), annotation.oldCategory());
                     ItemBlockList list = (ItemBlockList) field.get(null);
                     Objects.requireNonNull(list, field + " value must not be null");
                     values = (Set) readStringCollection(cfg, name, annotation.category(), annotation.comment(), new HashSet(list
                             .getRaw()));
                     list.clear();
                     list.addRaw(values);
                  } else if (annotationType == ConfigClassSet.class) {
                     checkType(field, ClassSet.class);
                     ConfigClassSet annotation = (ConfigClassSet) declaredAnnotation;
                     name = annotation.name().isEmpty() ? field.getName() : annotation.name();
                     tryMoveProperty(cfg, name, annotation.category(), annotation.oldName(), annotation.oldCategory());
                     ClassSet classSet = (ClassSet) field.get(null);
                     Objects.requireNonNull(classSet, field + " value must not be null");
                     values = (Set) readStringCollection(cfg, name, annotation.category(), annotation.comment(), new HashSet(classSet
                             .getRaw()));
                     classSet.clear();
                     classSet.addRaw(values);
                  } else if (annotationType == ConfigEnum.class) {
                     checkType(field, Enum.class);
                     checkNotFinal(field);
                     ConfigEnum annotation = (ConfigEnum) declaredAnnotation;
                     name = annotation.name().isEmpty() ? field.getName() : annotation.name();
                     tryMoveProperty(cfg, name, annotation.category(), annotation.oldName(), annotation.oldCategory());
                     Enum defaultValue = (Enum) field.get(null);
                     Objects.requireNonNull(defaultValue, field + " value must not be null");
                     valueName = cfg.getString(name, annotation.category(), defaultValue.name(), annotation.comment());

                     try {
                        Enum value = Enum.valueOf(defaultValue.getDeclaringClass(), valueName);
                        field.set(null, value);
                     } catch (IllegalArgumentException var12) {
                        var12.printStackTrace();
                     }
                  } else if (annotationType == ConfigStringCollection.class) {
                     checkType(field, Collection.class);
                     ConfigStringCollection annotation = (ConfigStringCollection) declaredAnnotation;
                     name = annotation.name().isEmpty() ? field.getName() : annotation.name();
                     tryMoveProperty(cfg, name, annotation.category(), annotation.oldName(), annotation.oldCategory());
                     Collection collection = (Collection) field.get(null);
                     Objects.requireNonNull(collection, field + " value must not be null");
                     readStringCollection(cfg, name, annotation.category(), annotation.comment(), collection);
                  }
               }
            }
         }
      }

   }

   private static void checkType(@Nonnull Field field, @Nonnull Class<?> expectedType) {
      Class<?> type = field.getType();
      Preconditions.checkArgument(expectedType == type || expectedType.isAssignableFrom(type), field + " type must be " + expectedType + " ( real type is " + type + ')');
   }

   private static void checkNotFinal(@Nonnull Field field) {
      int modifiers = field.getModifiers();
      Preconditions.checkArgument(!Modifier.isFinal(modifiers), field + " must not be final");
   }

   private static boolean tryMoveProperty(@Nonnull Configuration cfg, @Nonnull String newName, @Nonnull String newCategory, @Nullable String oldName, @Nullable String oldCategory) {
      if (!newName.isEmpty() && !newCategory.isEmpty()) {
         if (Strings.isNullOrEmpty(oldCategory)) {
            oldCategory = newCategory;
         }

         if (Strings.isNullOrEmpty(oldName)) {
            oldName = newName;
         }

         if (newName.equalsIgnoreCase(oldName) && newCategory.equalsIgnoreCase(oldCategory)) {
            return false;
         } else if (!cfg.hasKey(newCategory, newName) && cfg.hasKey(oldCategory, oldName)) {
            ConfigCategory prevCategory = cfg.getCategory(oldCategory);
            if (prevCategory.containsKey(oldName)) {
               Property property = prevCategory.remove(oldName);
               property.setName(newName);
               ConfigCategory category;
               if (newCategory.equalsIgnoreCase(oldCategory)) {
                  category = prevCategory;
               } else {
                  category = cfg.getCategory(newCategory);
                  if (prevCategory.isEmpty()) {
                     cfg.removeCategory(prevCategory);
                  }
               }

               category.put(newName, property);
               return true;
            } else {
               return false;
            }
         } else {
            return false;
         }
      } else {
         return false;
      }
   }

   @Nonnull
   private static String getConfigName(@Nonnull Class<?> configClass) {
      Config annotation = configClass.getAnnotation(Config.class);
      Objects.requireNonNull(annotation, "Annotaion " + Config.class.getName() + " not found for class " + configClass.getName());
      String cfgName = annotation.name();
      if (Strings.isNullOrEmpty(cfgName)) {
         cfgName = getPackageName(configClass.getName());
      }

      Preconditions.checkArgument(!Strings.isNullOrEmpty(cfgName), "Config name for class " + configClass.getName() + " is not determined");
      return cfgName;
   }

   @Nonnull
   private static String getPackageName(@Nullable String className) {
      if (Strings.isNullOrEmpty(className)) {
         return "default";
      } else {
         int classDelimeterIndex = className.lastIndexOf(46);
         if (classDelimeterIndex == -1) {
            return "default";
         } else {
            String packageName = className.substring(0, classDelimeterIndex);
            if (Strings.isNullOrEmpty(packageName)) {
               return "default";
            } else {
               int packageDelimeterIndex = packageName.lastIndexOf(46);
               if (packageDelimeterIndex == -1) {
                  return packageName;
               } else {
                  String simplePackageName = packageName.substring(packageDelimeterIndex + 1);
                  return Strings.isNullOrEmpty(simplePackageName) ? "default" : simplePackageName;
               }
            }
         }
      }
   }
}
