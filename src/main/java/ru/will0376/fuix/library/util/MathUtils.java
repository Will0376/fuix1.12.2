package ru.will0376.fuix.library.util;

public class MathUtils {
   private static final int BIG_ENOUGH_INT = 16384;
   private static final double BIG_ENOUGH_FLOOR = 16384.0D;
   private static final double CEIL = 0.9999999D;
   private static final double BIG_ENOUGH_CEIL = 16384.999999999996D;
   private static final double BIG_ENOUGH_ROUND = 16384.5D;
   private static final int SIN_BITS = 14;
   private static final int SIN_MASK = 16383;
   private static final int SIN_COUNT = 16384;
   private static final float PI = 3.1415927F;
   private static final float radFull = 6.2831855F;
   private static final float degFull = 360.0F;
   private static final float radToIndex = 2607.5945F;
   private static final float degToIndex = 45.511112F;
   private static final float degreesToRadians = 0.017453292F;

   public static int floor(double val) {
      return (int) (val + 1024.0D) - 1024;
   }

   public static float wrapDegrees(float value) {
      value %= 360.0F;
      if (value >= 180.0F) {
         value -= 360.0F;
      }

      if (value < -180.0F) {
         value += 360.0F;
      }

      return value;
   }

   public static float wrapDegrees(double value) {
      return wrapDegrees((float) value);
   }

   public static int ceil(float value) {
      return 16384 - (int) (16384.0D - (double) value);
   }

   public static int ceil(double value) {
      return 16384 - (int) (16384.0D - value);
   }

   public static float sqrt(float value) {
      return (float) Math.sqrt(value);
   }

   public static float sqrt(double value) {
      return (float) Math.sqrt(value);
   }

   public static double interpolate(double prev, double cur, double delta) {
      return prev + (cur - prev) * delta;
   }

   public static float sin(float radians) {
      return Sin.table[(int) (radians * 2607.5945F) & 16383];
   }

   public static float sin(double radians) {
      return Sin.table[(int) (radians * 2607.594482421875D) & 16383];
   }

   public static float cos(float radians) {
      return Sin.table[(int) ((radians + 1.5707964F) * 2607.5945F) & 16383];
   }

   public static float cos(double radians) {
      return Sin.table[(int) ((radians + 1.5707963705062866D) * 2607.594482421875D) & 16383];
   }

   public static float clamp(float input, float max) {
      return clamp(input, max, -max);
   }

   public static float clamp(float input, float max, float min) {
      if (input > max) {
         input = max;
      }

      if (input < min) {
         input = min;
      }

      return input;
   }

   public static int abs(int input) {
      return input >= 0 ? input : -input;
   }

   private static class Sin {
      static final float[] table = new float[16384];

      static {
         int i;
         for (i = 0; i < 16384; ++i) {
            table[i] = (float) Math.sin(((float) i + 0.5F) / 16384.0F * 6.2831855F);
         }

         for (i = 0; i < 360; i += 90) {
            table[(int) ((float) i * 45.511112F) & 16383] = (float) Math.sin((float) i * 0.017453292F);
         }

      }
   }
}
