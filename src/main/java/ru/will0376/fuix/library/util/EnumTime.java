package ru.will0376.fuix.library.util;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public enum EnumTime {
   SEC('s', 1, "\u0441\u0435\u043a\u0443\u043d\u0434\u0443", "\u0441\u0435\u043a\u0443\u043d\u0434\u044b", "\u0441\u0435\u043a\u0443\u043d\u0434", 60),
   MIN('m', 60, "\u043c\u0438\u043d\u0443\u0442\u0443", "\u043c\u0438\u043d\u0443\u0442\u044b", "\u043c\u0438\u043d\u0443\u0442", 60),
   HOUR('h', 3600, "\u0447\u0430\u0441", "\u0447\u0430\u0441\u0430", "\u0447\u0430\u0441\u043e\u0432", 24),
   DAY('d', 86400, "\u0434\u0435\u043d\u044c", "\u0434\u043d\u044f", "\u0434\u043d\u0435\u0439", 365),
   YEAR('y', 31536000, "\u0433\u043e\u0434", "\u0433\u043e\u0434\u0430", "\u043b\u0435\u0442", 999);

   public final char letter;
   public final int seconds;
   public final String firstForm;
   public final String secondForm;
   public final String thirdForm;
   public final int limit;

   EnumTime(char letter, int seconds, String firstForm, String secondForm, String thirdForm, int limit) {
      this.letter = letter;
      this.seconds = seconds;
      this.firstForm = firstForm;
      this.secondForm = secondForm;
      this.thirdForm = thirdForm;
      this.limit = limit;
   }

   private static int getTimeFromRaw(String raw) {
      EnumTime time = getEnumTime(raw.charAt(raw.length() - 1));
      int multiply = Integer.parseInt(raw.substring(0, raw.length() - 1));
      return time.seconds * multiply;
   }

   private static EnumTime getEnumTime(char c) {
      EnumTime[] var1 = values();
      int var2 = var1.length;

      for (EnumTime time : var1) {
         if (time.letter == c) {
            return time;
         }
      }

      return null;
   }

   private static String getAllowedChars() {
      StringBuilder str = new StringBuilder();
      EnumTime[] var1 = values();
      int var2 = var1.length;

      for (EnumTime time : var1) {
         str.append(time.letter);
      }

      return str.toString();
   }

   public static String timeToString(long time) {
      StringBuilder timeString = new StringBuilder();

      for (int i = values().length - 1; i >= 0; --i) {
         EnumTime enumTime = values()[i];
         long timeAmmount = time / (long) enumTime.seconds;
         timeAmmount %= enumTime.limit;
         if (timeAmmount > 0L) {
            if ((timeString.toString().trim().contains("\u0434\u043d\u0435") || timeString.toString()
                    .trim()
                    .contains("\u0434\u043d\u044f")) && timeString.toString()
                    .trim()
                    .contains("\u0447\u0430\u0441") && timeString.toString().trim().contains("\u043c\u0438\u043d")) {
               return timeString.toString().trim();
            }

            timeString.append(timeAmmount).append(" ").append(enumTime.getEndForNum(timeAmmount)).append(" ");
         }
      }

      return timeString.toString().trim();
   }

   public static int getTimeFromRawInput(String raw) {
      List<String> allMatches = new ArrayList<>();
      String pattern = String.format("([0-9]+[%s])", getAllowedChars());
      Matcher m = Pattern.compile(pattern).matcher(raw);

      while (m.find()) {
         allMatches.add(m.group());
      }

      long totalTime = 0L;

      String string;
      for (Iterator<String> var6 = allMatches.iterator(); var6.hasNext(); totalTime += getTimeFromRaw(string)) {
         string = var6.next();
      }

      return (int) totalTime;
   }

   private String getEndForNum(long time) {
      if (time % 10L == 0L) {
         return this.thirdForm;
      } else if (time != 1L && (time % 100L == 11L || time % 10L != 1L)) {
         return time >= 5L && (time % 100L >= 10L && time % 100L <= 20L || time % 10L >= 5L) ? this.thirdForm : this.secondForm;
      } else {
         return this.firstForm;
      }
   }
}
