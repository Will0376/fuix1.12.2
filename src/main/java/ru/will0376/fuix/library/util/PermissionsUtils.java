package ru.will0376.fuix.library.util;

import com.gamerforea.eventhelper.util.EventUtils;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import ru.will0376.fuix.library.server.ServerConfig;

@SideOnly(Side.SERVER)
public class PermissionsUtils {
   public static boolean isBukkit() {
      try {
         if (Bukkit.getServer() != null) {
            return true;
         }
      } catch (Exception ignored) {
      }

      return false;
   }

   public static Player toBukkitPlayer(EntityPlayer player) {
      try {
         if (Bukkit.getServer() != null) {
            return Bukkit.getServer().getPlayer(player.getName());
         }
      } catch (Exception ignored) {
      }

      return null;
   }

   public static boolean hasPex(EntityPlayer player, String s) {
      return ServerConfig.debugPermissions || FMLCommonHandler.instance()
              .getMinecraftServerInstance()
              .getPlayerList()
              .getOppedPlayers()
              .getEntry(player.getGameProfile()) != null || EventUtils.hasPermission(player, s);
/*
      if (ServerConfig.debugPermissions || FMLCommonHandler.instance()
              .getMinecraftServerInstance()
              .getPlayerList()
              .getOppedPlayers()
              .getEntry(player.getGameProfile()) != null) {
         return true;
      } else {
         boolean use = false;
         Player p = toBukkitPlayer(player);
         if (p != null && p.hasPermission(s)) {
            use = true;
         }

         return use;
      }
*/
   }
}
