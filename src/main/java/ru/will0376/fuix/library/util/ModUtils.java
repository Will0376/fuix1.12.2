package ru.will0376.fuix.library.util;


import net.minecraftforge.fml.common.API;
import net.minecraftforge.fml.common.Loader;
import net.minecraftforge.fml.common.ModContainer;
import net.minecraftforge.fml.common.versioning.ArtifactVersion;
import net.minecraftforge.fml.common.versioning.DefaultArtifactVersion;
import net.minecraftforge.fml.common.versioning.VersionParser;
import net.minecraftforge.fml.common.versioning.VersionRange;

public class ModUtils {
   public static boolean isModLoaded(String modname) {
      return Loader.isModLoaded(modname);
   }

   public static boolean isModLoaded(String modname, String versionRangeString) {
      if (!isModLoaded(modname)) {
         return false;
      } else {
         if (versionRangeString != null) {
            ModContainer mod = Loader.instance().getIndexedModList().get(modname);
            ArtifactVersion modVersion = mod.getProcessedVersion();
            VersionRange versionRange = VersionParser.parseRange(versionRangeString);
            DefaultArtifactVersion requiredVersion = new DefaultArtifactVersion(modname, versionRange);
            return requiredVersion.containsVersion(modVersion);
         }

         return true;
      }
   }

   public static boolean isAPILoaded(String apiName) {
      return isAPILoaded(apiName, null);
   }

   public static boolean isAPILoaded(String apiName, String versionRangeString) {
      Package apiPackage = Package.getPackage(apiName);
      if (apiPackage == null) {
         return false;
      } else {
         API apiAnnotation = apiPackage.getAnnotation(API.class);
         if (apiAnnotation == null) {
            return false;
         } else {
            if (versionRangeString != null) {
               String apiVersionString = apiAnnotation.apiVersion();
               if (apiVersionString == null) {
                  return false;
               }

               VersionRange versionRange = VersionParser.parseRange(versionRangeString);
               DefaultArtifactVersion givenVersion = new DefaultArtifactVersion(apiName, apiVersionString);
               DefaultArtifactVersion requiredVersion = new DefaultArtifactVersion(apiName, versionRange);
               return requiredVersion.containsVersion(givenVersion);
            }

            return true;
         }
      }
   }
}
