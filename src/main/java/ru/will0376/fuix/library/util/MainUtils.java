package ru.will0376.fuix.library.util;

import net.minecraft.block.Block;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.JsonToNBT;
import net.minecraft.nbt.NBTException;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class MainUtils {
	public static String pluralForm(long value, String form1, String form2, String form5) {
		value = Math.abs(value) % 100L;
		long value1 = value % 10L;
		if (value > 10L && value < 20L) {
			return form5;
		} else if (value1 > 1L && value1 < 5L) {
			return form2;
		} else {
			return value1 == 1L ? form1 : form5;
		}
	}

	public static boolean isEqualTo(ItemStack stack1, ItemStack stack2) {
		return (stack1 != null || stack2 != null) && stack1.getItem() == stack2.getItem() && stack1.getItemDamage() == stack2
				.getItemDamage();
	}

	public static boolean isEqualTo(Block block1, Block block2) {
		return block1 != null && block2 != null && block1 == block2;
	}

	public static ItemStack createItem(String item_str, int amount, int data, String dataTag) {
		Item item = Item.getByNameOrId(item_str);
		if (item == null) {
			try {
				item = Item.getItemById(Integer.parseInt(item_str));
			} catch (NumberFormatException ignored) {
			}
		}

		if (item != null) {
			ItemStack itemstack = new ItemStack(item, amount, data);
			if (!dataTag.isEmpty()) {
				try {
					NBTTagCompound nbtbase = JsonToNBT.getTagFromJson(dataTag);
					itemstack.setTagCompound(nbtbase);
				} catch (NBTException var7) {
					return itemstack;
				}
			}

			return itemstack;
		} else {
			return null;
		}
	}

	@SideOnly(Side.SERVER)
	public static boolean giveItem(EntityPlayer entityplayermp, String item_str, int amount, int data, String dataTag) {
      /*Item item = Item.getByNameOrId(item_str);
      if (item == null) {
         try {
            item = Item.getItemById(Integer.parseInt(item_str));
         } catch (NumberFormatException ignored) {
         }
      }*/

		ItemStack is = createItem(item_str, amount, data, dataTag);
		if (is != null) {
			if (!entityplayermp.inventory.addItemStackToInventory(is)) {
				entityplayermp.sendMessage(new TextComponentString(TextFormatting.GREEN + "[CenturyMine]" + TextFormatting.RED + " Освободите место в инвентаре! "));
				return false;
			} else {
				return true;
			}
		} else {
			return false;
		}
	}
}
