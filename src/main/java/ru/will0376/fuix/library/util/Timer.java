package ru.will0376.fuix.library.util;

public final class Timer {
	protected long lastCheck = getSystemTime();

	public static long getSystemTime() {
		return System.nanoTime() / 1000000L;
	}

	public boolean hasReach(int targetTime) {
		return this.getTimePassed() >= (long) targetTime;
	}

	public boolean hasReach(TimeUnit unit, int targetTime) {
		return this.getTimePassed() >= (long) (targetTime * unit.multiplier);
	}

	public long getTimePassed() {
		return getSystemTime() - this.lastCheck;
	}

	public void reset() {
		this.lastCheck = getSystemTime();
	}

	public enum TimeUnit {
		MILLISECONDS(1),
		SECONDS(1000);

		public final int multiplier;

		TimeUnit(int multiplier) {
			this.multiplier = multiplier;
		}
	}
}
