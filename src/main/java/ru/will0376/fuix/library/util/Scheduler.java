package ru.will0376.fuix.library.util;

import com.google.common.primitives.Ints;
import com.google.common.util.concurrent.AbstractListeningExecutorService;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.relauncher.Side;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.TimeUnit;

public final class Scheduler extends AbstractListeningExecutorService {
   private static final Scheduler server;
   private static final Scheduler client;

   static {
      if (FMLCommonHandler.instance().getSide().isClient()) {
         client = new Scheduler();
      } else {
         client = null;
      }

      server = new Scheduler();
   }

   private final ConcurrentLinkedQueue inputQueue = new ConcurrentLinkedQueue();
   private Task[] activeTasks = new Task[5];
   private int size = 0;

   private Scheduler() {
   }

   public static Scheduler server() {
      return server;
   }

   public static Scheduler client() {
      return client;
   }

   public static Scheduler forSide(Side side) {
      return side == Side.CLIENT ? client : server;
   }

   private static boolean checkedExecute(Task task) {
      try {
         return task.execute();
      } catch (Throwable var2) {
         System.err.println(String.format("Exception thrown during execution of %s", task) + ": " + var2);
         return false;
      }
   }

   public void schedule(Runnable r, long tickDelay, long tickPeriod) {
      this.execute(new runTaskTimer(r, tickDelay, tickPeriod));
   }

   public void schedule(Runnable r, long tickDelay) {
      this.execute(new runTaskLater(r, tickDelay));
   }

   public void execute(Runnable task) {
      this.execute(new runTask(task));
   }

   public void execute(Task task) {
      this.inputQueue.offer(task);
   }

   public void tick() {
      int idx = 0;

      int free;
      Task task;
      for (free = -1; idx < this.size; ++idx) {
         task = this.activeTasks[idx];
         if (!checkedExecute(task)) {
            this.activeTasks[idx] = null;
            if (free == -1) {
               free = idx;
            }
         } else if (free != -1) {
            this.activeTasks[free++] = task;
            this.activeTasks[idx] = null;
         }
      }

      if (free != -1) {
         this.size = free;
      }

      while ((task = (Task) this.inputQueue.poll()) != null) {
         if (checkedExecute(task)) {
            if (this.size == this.activeTasks.length) {
               Task[] newArr = new Task[this.size << 1];
               System.arraycopy(this.activeTasks, 0, newArr, 0, this.size);
               this.activeTasks = newArr;
            }

            this.activeTasks[this.size] = task;
            ++this.size;
         }
      }

   }

   /**
    * @deprecated
    */
   @Deprecated
   public boolean isShutdown() {
      return false;
   }

   /**
    * @deprecated
    */
   @Deprecated
   public boolean isTerminated() {
      return false;
   }

   /**
    * @deprecated
    */
   @Deprecated
   public void shutdown() {
   }

   /**
    * @deprecated
    */
   @Deprecated
   public List shutdownNow() {
      return Collections.emptyList();
   }

   /**
    * @deprecated
    */
   @Deprecated
   public boolean awaitTermination(long timeout, TimeUnit unit) throws InterruptedException {
      long millis = unit.toMillis(timeout);
      long milliNanos = TimeUnit.MILLISECONDS.toNanos(millis);
      int additionalNanos = Ints.saturatedCast(unit.toNanos(timeout) - milliNanos);
      Thread.sleep(millis, additionalNanos);
      return false;
   }

   public interface Task {
      boolean execute();
   }

   private static final class runTaskTimer implements Task {
      private final Runnable task;
      private final long tickDelay;
      private final long tickPeriod;
      private long delay;
      private long period;

      public runTaskTimer(Runnable task, long tickDelay, long tickPeriod) {
         this.task = task;
         this.delay = tickDelay;
         this.period = tickPeriod;
         this.tickDelay = tickDelay;
         this.tickPeriod = tickPeriod;
      }

      public boolean execute() {
         if (this.period == 0L) {
            if (this.delay == 0L) {
               this.task.run();
               this.delay = this.tickDelay;
               this.period = this.tickPeriod;
            } else {
               --this.delay;
            }
         } else {
            --this.period;
         }

         return true;
      }

      public String toString() {
         return String.format("Scheduled task (task=%s, delayTicks=%s, periodTicks=%s)", this.task, this.delay, this.period);
      }
   }

   private static final class runTaskLater implements Task {
      private final Runnable task;
      private long tickDelay;

      public runTaskLater(Runnable task, long tickDelay) {
         this.task = task;
         this.tickDelay = tickDelay;
      }

      public boolean execute() {
         if (this.tickDelay == 0L) {
            this.task.run();
            return false;
         } else {
            --this.tickDelay;
            return true;
         }
      }

      public String toString() {
         return String.format("Scheduled task (task=%s, remainingTicks=%s)", this.task, this.tickDelay);
      }
   }

   private static class runTask implements Task {
      private final Runnable task;

      public runTask(Runnable task) {
         this.task = task;
      }

      public boolean execute() {
         this.task.run();
         return false;
      }

      public String toString() {
         return this.task.toString();
      }
   }
}
