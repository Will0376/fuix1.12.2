package ru.will0376.fuix.library.util;


import net.minecraftforge.fml.common.event.FMLConstructionEvent;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;

public interface ISideCommon {
	void onConstruction(FMLConstructionEvent var1);

	void onInit(FMLInitializationEvent var1);

	void onPreInit(FMLPreInitializationEvent var1);

	void onPostInit(FMLPostInitializationEvent var1);
}
