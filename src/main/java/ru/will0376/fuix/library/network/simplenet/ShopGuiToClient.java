package ru.will0376.fuix.library.network.simplenet;

import io.netty.buffer.ByteBuf;
import net.minecraft.client.Minecraft;
import net.minecraft.client.network.NetHandlerPlayClient;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import ru.will0376.fuix.library.client.ui.GuiScreenAdapter;
import ru.will0376.fuix.library.client.ui.Window;
import ru.will0376.fuix.library.network.FXPacket;
import ru.will0376.fuix.library.network.IGuiResult;
import ru.will0376.fuix.library.network.packet.data.ResultGui;
import ru.will0376.fuix.shop.client.ui.GuiShop;
import ru.will0376.fuix.shop.network.data.Product;
import ru.will0376.fuix.shop.network.data.ShopEnchant;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ShopGuiToClient extends FXPacket {
	public ResultGui result = null;
	public Type type = Type.NONE;
	public boolean editMode = false;
	public List<Product> shopProduct = new ArrayList<>();
	public List<ShopEnchant> shopEnchant = new ArrayList<>();
	public Product product = null;

	public ShopGuiToClient() {
	}

	public ShopGuiToClient(Type type, ResultGui result) {
		this.type = type;
		this.result = result;
	}

	public ShopGuiToClient(Type type, List<Product> shopProduct, List<ShopEnchant> shopEnchant) {
		this.type = type;
		this.shopProduct = shopProduct;
		this.shopEnchant = shopEnchant;
	}

	public ShopGuiToClient(Type type, Product product, List<ShopEnchant> shopEnchant) {
		this.type = type;
		this.product = product;
		this.shopEnchant = shopEnchant;
	}

	public ShopGuiToClient(Type type, Product product) {
		this.type = type;
		this.product = product;
	}

	@Override
	public void write(ByteBuf buf) throws IOException {
		ByteBufUtils.writeUTF8String(buf, type.name());
		if (type == Type.GUISHOP) {
			buf.writeBoolean(editMode);

			buf.writeInt(shopProduct.size());
			for (Product item : shopProduct) {
				item.write(buf);
			}

			buf.writeInt(shopEnchant.size());
			for (ShopEnchant item : shopEnchant) {
				item.write(buf);
			}
		} else if (type == Type.EDIT || type == Type.REMOVE) {
			product.write(buf);
		} else if (type == Type.BUY) {
			this.product.write(buf);
			buf.writeInt(shopEnchant.size());
			for (ShopEnchant item : shopEnchant) {
				item.write(buf);
			}
		} else if (type == Type.RESULTCODE) result.write(buf);
	}

	@Override
	public void read(ByteBuf buf) throws IOException {
		type = Type.valueOf(ByteBufUtils.readUTF8String(buf));
		if (type == Type.GUISHOP) {
			editMode = buf.readBoolean();

			int size = buf.readInt();
			shopProduct = size == 0 ? Collections.emptyList() : new ArrayList<>(size);
			for (int i = 0; i < size; i++) {
				shopProduct.add(Product.read(buf));
			}

			size = buf.readInt();
			shopEnchant = size == 0 ? Collections.emptyList() : new ArrayList<>(size);
			for (int i = 0; i < size; i++) {
				shopEnchant.add(ShopEnchant.read(buf));
			}
		} else if (type == Type.EDIT || type == Type.REMOVE) {
			product = Product.read(buf);
		} else if (type == Type.BUY) {
			this.product = Product.read(buf);
			int size = buf.readInt();
			shopEnchant = size == 0 ? Collections.emptyList() : new ArrayList<>(size);
			for (int i = 0; i < size; i++) {
				shopEnchant.add(ShopEnchant.read(buf));
			}
		} else if (type == Type.RESULTCODE) this.result = ResultGui.read(buf);
	}

	@SideOnly(Side.CLIENT)
	@Override
	public void processClient(IMessage message, NetHandlerPlayClient net) {
		if (getCasted(message).type == Type.GUISHOP) {
			Minecraft.getMinecraft()
					.displayGuiScreen(new GuiScreenAdapter(new GuiShop(getCasted(message).editMode, getCasted(message).shopProduct, getCasted(message).shopEnchant)));
		} else if (getCasted(message).type == Type.RESULTCODE) {
			net.minecraft.client.gui.GuiScreen currentScreen = Minecraft.getMinecraft().currentScreen;
			if (currentScreen instanceof GuiScreenAdapter) {
				Window gui = ((GuiScreenAdapter) currentScreen).getWindow().getModalWindow();
				if (gui instanceof IGuiResult) {
					((IGuiResult) gui).onResultGui(getCasted(message).result);
				}
			}
		}
	}

	@Override
	public ShopGuiToClient getCasted(IMessage message) {
		return (ShopGuiToClient) message;
	}

	public ShopGuiToClient setEditMode(boolean editMode) {
		this.editMode = editMode;
		return this;
	}

	public enum Type {
		NONE,
		BUY,
		EDIT,
		REMOVE,
		RESULTCODE,
		GUISHOP,
	}
}
