package ru.will0376.fuix.library.network;

import io.netty.buffer.ByteBuf;
import net.minecraft.client.Minecraft;
import net.minecraft.client.network.NetHandlerPlayClient;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.network.NetHandlerPlayServer;
import net.minecraft.world.WorldServer;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.network.NetworkRegistry;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import ru.will0376.fuix.Fuix;

import java.io.IOException;

public abstract class FXPacket implements IMessage, IMessageHandler<IMessage, FXPacket> {
	public abstract void write(ByteBuf var1) throws IOException;

	public abstract void read(ByteBuf var1) throws IOException;

	@SideOnly(Side.CLIENT)
	public void processClient(IMessage message, NetHandlerPlayClient net) {
		throw new UnsupportedOperationException("Client side not implement " + this.getClass());
	}

	public void processServer(IMessage message, NetHandlerPlayServer net) {
		throw new UnsupportedOperationException("Server side not implement " + this.getClass());
	}

	@Override
	public FXPacket onMessage(IMessage message, MessageContext ctx) {
		if (FMLCommonHandler.instance().getSide().isServer()) {
			FMLCommonHandler.instance()
					.getMinecraftServerInstance()
					.addScheduledTask(() -> processServer(message, ctx.getServerHandler()));
		} else {
			Minecraft.getMinecraft().addScheduledTask(() -> processClient(message, ctx.getClientHandler()));
		}
		return null;
	}

	public abstract FXPacket getCasted(IMessage message);

	@SideOnly(Side.CLIENT)
	public void sendToServer() {
		Fuix.net.sendToServer(this);
	}

	public void sendTo(EntityPlayerMP player) {
		Fuix.net.sendTo(this, player);
	}

	public void sendTo(NetHandlerPlayServer net) {
		Fuix.net.sendTo(this, net.player);
	}

	public void sendToAllAround(NetworkRegistry.TargetPoint tp) {
		for (EntityPlayerMP player : FMLCommonHandler.instance()
				.getMinecraftServerInstance()
				.getPlayerList()
				.getPlayers()) {
			if (player.dimension == tp.dimension) {
				double d4 = tp.x - player.posX;
				double d5 = tp.y - player.posY;
				double d6 = tp.z - player.posZ;
				if (d4 * d4 + d5 * d5 + d6 * d6 < tp.range * tp.range) {
					this.sendTo(player);
				}
			}
		}
	}

	public void sendToAllInWorld(WorldServer world) {
		for (EntityPlayer player : world.playerEntities) {
			this.sendTo((EntityPlayerMP) player);
		}
	}

	public void sendToAllExcept(EntityPlayer player) {
		for (EntityPlayerMP p : FMLCommonHandler.instance().getMinecraftServerInstance().getPlayerList().getPlayers()) {
			if (!player.getName().equalsIgnoreCase(p.getName())) {
				this.sendTo(p);
			}
		}
	}

	public void sendToAll() {
		for (EntityPlayerMP p : FMLCommonHandler.instance().getMinecraftServerInstance().getPlayerList().getPlayers()) {
			this.sendTo(p);
		}
	}

	@Override
	public void fromBytes(ByteBuf buf) {
		try {
			read(buf);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void toBytes(ByteBuf buf) {
		try {
			write(buf);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
