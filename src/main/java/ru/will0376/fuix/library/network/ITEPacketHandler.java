package ru.will0376.fuix.library.network;

import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public interface ITEPacketHandler {
	@SideOnly(Side.CLIENT)
	void handlePacketClient(TEPacket var1);

	void handlePacketServer(TEPacket var1, EntityPlayerMP var2);
}
