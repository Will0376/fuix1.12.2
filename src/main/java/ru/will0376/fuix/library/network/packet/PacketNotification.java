package ru.will0376.fuix.library.network.packet;

import io.netty.buffer.ByteBuf;
import net.minecraft.client.network.NetHandlerPlayClient;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import ru.will0376.fuix.Fuix;
import ru.will0376.fuix.library.client.notification.Notification;
import ru.will0376.fuix.library.network.FXPacket;

import java.io.IOException;

public class PacketNotification extends FXPacket {
   private String text;
   private int time;
   private String style;
   private boolean replace = false;

   public PacketNotification() {
   }

   public PacketNotification(String text, int time, String style) {
      this.text = text;
      this.time = time;
      this.style = style;
   }

   public void write(ByteBuf buf) throws IOException {
      ByteBufUtils.writeUTF8String(buf, text);
      buf.writeInt(this.time);
      ByteBufUtils.writeUTF8String(buf, style);
      buf.writeBoolean(this.replace);
   }

   public void read(ByteBuf buf) throws IOException {
      this.text = ByteBufUtils.readUTF8String(buf);
      this.time = buf.readInt();
      this.style = ByteBufUtils.readUTF8String(buf);
      this.replace = buf.readBoolean();
   }

   @SideOnly(Side.CLIENT)
   public void processClient(IMessage message, NetHandlerPlayClient net) {
      Fuix.getClient()
              .getRenderEvent()
              .setNotification(new Notification(getCasted(message).text, getCasted(message).time, getCasted(message).style));
   }

   public boolean isReplace() {
      return this.replace;
   }

   public PacketNotification setReplace(boolean replace) {
      this.replace = replace;
      return this;
   }

   @Override
   public PacketNotification getCasted(IMessage message) {
      return (PacketNotification) message;
   }
}
