package ru.will0376.fuix.library.network.simplenet;

import io.netty.buffer.ByteBuf;
import net.minecraft.client.Minecraft;
import net.minecraft.client.network.NetHandlerPlayClient;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import ru.will0376.fuix.donate.client.gui.GuiDonate;
import ru.will0376.fuix.donate.client.gui.GuiPrefix;
import ru.will0376.fuix.library.client.ui.GuiScreenAdapter;
import ru.will0376.fuix.library.network.FXPacket;

import java.io.IOException;

public class PacketDonateToClient extends FXPacket {
	private Type type = Type.NONE;

	public PacketDonateToClient() {
	}

	public PacketDonateToClient(Type type) {
		this.type = type;
	}

	public PacketDonateToClient(Type type, Object object) {
		this.type = type;
	}

	@Override
	public void write(ByteBuf buf) throws IOException {
		ByteBufUtils.writeUTF8String(buf, type.name());
	}

	@Override
	public void read(ByteBuf buf) throws IOException {
		type = Type.valueOf(ByteBufUtils.readUTF8String(buf));
	}

	@SideOnly(Side.CLIENT)
	@Override
	public void processClient(IMessage message, NetHandlerPlayClient net) {
		if (getCasted(message).type == Type.OPEN_GUI_DONATE) {
			Minecraft.getMinecraft().displayGuiScreen(new GuiScreenAdapter(new GuiDonate()));
		} else if (getCasted(message).type == Type.OPEN_GUI_PREFIX) {
			Minecraft.getMinecraft().displayGuiScreen(new GuiScreenAdapter(new GuiPrefix()));
		}
	}

	@Override
	public PacketDonateToClient getCasted(IMessage message) {
		return (PacketDonateToClient) message;
	}

	public enum Type {
		NONE,
		OPEN_GUI_DONATE,
		OPEN_GUI_PREFIX,
		GET,
		RESULT
	}
}
