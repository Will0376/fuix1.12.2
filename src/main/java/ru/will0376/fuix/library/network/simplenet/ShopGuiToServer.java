package ru.will0376.fuix.library.network.simplenet;

import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import ru.justagod.cutter.invoke.Invoke;
import ru.will0376.fuix.Fuix;
import ru.will0376.fuix.library.init.ServerSide;
import ru.will0376.fuix.library.network.FXPacket;
import ru.will0376.fuix.library.network.packet.PacketNotification;
import ru.will0376.fuix.library.network.packet.data.ResultGui;
import ru.will0376.fuix.library.util.PermissionsUtils;
import ru.will0376.fuix.shop.network.data.Product;
import ru.will0376.fuix.shop.network.data.ShopEnchant;
import ru.will0376.fuix.shop.server.ServerDataHelper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ShopGuiToServer extends FXPacket {
	public ResultGui result = null;
	private Type type = Type.NONE;
	private boolean editMode = false;
	private List<Product> shopProduct = new ArrayList<>();
	private List<ShopEnchant> shopEnchant = new ArrayList<>();
	private Product product = null;

	public ShopGuiToServer() {
	}

	public ShopGuiToServer(Type type, ResultGui result) {
		this.type = type;
		this.result = result;
	}

	public ShopGuiToServer(Type type, List<Product> shopProduct, List<ShopEnchant> shopEnchant) {
		this.type = type;
		this.shopProduct = shopProduct;
		this.shopEnchant = shopEnchant;
	}

	public ShopGuiToServer(Type type, Product product, List<ShopEnchant> shopEnchant) {
		this.type = type;
		this.product = product;
		this.shopEnchant = shopEnchant;
	}

	public ShopGuiToServer(Type type, Product product) {
		this.type = type;
		this.product = product;
	}

	@Override
	public void write(ByteBuf buf) throws IOException {
		ByteBufUtils.writeUTF8String(buf, type.name());
		if (type == Type.GUISHOP) {
			buf.writeBoolean(editMode);

			buf.writeInt(shopProduct.size());
			for (Product item : shopProduct) {
				item.write(buf);
			}

			buf.writeInt(shopEnchant.size());
			for (ShopEnchant item : shopEnchant) {
				item.write(buf);
			}
		} else if (type == Type.EDIT || type == Type.REMOVE) {
			product.write(buf);
		} else if (type == Type.BUY) {
			this.product.write(buf);
			buf.writeInt(shopEnchant.size());
			for (ShopEnchant item : shopEnchant) {
				item.write(buf);
			}
		}
	}

	@Override
	public void read(ByteBuf buf) throws IOException {
		type = Type.valueOf(ByteBufUtils.readUTF8String(buf));
		if (type == Type.GUISHOP) {
			editMode = buf.readBoolean();

			int size = buf.readInt();
			shopProduct = size == 0 ? Collections.emptyList() : new ArrayList<>(size);
			for (int i = 0; i < size; i++) {
				shopProduct.add(Product.read(buf));
			}

			size = buf.readInt();
			shopEnchant = size == 0 ? Collections.emptyList() : new ArrayList<>(size);
			for (int i = 0; i < size; i++) {
				shopEnchant.add(ShopEnchant.read(buf));
			}
		} else if (type == Type.EDIT || type == Type.REMOVE) {
			product = Product.read(buf);
		} else if (type == Type.BUY) {
			this.product = Product.read(buf);
			int size = buf.readInt();
			shopEnchant = size == 0 ? Collections.emptyList() : new ArrayList<>(size);
			for (int i = 0; i < size; i++) {
				shopEnchant.add(ShopEnchant.read(buf));
			}
		}
	}

	@Override
	public FXPacket onMessage(IMessage message, MessageContext ctx) {
		Invoke.server(() -> {
			EntityPlayerMP player = ctx.getServerHandler().player;
			ServerDataHelper dataHelper = Fuix.getServer().dataHelper;

			if (getCasted(message).type == Type.EDIT) {
				if (!PermissionsUtils.hasPex(player, "fuix.shop.edit")) {
					new PacketNotification("§cНет прав на использование (EDIT)!", 3, "INFO").sendTo(player);
					new ShopGuiToClient(ShopGuiToClient.Type.RESULTCODE, new ResultGui(1, "")).sendTo(player);
					return;
				}
				dataHelper.onEditProduct(getCasted(message).product);
			} else if (getCasted(message).type == Type.REMOVE) {
				if (!PermissionsUtils.hasPex(player, "fuix.shop.remove")) {
					new PacketNotification("§cНет прав на использование (REMOVE)!", 3, "INFO").sendTo(player);
					new ShopGuiToClient(ShopGuiToClient.Type.RESULTCODE, new ResultGui(1, "")).sendTo(player);
					return;
				}
				dataHelper.onRemoveProduct(getCasted(message).product);
			}

			if (getCasted(message).type == Type.GUISHOP || getCasted(message).type == Type.EDIT || getCasted(message).type == Type.REMOVE) {
				new ShopGuiToClient(ShopGuiToClient.Type.GUISHOP, dataHelper.getShopProduct(), dataHelper.getShopEnchant())
						.setEditMode(PermissionsUtils.hasPex(player, "fuix.shop.edit"))
						.sendTo(player);
				ServerSide.onUpdateMoney(player);
			}

			if (getCasted(message).type == Type.BUY) {
				if (!PermissionsUtils.hasPex(player, "fuix.shop.buy")) {
					new PacketNotification("Нет прав на покупку предмета!", 3, "INFO").sendTo(player);
					new ShopGuiToClient(ShopGuiToClient.Type.RESULTCODE, new ResultGui(1, "")).sendTo(player);
					return;
				}
				Fuix.getServer().dataHelper.onBuyProduct(player, getCasted(message).product, getCasted(message).shopEnchant);
			}

			new ShopGuiToClient(ShopGuiToClient.Type.RESULTCODE, new ResultGui(0, "")).sendTo(player);
		});
		return null;
	}

	@Override
	public ShopGuiToServer getCasted(IMessage message) {
		return (ShopGuiToServer) message;
	}

	public ShopGuiToServer setEditMode(boolean editMode) {
		this.editMode = editMode;
		return this;
	}

	public enum Type {
		NONE,
		BUY,
		EDIT,
		REMOVE,
		RESULTCODE,
		GUISHOP,
	}
}
