package ru.will0376.fuix.library.network.simplenet;

import io.netty.buffer.ByteBuf;
import net.minecraft.client.Minecraft;
import net.minecraft.client.network.NetHandlerPlayClient;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import ru.will0376.fuix.donate.client.gui.GuiInventory;
import ru.will0376.fuix.donate.client.gui.modal.GuiInventoryItem;
import ru.will0376.fuix.donate.network.data.InventoryItem;
import ru.will0376.fuix.library.client.ui.GuiScreenAdapter;
import ru.will0376.fuix.library.client.ui.Window;
import ru.will0376.fuix.library.network.FXPacket;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class PacketInventoryToClient extends FXPacket {
	public Type type = Type.NONE;
	public int inventoryItemId;
	public Result result;
	public List<InventoryItem> inventoryItems = new ArrayList<>();

	public PacketInventoryToClient() {
	}

	public PacketInventoryToClient(Type type) {
		this.type = type;
	}

	@Override
	public void write(ByteBuf buf) throws IOException {
		ByteBufUtils.writeUTF8String(buf, type.name());
		if (type == Type.UPDATE) {
			buf.writeInt(inventoryItems.size());
			for (InventoryItem inventoryItem : inventoryItems) {
				inventoryItem.write(buf);
			}
		} else if (type == Type.GET) {
			buf.writeInt(inventoryItemId);
		} else if (type == Type.RESULT) {
			buf.writeInt(result.getCode());
			ByteBufUtils.writeUTF8String(buf, result.getMessage());
		}
	}

	@Override
	public void read(ByteBuf buf) throws IOException {
		type = Type.valueOf(ByteBufUtils.readUTF8String(buf));
		if (type == Type.UPDATE) {
			int size = buf.readInt();
			inventoryItems = size == 0 ? Collections.emptyList() : new ArrayList<>(size);
			for (int i = 0; i < size; i++) {
				inventoryItems.add(InventoryItem.read(buf));
			}
		} else if (type == Type.GET) {
			setInventoryItemId(buf.readInt());
		} else if (type == Type.RESULT) {
			setResult(new Result(buf.readInt(), ByteBufUtils.readUTF8String(buf)));
		}
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void processClient(IMessage message, NetHandlerPlayClient net) {
		if (getCasted(message).type == Type.OPEN) {
			Minecraft.getMinecraft().displayGuiScreen(new GuiScreenAdapter(new GuiInventory()));
		} else if (getCasted(message).type == Type.UPDATE) {
			net.minecraft.client.gui.GuiScreen currentScreen = Minecraft.getMinecraft().currentScreen;
			if (currentScreen instanceof GuiScreenAdapter) {
				Window gui = ((GuiScreenAdapter) currentScreen).getWindow();
				if (gui instanceof GuiInventory) ((GuiInventory) gui).onListUpdate(getCasted(message).inventoryItems);
			}
		} else if (getCasted(message).type == Type.RESULT) {
			net.minecraft.client.gui.GuiScreen currentScreen = Minecraft.getMinecraft().currentScreen;
			if (currentScreen instanceof GuiScreenAdapter) {
				Window gui = ((GuiScreenAdapter) currentScreen).getWindow();
				if (gui.getModalWindow() != null && gui instanceof GuiInventoryItem) {
					((GuiInventoryItem) gui.getModalWindow()).onResult(getCasted(message).result);
				} else if (gui instanceof GuiInventory) {
					((GuiInventory) gui).onResult(getCasted(message).result);
				}
			}
		}
	}

	@Override
	public PacketInventoryToClient getCasted(IMessage message) {
		return (PacketInventoryToClient) message;
	}

	public PacketInventoryToClient setInventoryItems(List<InventoryItem> inventoryItems) {
		this.inventoryItems = inventoryItems;
		return this;
	}

	public PacketInventoryToClient setInventoryItemId(int inventoryItemId) {
		this.inventoryItemId = inventoryItemId;
		return this;
	}

	public void setResult(Result result) {
		this.result = result;
	}

	public enum Type {
		NONE,
		OPEN,
		UPDATE,
		GET,
		RESULT
	}

	public static class Result {
		private int code;
		private String message;

		public Result(int code, String message) {
			this.code = code;
			this.message = message;
		}

		public int getCode() {
			return code;
		}

		public Result setCode(int code) {
			this.code = code;
			return this;
		}

		public String getMessage() {
			return message;
		}

		public Result setMessage(String message) {
			this.message = message;
			return this;
		}
	}
}
