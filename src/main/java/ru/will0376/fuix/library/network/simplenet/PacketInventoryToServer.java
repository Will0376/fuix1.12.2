package ru.will0376.fuix.library.network.simplenet;

import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import ru.justagod.cutter.invoke.Invoke;
import ru.will0376.fuix.Fuix;
import ru.will0376.fuix.donate.network.data.InventoryItem;
import ru.will0376.fuix.library.network.FXPacket;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class PacketInventoryToServer extends FXPacket {
	private Type type = Type.NONE;
	private int inventoryItemId;
	private Result result;
	private List<InventoryItem> inventoryItems = new ArrayList<>();

	public PacketInventoryToServer() {
	}

	public PacketInventoryToServer(Type type) {
		this.type = type;
	}

	@Override
	public void write(ByteBuf buf) throws IOException {
		ByteBufUtils.writeUTF8String(buf, type.name());
		if (type == Type.UPDATE) {
			buf.writeInt(inventoryItems.size());
			for (InventoryItem inventoryItem : inventoryItems) {
				inventoryItem.write(buf);
			}
		} else if (type == Type.GET) {
			buf.writeInt(inventoryItemId);
		} else if (type == Type.RESULT) {
			buf.writeInt(result.getCode());
			ByteBufUtils.writeUTF8String(buf, result.getMessage());
		}
	}

	@Override
	public void read(ByteBuf buf) throws IOException {
		type = Type.valueOf(ByteBufUtils.readUTF8String(buf));
		if (type == Type.UPDATE) {
			int size = buf.readInt();
			inventoryItems = size == 0 ? Collections.emptyList() : new ArrayList<InventoryItem>(size);
			for (int i = 0; i < size; i++) {
				inventoryItems.add(InventoryItem.read(buf));
			}
		} else if (type == Type.GET) {
			setInventoryItemId(buf.readInt());
		} else if (type == Type.RESULT) {
			setResult(new Result(buf.readInt(), ByteBufUtils.readUTF8String(buf)));
		}
	}

	@Override
	public FXPacket onMessage(IMessage message, MessageContext ctx) {
		Invoke.server(() -> {
			if (getCasted(message).type == Type.GET) {
				EntityPlayerMP player = ctx.getServerHandler().player;
				Fuix.getServer().dbHandler.pickupPurchase(player, getCasted(message).inventoryItemId);

				List<InventoryItem> inventoryItems = Fuix.getServer().dbHandler.getInventory(player);
				new PacketInventoryToClient(PacketInventoryToClient.Type.UPDATE).setInventoryItems(inventoryItems)
						.sendTo(player);
			}
		});
		return null;
	}

	@Override
	public PacketInventoryToServer getCasted(IMessage message) {
		return (PacketInventoryToServer) message;
	}

	public PacketInventoryToServer setInventoryItems(List<InventoryItem> inventoryItems) {
		this.inventoryItems = inventoryItems;
		return this;
	}

	public PacketInventoryToServer setInventoryItemId(int inventoryItemId) {
		this.inventoryItemId = inventoryItemId;
		return this;
	}

	public void setResult(Result result) {
		this.result = result;
	}

	public enum Type {
		NONE,
		OPEN,
		UPDATE,
		GET,
		RESULT
	}

	public static class Result {
		private int code;
		private String message;

		public Result(int code, String message) {
			this.code = code;
			this.message = message;
		}

		public int getCode() {
			return code;
		}

		public Result setCode(int code) {
			this.code = code;
			return this;
		}

		public String getMessage() {
			return message;
		}

		public Result setMessage(String message) {
			this.message = message;
			return this;
		}
	}
}
