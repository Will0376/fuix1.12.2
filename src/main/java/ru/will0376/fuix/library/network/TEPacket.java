package ru.will0376.fuix.library.network;

import io.netty.buffer.ByteBuf;
import net.minecraft.client.Minecraft;
import net.minecraft.client.network.NetHandlerPlayClient;
import net.minecraft.network.NetHandlerPlayServer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import java.io.IOException;

public abstract class TEPacket extends FXPacket {
   BlockPos pos;

   protected TEPacket() {
   }

   public TEPacket form(TileEntity te) {
      pos = te.getPos();
      return this;
   }

   public void write(ByteBuf buf) throws IOException {
      buf.writeInt(pos.getX());
      buf.writeInt(pos.getY());
      buf.writeInt(pos.getZ());
      writePacketData(buf);
   }

   public void read(ByteBuf buf) throws IOException {
      int x = buf.readInt();
      int y = buf.readInt();
      int z = buf.readInt();
      pos = new BlockPos(x, y, z);
      readPacketData(buf);
   }

   public abstract void writePacketData(ByteBuf var1) throws IOException;

   public abstract void readPacketData(ByteBuf var1) throws IOException;

   @SideOnly(Side.CLIENT)
   public void processClient(IMessage message, NetHandlerPlayClient net) {
      TileEntity te = Minecraft.getMinecraft().world.getTileEntity(pos);
      if (te instanceof ITEPacketHandler) {
         ((ITEPacketHandler) te).handlePacketClient(this);
      }

   }

   public void processServer(IMessage message, NetHandlerPlayServer net) {
      TileEntity te = net.player.getEntityWorld().getTileEntity(pos);
      if (te instanceof ITEPacketHandler) {
         ((ITEPacketHandler) te).handlePacketServer(this, net.player);
      }

   }
}
