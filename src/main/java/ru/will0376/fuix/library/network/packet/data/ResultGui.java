package ru.will0376.fuix.library.network.packet.data;

import io.netty.buffer.ByteBuf;
import net.minecraftforge.fml.common.network.ByteBufUtils;

import java.io.IOException;

public class ResultGui {
   private int code = 0;
   private String message = "";

   public ResultGui(int code, String message) {
      this.code = code;
      this.message = message;
   }

   public static ResultGui read(ByteBuf buf) throws IOException {
      int code = buf.readInt();
      String message = ByteBufUtils.readUTF8String(buf);
      return new ResultGui(code, message);
   }

   public String getMessage() {
      return this.message;
   }

   public int getCode() {
      return this.code;
   }

   public ByteBuf write(ByteBuf buf) throws IOException {
      buf.writeInt(this.getCode());
      ByteBufUtils.writeUTF8String(buf, getMessage());
      return buf;
   }
}
