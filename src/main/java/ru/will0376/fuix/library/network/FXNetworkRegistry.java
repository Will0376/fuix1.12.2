package ru.will0376.fuix.library.network;

import net.minecraftforge.fml.relauncher.Side;
import ru.will0376.fuix.Fuix;

public class FXNetworkRegistry {
	static int i = 0;

	public static void registerPacket(Class<? extends FXPacket> pClass, Side side) {

		try {
			Fuix.net.registerMessage(pClass.newInstance(), pClass, i++, side);
		} catch (InstantiationException | IllegalAccessException e) {
			e.printStackTrace();
		}
	}
/*
   private static final Map<Class<? extends Packet<? >>,EnumConnectionState> packetToStateMap;
   private static final List<Class<? extends FXPacket> > registered = new ArrayList<>();
   private static final Map<String,Class<? extends FXPacket>> nameToPacket = new HashMap<>();
   private static final Class[] idToPacket = new Class[256];
   private static final TObjectIntMap<Class<? extends FXPacket>> packetToId = new TObjectIntHashMap<>();
   private static boolean built;
   private static SPacketNetworkList prewritedPacket;
   @SubscribeEvent(
      priority = EventPriority.HIGHEST
   )
   public void onPlayerLoggedIn(FMLNetworkEvent.ServerConnectionFromClientEvent e) {
      if (!built) {
         build();
      }

      if (FMLCommonHandler.instance().getSide().isServer()) {
         e.getManager().sendPacket(prewritedPacket);
      }

   }

   @SideOnly(Side.CLIENT)
   @SubscribeEvent(
      priority = EventPriority.LOWEST
   )
   public void onClientDisconnected(FMLNetworkEvent.ClientDisconnectionFromServerEvent e) {
      build();
   }

   public static void build() {
      built = true;
      int i = 0;

      for(int s = registered.size(); i < s; ++i) {
         Class<? extends FXPacket> cls = registered.get(i);
         idToPacket[i] = cls;
         packetToId.put(cls, i);
      }

      if (FMLCommonHandler.instance().getSide().isServer()) {
         ByteArrayOutputStream out = new ByteArrayOutputStream(1024);
         DataOutputStream data = new DataOutputStream(out);

         try {
            data.write(registered.size());
            i = 0;

            for(int s = registered.size(); i < s; ++i) {
               Class<? extends FXPacket> cls = registered.get(i);
               data.writeUTF(cls.getName());
            }
         } catch (IOException ignored) {
         }

         prewritedPacket = new SPacketNetworkList(out.toByteArray());
      }

   }

   public static int getPacketID(FXPacket packet) {
      return packetToId.get(packet.getClass());
   }

   public static FXPacket makePacketServer(int id) {
      try {
         return (FXPacket)idToPacket[id].newInstance();
      } catch (Exception var2) {
         throw new RuntimeException("Failed to make FXPaket for ID: " + id, var2);
      }
   }

   @SideOnly(Side.CLIENT)
   public static synchronized FXPacket makePacketClient(int id) {
      return makePacketServer(id);
   }

   @SideOnly(Side.CLIENT)
   static synchronized void acceptServerList(String[] classes) {
      if (FMLCommonHandler.instance().getMinecraftServerInstance() == null) {
         int i = 0;

         for(int s = classes.length; i < s; ++i) {
            Class<? extends FXPacket> cls = nameToPacket.get(classes[i]);
            idToPacket[i] = cls;
            packetToId.put(cls, i);
         }

      }
   }

   public static void registerVanillaPacket(Side side, EnumConnectionState state, int id, Class<? extends Packet<?>> packet) {
      try {
         state.registerPacket(side == Side.SERVER ? EnumPacketDirection.SERVERBOUND : EnumPacketDirection.CLIENTBOUND,packet);
      } catch (Exception var5) {
         throw new RuntimeException(var5);
      }

      packetToStateMap.put(packet, state);
   }

   public static void registerPacket(Class<? extends FXPacket> cls) {
      if (built) {
         throw new IllegalStateException("FXNetworkRegistry has been locked");
      } else {
         registered.add(cls);
         nameToPacket.put(cls.getName(), cls);
      }
   }

   static {
      packetToStateMap = EnumConnectionState.STATES_BY_CLASS;

      FMLCommonHandler.instance().bus().register(new FXNetworkRegistry());
      registerVanillaPacket(Side.SERVER, EnumConnectionState.PLAY, 180, SPacketNetworkList.class);
      registerVanillaPacket(Side.SERVER, EnumConnectionState.PLAY, 181, ServerFXPacketProxy.class);
      registerVanillaPacket(Side.CLIENT, EnumConnectionState.PLAY, 181, ClientFXPacketProxy.class);
   }
*/
}
