package ru.will0376.fuix.library.network;


import ru.will0376.fuix.library.network.packet.data.ResultGui;

public interface IGuiResult {
	void onResultGui(ResultGui var1);
}
