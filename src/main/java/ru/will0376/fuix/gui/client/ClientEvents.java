package ru.will0376.fuix.gui.client;

import net.minecraft.client.Minecraft;
import net.minecraftforge.client.event.GuiOpenEvent;
import net.minecraftforge.client.event.RenderGameOverlayEvent;
import net.minecraftforge.fml.common.eventhandler.EventPriority;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.InputEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class ClientEvents {

    @SideOnly(Side.CLIENT)
    @SubscribeEvent
    public void onKeyInput(InputEvent.KeyInputEvent e) {
    }

    @SideOnly(Side.CLIENT)
    @SubscribeEvent
    public void onGuiOpen(GuiOpenEvent event) {
        Minecraft mc = Minecraft.getMinecraft();

        /*if (event.getGui() instanceof GuiMainMenu) event.setGui(new GuiScreenAdapter(new MainMenu((GuiMainMenu) event.getGui())));
        if (event.getGui() instanceof GuiIngameMenu) event.setGui(new GuiScreenAdapter(new IngameMenu((GuiIngameMenu) event.getGui())));*/
    }

    @SideOnly(Side.CLIENT)
    @SubscribeEvent(priority = EventPriority.HIGHEST)
    public void onRenderGameOverlayEvent(RenderGameOverlayEvent.Pre event) {
        Minecraft mc = Minecraft.getMinecraft();


/*
        if (event.getType() == RenderGameOverlayEvent.ElementType.DEBUG) {
            event.setCanceled(true);
            mc.profiler.startSection("debug");

            GL11.glPushMatrix();
            int uX = MathHelper.floor(mc.player.posX);
            int x = MathHelper.floor(mc.player.posY);
            int uZ = MathHelper.floor(mc.player.posZ);

            long memMax = Runtime.getRuntime().maxMemory();
            long memTotal = Runtime.getRuntime().totalMemory();
            long memFree = Runtime.getRuntime().freeMemory();
            long memUsed = memTotal - memFree;

            long memUsed1 = memUsed / 1024L / 1024L;
            long memMax1 = memMax / 1024L / 1024L;

            String line1 = "§3X:" + uX + " | Y:" + x + " | Z:" + uZ;
            String line2 = "§6Направление: ";
           // String line3 = "§6" + Direction.directions[MathHelper.floor((double)(mc.player.rotationYaw * 4.0F / 360.0F) + 0.5D) & 3] + ", " + MathHelper.floor((double)(MathHelper.wrapAngleTo180_float(mc.player.rotationYaw) + 180.0F)) + ", " + (mc.player.onGround ? "На земле" : "В воздухе");
            String line4 = "§7" + mc.debug;
            String line5 = "§7" + memUsed1 + "MB из " + memMax1 + "MB";

            double debugWidth = 0;
            if(FontHelper.getWidth(line1, 1) > debugWidth) debugWidth = FontHelper.getWidth(line1, 1);
            if(FontHelper.getWidth(line2, 1) > debugWidth) debugWidth = FontHelper.getWidth(line2, 1);
            if(FontHelper.getWidth(line3, 1) > debugWidth) debugWidth = FontHelper.getWidth(line3, 1);
            if(FontHelper.getWidth(line4, 1) > debugWidth) debugWidth = FontHelper.getWidth(line4, 1);
            if(FontHelper.getWidth(line5, 1) > debugWidth) debugWidth = FontHelper.getWidth(line5, 1);

            debugWidth += 8;

            Rectangle rec = new Rectangle(Monitor.getWidth() - (int) debugWidth - 4, 5, (int) debugWidth, 57);

            DrawHelper.drawRectRounded(rec.x, rec.y, rec.width, rec.height,0.5, new Color(255, 255, 255, 60).hashCode());
            DrawHelper.drawRectRounded(rec.x + 0.3, rec.y + 0.3, rec.width - 0.6, rec.height - 0.6, 0.5, new Color(0, 0, 0, 120).hashCode());

            FontHelper.draw(line1, rec.x + 4, rec.y + 4, 1);
            FontHelper.draw(line2, rec.x + 4, rec.y + 4 + (10 * 1), 1);
            FontHelper.draw(line3, rec.x + 4, rec.y + 4 + (10 * 2), 1);
            FontHelper.draw(line4, rec.x + 4, rec.y + 4 + (10 * 3), 1);
            FontHelper.draw(line5, rec.x + 4, rec.y + 4 + (10 * 4), 1);


            Rectangle rec1 = new Rectangle(Monitor.getWidth() - (int) debugWidth - 4, 64, (int) debugWidth, 8);

            DrawHelper.drawRectRounded(rec1.x, rec1.y, rec1.width, rec1.height,0.5, new Color(255, 255, 255, 60).hashCode());
            DrawHelper.drawRectRounded(rec1.x + 0.3, rec1.y + 0.3, rec1.width - 0.6, rec1.height - 0.6, 0.5, new Color(0, 0, 0, 120).hashCode());

            long progress = 100 * memUsed1 / memMax1;
            long progress1 = progress * rec1.width / 100;

            Color color = new Color(123, 255, 0, 160);
            if(progress > 70) color = new Color(255, 211, 0, 160);
            if(progress > 90) color = new Color(255, 74, 13, 160);

            DrawHelper.drawRectRounded(rec1.x + 0.3, rec1.y + 0.3, progress1 - 0.6, rec1.height - 0.6, 0.5, color.hashCode());

            GL11.glPopMatrix();


            mc.mcProfiler.endSection();
        }
*/
    }

}
