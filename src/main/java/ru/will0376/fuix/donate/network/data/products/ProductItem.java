package ru.will0376.fuix.donate.network.data.products;

import io.netty.buffer.ByteBuf;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.common.network.ByteBufUtils;

import java.io.IOException;

public class ProductItem {

	private final ItemStack stack;

	public ProductItem(ItemStack stack) {
		this.stack = stack;
	}

	public static ProductItem read(ByteBuf buf) throws IOException {
		return new ProductItem(ByteBufUtils.readItemStack(buf));
	}

	public ItemStack getStack() {
		return stack;
	}

	/**
	 * ============================================================================================================
	 **/

	public ByteBuf write(ByteBuf buf) throws IOException {
		ByteBufUtils.writeItemStack(buf, getStack());
		return buf;
	}

}
