package ru.will0376.fuix.donate.network.data;

import io.netty.buffer.ByteBuf;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import ru.will0376.fuix.donate.network.data.products.ProductGroup;
import ru.will0376.fuix.donate.network.data.products.ProductItem;

import java.io.IOException;

public class InventoryItem {

    /*
    1 - обычные предметы
    2 - комплекты предметов
    3 - группа
    4 - кейсы
    5 - внешность
    */
    private final int id;
    private final int type_id;
    private final Object value;
    private final int amount;
    private String name = "";
    private String description = "";

    public InventoryItem(int id, int type_id, Object value, int amount) {
        this.id = id;
        this.type_id = type_id;
        this.value = value;
        this.amount = amount;
    }

    public static InventoryItem read(ByteBuf buf) throws IOException {
        int id = buf.readInt();
        int type_id = buf.readInt();

        Object value = null;

        switch (type_id) {
            case 1:
                value = ProductItem.read(buf);
                break;
            case 3:
                value = ProductGroup.read(buf);
                break;
        }

        InventoryItem inventoryItem = new InventoryItem(id, type_id, value, buf.readInt());

//        String name = buf.readStringFromBuffer(64);
        String name = ByteBufUtils.readUTF8String(buf);
        if (name.isEmpty() && type_id == 1) {
            name = ((ProductItem) value).getStack().getDisplayName();
        }
        inventoryItem.setName(name);
        inventoryItem.setDescription(ByteBufUtils.readUTF8String(buf));
//        inventoryItem.setDescription(buf.readStringFromBuffer(255));
        return inventoryItem;
    }

    public int getId() {
        return id;
    }

    public int getTypeId() {
        return type_id;
    }

    public String getName() {
        return name;
    }

    public InventoryItem setName(String name) {
        this.name = name;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public InventoryItem setDescription(String description) {
        this.description = description;
        return this;
    }

    public Object getValue() {
        return value;
    }

    public int getAmount() {
        return amount;
    }

    /**
     * ==============================================================================================================
     **/

    public ByteBuf write(ByteBuf buf) throws IOException {

        buf.writeInt(getId());
        buf.writeInt(getTypeId());

        switch (getTypeId()) {
            case 1:
                ((ProductItem) getValue()).write(buf);
                break;
            case 3:
                ((ProductGroup) getValue()).write(buf);
                break;
        }

        buf.writeInt(getAmount());
        ByteBufUtils.writeUTF8String(buf, getName());
        ByteBufUtils.writeUTF8String(buf, getDescription());
        return buf;
    }

    public InventoryItem clone() {
        return new InventoryItem(getId(), getTypeId(), getValue(), getAmount()).setName(getName())
                .setDescription(getDescription());
    }
}
