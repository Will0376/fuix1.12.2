package ru.will0376.fuix.donate.network;

import io.netty.buffer.ByteBuf;
import net.minecraft.client.Minecraft;
import net.minecraft.client.network.NetHandlerPlayClient;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import ru.will0376.fuix.library.client.ui.GuiScreenAdapter;
import ru.will0376.fuix.library.client.ui.Window;
import ru.will0376.fuix.library.network.FXPacket;

import java.io.IOException;

public class PacketUpdateMoney extends FXPacket {
    private int userMoney = 0;

    public PacketUpdateMoney() {
    }

    public PacketUpdateMoney(int userMoney) {
        this.userMoney = userMoney;
    }

    @Override
    public void write(ByteBuf buf) throws IOException {
        buf.writeInt(userMoney);
    }

    @Override
    public void read(ByteBuf buf) throws IOException {
        userMoney = buf.readInt();
    }

    @SideOnly(Side.CLIENT)
    @Override
    public void processClient(IMessage message, NetHandlerPlayClient net) {
        net.minecraft.client.gui.GuiScreen currentScreen = Minecraft.getMinecraft().currentScreen;
        if (currentScreen instanceof GuiScreenAdapter) {
            Window gui = ((GuiScreenAdapter) currentScreen).getWindow();
            if (gui instanceof IGuiUpdateMoney) {
                ((IGuiUpdateMoney) gui).onUpdateMoney(getCasted(message).userMoney);
            }
        }
    }

    @Override
    public PacketUpdateMoney getCasted(IMessage message) {
        return (PacketUpdateMoney) message;
    }
}
