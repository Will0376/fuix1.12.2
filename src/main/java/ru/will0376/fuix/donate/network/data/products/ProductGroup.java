package ru.will0376.fuix.donate.network.data.products;

import io.netty.buffer.ByteBuf;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.common.network.ByteBufUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ProductGroup {

	private final String title;
	private final String nameId;
	private final int validity;
	private final List<Kit> kits;

	public ProductGroup(String title, String nameId, int validity, List<Kit> kits) {
		this.title = title;
		this.nameId = nameId;
		this.validity = validity;
		this.kits = kits;
	}

	public static ProductGroup read(ByteBuf buf) throws IOException {
		int size1 = buf.readInt();
		List<Kit> kits = size1 == 0 ? Collections.emptyList() : new ArrayList<Kit>(size1);
		for (int i1 = 0; i1 < size1; i1++) {
			int size2 = buf.readInt();
			List<ItemStack> stacks = size2 == 0 ? Collections.emptyList() : new ArrayList<ItemStack>(size2);
			for (int i2 = 0; i2 < size2; i2++) {
				stacks.add(ByteBufUtils.readItemStack(buf));
			}
			kits.add(new Kit(ByteBufUtils.readUTF8String(buf), stacks));
		}
		return new ProductGroup(ByteBufUtils.readUTF8String(buf), ByteBufUtils.readUTF8String(buf), buf.readInt(), kits);
	}

	public String getTitle() {
		return title;
	}

	public String getNameId() {
		return nameId;
	}

	public List<Kit> getKits() {
		return kits;
	}

	public int getValidity() {
		return validity;
	}

	/**
	 * ============================================================================================================
	 **/

	public ByteBuf write(ByteBuf buf) throws IOException {
		buf.writeInt(getKits().size());
		for (Kit kit : getKits()) {
			buf.writeInt(kit.getStackList().size());
			for (ItemStack stack : kit.getStackList()) {
				ByteBufUtils.writeItemStack(buf, stack);
			}
			ByteBufUtils.writeUTF8String(buf, kit.getName());
		}
		ByteBufUtils.writeUTF8String(buf, getTitle());
		ByteBufUtils.writeUTF8String(buf, getNameId());
		buf.writeInt(getValidity());
		return buf;
	}

	public static class Kit {
		private final String name;
		private final List<ItemStack> stackList;

		public Kit(String name, List<ItemStack> stackList) {
			this.name = name;
			this.stackList = stackList;
		}

		public List<ItemStack> getStackList() {
			return stackList;
		}

		public String getName() {
			return name;
		}
	}

}
