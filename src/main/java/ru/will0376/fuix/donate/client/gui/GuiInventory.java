package ru.will0376.fuix.donate.client.gui;

import com.google.common.collect.ImmutableList;
import ru.will0376.fuix.donate.client.gui.element.ElementInventoryItem;
import ru.will0376.fuix.donate.network.data.InventoryItem;
import ru.will0376.fuix.library.client.ui.GuiComponent;
import ru.will0376.fuix.library.client.ui.Window;
import ru.will0376.fuix.library.client.ui.drawable.Drawable;
import ru.will0376.fuix.library.client.ui.util.ArrayListObserver;
import ru.will0376.fuix.library.client.ui.util.DrawHelper;
import ru.will0376.fuix.library.client.ui.util.FontHelper;
import ru.will0376.fuix.library.client.ui.util.Monitor;
import ru.will0376.fuix.library.client.ui.widget.ListView;
import ru.will0376.fuix.library.client.ui.widget.ListViewItem;
import ru.will0376.fuix.library.client.ui.widget.TextField;
import ru.will0376.fuix.library.network.simplenet.PacketInventoryToClient;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class GuiInventory extends Window {

    public ListView listView;
    public TextField textSearch;
    private List<InventoryItem> inventoryItems;
    private String info = null;

    public GuiInventory() {
        super(291, 200);
        inventoryItems = new ArrayList<>();
        info = "Загрузка...";
    }

    public void onResult(PacketInventoryToClient.Result result) {

    }

    public void onListUpdate(List<InventoryItem> inventoryItems) {
        if (getModalWindow() != null) getModalWindow().onClose();
        this.inventoryItems = inventoryItems;
        if (inventoryItems.size() <= 0) {
            info = "Инвентарь пуст!";
        } else info = null;

        listView.updateList(new ArrayList<InventoryItem>());
        listView.updateList(new ArrayList<InventoryItem>(inventoryItems));
        System.err.println(" | " + inventoryItems.size());
    }

    @Override
    protected void onInit(int x, int y) {
        listView = new ListView("productList", new Rectangle(5, 24, this.getWidth() - 7, this.getHeight() - 29), this, new ArrayListObserver() {
            @Override
            public ListViewItem getListViewItem() {
                return new ElementInventoryItem(45, 38, getListView());
            }
        });
        listView.getListViewContent().setElementsPadding(2);
        listView.getListViewSlider().setSize(2);
        listView.getListViewSlider().setColorSlider(new Color(255, 255, 255, 60), new Color(255, 165, 0));
        addComponent(listView);

        textSearch = new TextField("textSearch", new Rectangle(getWidth() - 98, 2, 92, 18), this);
        textSearch.setHintText("Поиск");
        textSearch.setPaddingHorizontal(6);
        textSearch.setDrawable(new Drawable() {
            @Override
            public void onDraw(GuiComponent component, State state) {
                Rectangle rec = component.getAbsoluteBounds();
                DrawHelper.drawRect(rec.x + 0.4, rec.y, rec.width - 0.8, rec.height - 0.3, new Color(0, 0, 0, 40).hashCode());
                DrawHelper.drawRect(rec.x, rec.y, 0.3, rec.height, new Color(255, 255, 255, 60).hashCode());
                DrawHelper.drawRect(rec.x + rec.width - 0.3, rec.y, 0.3, rec.height, new Color(255, 255, 255, 60).hashCode());
            }
        });
        addComponent(textSearch);
    }

    @Override
    protected void onDraw() {
        DrawHelper.drawGradientRect(0, 0, Monitor.getWidth(), Monitor.getHeight(), new Color(0, 0, 0, 120).hashCode(), Integer.MIN_VALUE);

        Point p = getZeroPoint();
        DrawHelper.drawRect(0, 0, Monitor.getWidth(), Monitor.getHeight() + 10, new Color(32, 32, 32, 80).hashCode());
        GuiUtils.drawBackgroundGui(getZeroPoint(), getWidth(), getHeight());

        DrawHelper.drawRect(getZeroPoint().x, getZeroPoint().y, getWidth(), 20, new Color(255, 255, 255, 10).hashCode());
        DrawHelper.drawRect(p.x, p.y + 20, getWidth(), 0.3, new Color(255, 255, 255, 60).hashCode());

        FontHelper.draw("Внешний инвентарь", p.x + 6, p.y + 3, new Color(255, 255, 255).hashCode(), 1);
        FontHelper.draw(inventoryItems.size() + " предметов", p.x + 6, p.y + 10, new Color(255, 255, 255).hashCode(), 1);

        if (info != null)
            FontHelper.draw(info, p.x + getWidth() / 2.0, p.y + getHeight() / 2.0, new Color(255, 255, 255).hashCode(), FontHelper.Align.CENTER, 1);
    }

    @Override
    protected void onUpdate() {
        if (textSearch != null) {
            if (textSearch.getIsChanged()) filterBySearch(textSearch.getText());
        }
    }

    private ArrayList<InventoryItem> filterBySearch(final String query) {
        listView.updateList(new ArrayList<>());

        List<InventoryItem> list = ImmutableList.copyOf(inventoryItems)
                .stream()
                .filter(it -> it.getName().toLowerCase().contains(query.toLowerCase().trim()))
                .collect(Collectors.toList());

        if (list.size() <= 0) {
            info = "Нечего не найдено!";
        } else info = null;

        listView.updateList(new ArrayList<>(list));

        return new ArrayList<>(list);
    }
}
