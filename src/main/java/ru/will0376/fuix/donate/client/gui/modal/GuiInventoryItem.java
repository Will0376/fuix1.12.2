package ru.will0376.fuix.donate.client.gui.modal;

import ru.will0376.fuix.donate.client.gui.GuiUtils;
import ru.will0376.fuix.donate.network.data.InventoryItem;
import ru.will0376.fuix.library.client.ui.ModalWindow;
import ru.will0376.fuix.library.client.ui.Window;
import ru.will0376.fuix.library.client.ui.drawable.ButtonDrawable;
import ru.will0376.fuix.library.client.ui.util.DrawHelper;
import ru.will0376.fuix.library.client.ui.util.FontHelper;
import ru.will0376.fuix.library.client.ui.widget.Button;
import ru.will0376.fuix.library.network.simplenet.PacketInventoryToClient;
import ru.will0376.fuix.library.network.simplenet.PacketInventoryToServer;

import java.awt.*;

public class GuiInventoryItem extends ModalWindow {

    private final InventoryItem inventoryItem;

    public GuiInventoryItem(InventoryItem inventoryItem, Window mainWindow) {
        super(mainWindow, 180, 60);
        this.inventoryItem = inventoryItem;
    }

    public void onResult(PacketInventoryToClient.Result result) {

    }

    @Override
    protected void onInit(int x, int y) {
        addComponent(new Button("btnGet", new Rectangle(getWidth() - 55, getHeight() - 17, 50, 12), this).setText("Забрать")
                .setCallback(() -> {
                    new PacketInventoryToServer(PacketInventoryToServer.Type.GET).setInventoryItemId(inventoryItem.getId())
                            .sendToServer();
                })
                .setShadow(true));

        addComponent(new Button("btnExit", new Rectangle(5, getHeight() - 17, 50, 12), this).setText("Закрыть")
                .setCallback(() -> {
                    onClose();
                })
                .setShadow(true)
                .setDrawable(ButtonDrawable.RED));
    }

    @Override
    protected void onDraw() {
        Point p = getZeroPoint();
        DrawHelper.drawRect(getMainWindow().getZeroPoint().getX(), getMainWindow().getZeroPoint()
                .getY(), getMainWindow().getWidth(), getMainWindow().getHeight(), new Color(32, 32, 32, 80).hashCode());
        GuiUtils.drawBackgroundGui(getZeroPoint(), getWidth(), getHeight());

        DrawHelper.drawRect(getZeroPoint().x, getZeroPoint().y, getWidth(), 16, new Color(255, 255, 255, 10).hashCode());
        DrawHelper.drawRect(p.x, p.y + 16, getWidth(), 0.3, new Color(255, 255, 255, 60).hashCode());

        FontHelper.draw(inventoryItem.getName(), p.x + (getWidth() / 2), p.y + 8.5, new Color(255, 255, 255).hashCode(), FontHelper.Align.CENTER, 1);

        String text = "Вы уверены что хотите забрать предмет?";
        FontHelper.draw(text, getZeroPoint().x + (getWidth() / 2), getZeroPoint().y + 23, new Color(152, 251, 152, 180).hashCode(), FontHelper.Align.CENTER_X, 1);

        DrawHelper.drawRect(p.x, p.y + getHeight() - 20, getWidth(), 0.3, new Color(255, 255, 255, 60).hashCode());

    }

}
