package ru.will0376.fuix.donate.client.gui.element;

import ru.will0376.fuix.donate.client.gui.modal.GuiInventoryItem;
import ru.will0376.fuix.donate.network.data.InventoryItem;
import ru.will0376.fuix.donate.network.data.products.ProductItem;
import ru.will0376.fuix.library.client.ui.MouseAction;
import ru.will0376.fuix.library.client.ui.util.DrawHelper;
import ru.will0376.fuix.library.client.ui.util.FontHelper;
import ru.will0376.fuix.library.client.ui.widget.ListView;
import ru.will0376.fuix.library.client.ui.widget.ListViewItem;

import java.awt.*;

public class ElementInventoryItem extends ListViewItem {

    private InventoryItem inventoryItem;

    public ElementInventoryItem(int width, int height, ListView listView) {
        super(width, height, listView);
    }

    public ElementInventoryItem(InventoryItem inventoryItem, int width, int height, ListView listView) {
        super(width, height, listView);
        this.inventoryItem = inventoryItem;
    }

    @Override
    public void onInit(int x, int y) {
    }

    @Override
    protected void onItemDraw(int x, int y, int topLeftX, int topLeftY) {
        int posX = getAbsoluteBounds().x;
        int posY = getAbsoluteBounds().y;
        int width = getBounds().width;
        int height = getBounds().height;
        if (inventoryItem == null)
            this.inventoryItem = ((InventoryItem) getListView().getObserver().getObservableList().get(getRenderId()));

        if (inventoryItem.getTypeId() == 1) {
            ProductItem productItem = (ProductItem) inventoryItem.getValue();

            DrawHelper.drawRectRounded(posX, posY, width, height, 0.4, new Color(255, 255, 255, 80).hashCode());
            DrawHelper.drawRectRounded(posX + 0.3, posY + 0.3, width - 0.6, height - 0.6, 0.4, new Color(0, 0, 0, 100).hashCode());

            DrawHelper.drawItem(productItem.getStack(), posX + (width / 2) - 12, posY, 24);

            DrawHelper.drawRect(posX + 0.3, posY + height - 11, width - 0.6, 0.3, new Color(255, 255, 255, 40).hashCode());

            if (isFocused()) {
                DrawHelper.drawRectRounded(posX + 0.3, posY + height - 11, width - 0.6, 11 - 0.3, 0.4, new Color(52, 255, 62, 50)
                        .hashCode());
            } else {
                DrawHelper.drawRectRounded(posX + 0.3, posY + height - 11, width - 0.6, 11 - 0.3, 0.4, new Color(255, 255, 255, 18)
                        .hashCode());
            }

            FontHelper.draw("Забрать", posX + 11, posY + height - 5.5, new Color(255, 255, 255).hashCode(), FontHelper.Align.CENTER_Y, 0.8F);




            /*if(isFocused()) {
                DrawHelper.drawRectRounded(posX, posY, width, height, 0.5, new Color(255, 255, 0, 200).hashCode());
            } else DrawHelper.drawRectRounded(posX, posY, width, height, 0.5, new Color(255, 255, 255, 180).hashCode());

            DrawHelper.drawRectRounded(posX + 0.3, posY + 0.3, width - 0.6, height - 0.6, 0.5, new Color(0, 0, 0, 140).hashCode());

            ItemStack stack = productItem.getStack();
            stack.stackSize = 1;
            DrawHelper.drawItem(stack, posX - 1, posY - 1, 20);

            FontHelper.draw(productItem.getStack().getDisplayName(), posX + 20, posY + 1.5, new Color(255, 255, 255).hashCode(), 1);
            FontHelper.draw(inventoryItem.getAmount() + "шт.", posX + 20, posY + 10, new Color(152, 251, 152, 180).hashCode(), 1);
*/
        }
    }

    @Override
    protected void onUpdate(int x, int y) {
    }

    @Override
    protected void handleMouse(MouseAction mouseAction) {
        if (mouseAction.getActionType().equals(MouseAction.ActionType.Click) && getAbsoluteBounds().contains(mouseAction
                .getPosX(), mouseAction.getPosY())) {
            if (inventoryItem == null) this.inventoryItem = ((InventoryItem) getListView().getObserver()
                    .getObservableList()
                    .get(getRenderId()));
            getWindow().onViewModal(new GuiInventoryItem(inventoryItem.clone(), getWindow()));
        }
    }

}
