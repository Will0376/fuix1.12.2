package ru.will0376.fuix.donate.client.gui;

import net.minecraft.client.Minecraft;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;
import ru.will0376.fuix.Fuix;
import ru.will0376.fuix.library.client.ui.GuiComponent;
import ru.will0376.fuix.library.client.ui.MouseAction;
import ru.will0376.fuix.library.client.ui.Window;
import ru.will0376.fuix.library.client.ui.drawable.ButtonDrawable;
import ru.will0376.fuix.library.client.ui.drawable.Drawable;
import ru.will0376.fuix.library.client.ui.render.ParticleEngine;
import ru.will0376.fuix.library.client.ui.util.DrawHelper;
import ru.will0376.fuix.library.client.ui.util.FontHelper;
import ru.will0376.fuix.library.client.ui.util.Monitor;
import ru.will0376.fuix.library.client.ui.widget.Button;
import ru.will0376.fuix.library.client.ui.widget.ListView;
import ru.will0376.fuix.library.client.ui.widget.ListViewItem;
import ru.will0376.fuix.library.util.Timer;

import java.awt.*;


public class GuiDonate extends Window {

    private final Timer timer1 = new Timer();
    private final ParticleEngine particleEngine = new ParticleEngine(false);

    public ListView btnList;
    public Button btnPlay;
    public Button btnExit;

    public GuiDonate() {
        super(Monitor.getWidth(), Monitor.getHeight());
    }

    public static void drawLogo(double posX, double posY, double w, double h) {
        GL11.glEnable(3042);
        GL11.glDisable(GL11.GL_DEPTH_TEST);
        GL11.glDepthMask(true);
        GL11.glBlendFunc(770, 771);
        GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
        GL11.glDisable(GL11.GL_ALPHA_TEST);
        DrawHelper.draw(new ResourceLocation(Fuix.MOD_ID, "textures/logo.png"), posX, posY, w, h);
        GL11.glDepthMask(true);
        GL11.glEnable(GL11.GL_DEPTH_TEST);
        GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
        GL11.glDisable(3042);
        GL11.glEnable(GL11.GL_ALPHA_TEST);
        GL11.glEnable(GL11.GL_DEPTH_TEST);
    }

    @Override
    protected void onInit(int x, int y) {
        Minecraft mc = Minecraft.getMinecraft();

        int width = 120;
        int height = 75;

        int posX = getWidth() / 2 - width / 2;
        int posY = (getHeight() / 2 - height / 2) + 15;

        btnList = new ListView("categoryList", new Rectangle(posX, posY, width, height), this);

        btnList.getListViewSlider().setSize(2);
        btnList.getListViewSlider().setColorSlider(new Color(0, 0, 0, 0), new Color(255, 165, 0));
        btnList.getListViewContent().setElementsPadding(0);

        btnList.setDrawable(new Drawable() {
            @Override
            public void onDraw(GuiComponent component, State state) {
                Rectangle rec = component.getAbsoluteBounds();
                DrawHelper.drawRectRounded(rec.x, rec.y, rec.width, rec.height, 0.5, new Color(255, 255, 255, 60).hashCode());
                DrawHelper.drawRectRounded(rec.x + 0.3, rec.y + 0.3, rec.width - 0.6, rec.height - 0.6, 0.5, new Color(0, 0, 0, 120)
                        .hashCode());

            }
        });

        addComponent(btnList);

        btnList.addItem(new ElementCategory("§eКупленные предметы", () -> mc.player.sendChatMessage("/inventory"), width, 15, btnList));
        btnList.addItem(new ElementCategory("Магазин предметов", () -> mc.player.sendChatMessage("/shop item"), width, 15, btnList));
        //btnList.addItem(new ElementCategory("Магазин привелегий", () -> mc.thePlayer.sendChatMessage("/shop group"), width, 15, btnList));
        //btnList.addItem(new ElementCategory("Магазин кейсов", () -> mc.thePlayer.sendChatMessage("/shop cases"), width, 15, btnList));
        //btnList.addItem(new ElementCategory("Настройки префикса", () -> mc.thePlayer.sendChatMessage("/donate prefix"), width, 15, btnList));


        addComponent(btnExit = (Button) new Button("btnExit", new Rectangle(posX, posY + height + 3, width, 15), this).setText("Вернутся к игре")
                .setCallback(() -> {
                    Minecraft.getMinecraft().displayGuiScreen(null);
                })
                .setDrawable(ButtonDrawable.RED));
    }

    @Override
    protected void onDraw() {
        setSize(Monitor.getWidth(), Monitor.getHeight());

        particleEngine.render();
        if (timer1.hasReach(30)) {
            particleEngine.spawnParticles(0, 0, getWidth(), getHeight(), 25F, 15F);
            timer1.reset();
        }

        DrawHelper.drawGradientRect(0, 0, getWidth(), getHeight(), new Color(0, 0, 0, 120).hashCode(), Integer.MIN_VALUE);
        drawLogo(getWidth() / 2.0 - 96 / 2.0, getHeight() / 2.0 - 86, 96, 22);

        //============================================================================================================//

        int posX = getWidth() / 2 - btnList.getBounds().width / 2;
        int posY = (getHeight() / 2 - btnList.getBounds().height / 2) + 15;

        btnList.getBounds().x = posX;
        btnList.getBounds().y = posY;

       /* btnPlay.getBounds().x = posX;
        btnPlay.getBounds().y = posY - (16 + 2);*/

        btnExit.getBounds().x = posX;
        btnExit.getBounds().y = posY + btnList.getBounds().height + 3;
    }

    @Override
    public void onUpdate() {
        particleEngine.updateParticles();
    }

    public static class ElementCategory extends ListViewItem {
        private String text = "";
        private Runnable callback = null;

        public ElementCategory(String text, Runnable callback, int width, int height, ListView listView) {
            super(width, height, listView);
            this.text = text;
            this.callback = callback;
        }

        @Override
        public void onInit(int x, int y) {
            this.setEnabled(true);
        }

        @Override
        protected void onItemDraw(int x, int y, int topLeftX, int topLeftY) {
            Color textColor = new Color(255, 255, 255);
            if (isFocused()) {
                DrawHelper.drawRect(getAbsoluteBounds().x, getAbsoluteBounds().y, getBounds().width, getBounds().height, new Color(255, 255, 255, 30)
                        .hashCode());
            }

            DrawHelper.drawRect(getAbsoluteBounds().x, getAbsoluteBounds().y + getBounds().height, getBounds().width, 0.3, new Color(255, 255, 255, 28)
                    .hashCode());
            FontHelper.draw(text, getAbsoluteBounds().x + getBounds().width / 2.0, getAbsoluteBounds().y + getBounds().height / 2.0, textColor
                    .hashCode(), FontHelper.Align.CENTER, 1);
        }

        @Override
        protected void onUpdate(int x, int y) {
        }

        @Override
        protected void handleMouse(MouseAction mouseAction) {
            if (mouseAction.getActionType()
                    .equals(MouseAction.ActionType.Click) && getAbsoluteBounds().contains(mouseAction.getPosX(), mouseAction
                    .getPosY())) {
                if (callback != null) callback.run();
            }
        }
    }
}
