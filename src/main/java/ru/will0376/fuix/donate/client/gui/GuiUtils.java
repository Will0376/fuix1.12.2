package ru.will0376.fuix.donate.client.gui;


import ru.will0376.fuix.library.client.ui.util.DrawHelper;

import java.awt.*;

public class GuiUtils {
	public static void drawBackgroundGui(Point point, int width, int height) {
		DrawHelper.drawRectRounded(point.x, point.y, width, height, 1, new Color(255, 255, 255, 160).hashCode());
		DrawHelper.drawRectRounded(point.x, point.y, width, height, 1, new Color(0, 0, 0, 200).hashCode());
		drawBorder(point, width, height, new Color(255, 255, 255, 200));
	}

	public static void drawBorder(Point point, int width, int height, Color color) {
		Color color1 = new Color(color.getRed(), color.getGreen(), color.getBlue(), 180);
		Color color2 = new Color(color.getRed(), color.getGreen(), color.getBlue());

		DrawHelper.drawRectRounded(point.x, point.y, width, 2, 1, color1.hashCode());
		DrawHelper.drawRectRounded(point.x, point.y, width, 1.5, 1, color2.hashCode());

		DrawHelper.drawRectRounded(point.x, point.y, 2, height, 1, color1.hashCode());
		DrawHelper.drawRectRounded(point.x, point.y, 1.5, height, 1, color2.hashCode());

		DrawHelper.drawRectRounded(point.x + width - 2, point.y, 2, height, 1, color1.hashCode());
		DrawHelper.drawRectRounded(point.x + width - 1.5, point.y, 1.5, height, 1, color2.hashCode());

		DrawHelper.drawRectRounded(point.x, point.y + height - 2, width, 2, 1, color1.hashCode());
		DrawHelper.drawRectRounded(point.x, point.y + height - 1.5, width, 1.5, 1, color2.hashCode());
	}
}
