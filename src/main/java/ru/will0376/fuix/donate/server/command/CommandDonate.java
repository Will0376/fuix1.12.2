package ru.will0376.fuix.donate.server.command;


import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import ru.justagod.cutter.GradleSide;
import ru.justagod.cutter.GradleSideOnly;
import ru.will0376.fuix.Fuix;
import ru.will0376.fuix.donate.network.data.InventoryItem;
import ru.will0376.fuix.library.network.packet.PacketNotification;
import ru.will0376.fuix.library.network.simplenet.PacketDonateToClient;
import ru.will0376.fuix.library.network.simplenet.PacketInventoryToClient;
import ru.will0376.fuix.library.server.command.CommandExecuteException;
import ru.will0376.fuix.library.server.command.CommandExecutor;
import ru.will0376.fuix.library.server.command.CommandHandler;

import java.util.List;

@SideOnly(Side.SERVER)
@GradleSideOnly(GradleSide.SERVER)
public class CommandDonate extends CommandExecutor {

    @SideOnly(Side.SERVER)
    @CommandHandler(name = "donate", group = "FuixDonate")
    public void donate(final ICommandSender sender, final String[] args) throws CommandExecuteException {
        if (sender instanceof EntityPlayerMP) {
            if (args.length <= 0) {
                new PacketDonateToClient(PacketDonateToClient.Type.OPEN_GUI_DONATE).sendTo((EntityPlayerMP) sender);
                return;
            }

            if (args[0].equalsIgnoreCase("prefix")) {
                new PacketNotification("Настройка префикса в разработки! Ожидайте...", 3, "INFO").sendTo((EntityPlayerMP) sender);
            }
        }
    }

    @SideOnly(Side.SERVER)
    @CommandHandler(name = "inventory", group = "FuixDonate")
    public void inventory(final ICommandSender sender, final String[] args) throws CommandExecuteException {
        if (sender instanceof EntityPlayerMP) {
            EntityPlayerMP player = (EntityPlayerMP) sender;

            //new PacketNotification("dsfsdfdsf", 10, "INFO").sendTo(player);

            new PacketInventoryToClient(PacketInventoryToClient.Type.OPEN).sendTo(player);

            List<InventoryItem> inventoryItems = Fuix.getServer().dbHandler.getInventory(player);
            new PacketInventoryToClient(PacketInventoryToClient.Type.UPDATE).setInventoryItems(inventoryItems)
                    .sendTo(player);
        }
    }
}
