package ru.will0376.fuix.donate.server;

import net.milkbowl.vault.economy.Economy;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import org.json.JSONException;
import org.json.JSONObject;
import ru.justagod.cutter.GradleSide;
import ru.justagod.cutter.GradleSideOnly;
import ru.will0376.fuix.donate.network.data.InventoryItem;
import ru.will0376.fuix.donate.network.data.products.ProductItem;
import ru.will0376.fuix.library.server.ServerConfig;
import ru.will0376.fuix.library.server.data.Database;
import ru.will0376.fuix.library.util.MainUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@SideOnly(Side.SERVER)
@GradleSideOnly(GradleSide.SERVER)
public class ServerDataHelper {

    private final Database database;

    public ServerDataHelper() {
        this.database = new Database(ServerConfig.address, ServerConfig.port, ServerConfig.username, ServerConfig.password, ServerConfig.basename);
    }

    @SideOnly(Side.SERVER)
    public List<InventoryItem> getInventory(EntityPlayerMP entityPlayerMP) {
        List<InventoryItem> inventoryItems = new ArrayList<>();
        try (Connection c = database.getConnection()) {
            PreparedStatement s = c.prepareStatement("SELECT * FROM `" + ServerConfig.cartTable + "` WHERE `uuid` = ? and `server_id` = ?");
            s.setString(1, (entityPlayerMP.getUniqueID().toString()));
            s.setInt(2, ServerConfig.server_id);
            s.setQueryTimeout(Database.TIMEOUT);
            try (ResultSet rs = s.executeQuery()) {
                while (rs.next()) {
                    try {
                        int id = rs.getInt("id");
                        int type_id = rs.getInt("type_id");
                        String valueString = rs.getString("value");
                        JSONObject valueJson = new JSONObject(valueString);
                        int amount = rs.getInt("amount");

                        Object value = null;
                        if (type_id == 1) {
                            ItemStack stack = MainUtils.createItem(valueJson.getString("item"), amount, !valueJson.isNull("data") ? valueJson
                                    .getInt("data") : 0, !valueJson.isNull("tag") ? valueJson.getString("tag") : "");
                            if (stack != null) {
                                value = new ProductItem(stack);
                            }
                        }
                        if (value != null) inventoryItems.add(new InventoryItem(id, type_id, value, amount));

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return inventoryItems;
    }

    @SideOnly(Side.SERVER)
    public String pickupPurchase(EntityPlayer playerEntity, int id) {
        String uuid = playerEntity.getUniqueID().toString();
        try (Connection c = database.getConnection()) {

            PreparedStatement s;
            if (id != -1) {
                s = c.prepareStatement("SELECT * FROM `" + ServerConfig.cartTable + "` WHERE `id` = ? and `uuid` = ? and `server_id` = ?;");
                s.setInt(1, id);
                s.setString(2, uuid);
                s.setInt(3, ServerConfig.server_id);
                s.setQueryTimeout(Database.TIMEOUT);
            } else {
                s = c.prepareStatement("SELECT * FROM `" + ServerConfig.cartTable + "` WHERE `uuid` = ? and `server_id` = ?;");
                s.setString(1, uuid);
                s.setInt(2, ServerConfig.server_id);
                s.setQueryTimeout(Database.TIMEOUT);
            }
            try (ResultSet rs = s.executeQuery()) {
                while (rs.next()) {
                    try {
                        id = rs.getInt("id");
                        int type_id = rs.getInt("type_id");
                        String valueString = rs.getString("value");
                        JSONObject valueJson = new JSONObject(valueString);
                        int amount = rs.getInt("amount");

                        if (type_id == 1) {
                            ItemStack stack = MainUtils.createItem(valueJson.getString("item"), amount, !valueJson.isNull("data") ? valueJson
                                    .getInt("data") : 0, !valueJson.isNull("dataTag") ? valueJson.getJSONObject("dataTag")
                                    .toString()
                                    .replace("\"", "") : "");
                            if (stack != null) {
                                playerEntity.inventory.addItemStackToInventory(stack);

                                if (stack.getCount() == 0) {
                                    s = c.prepareStatement("DELETE FROM `" + ServerConfig.cartTable + "` WHERE `id` = ?");
                                    s.setInt(1, id);
                                    s.setQueryTimeout(Database.TIMEOUT);
                                    s.executeUpdate();
                                } else {
                                    s = c.prepareStatement("UPDATE `" + ServerConfig.cartTable + "` SET `amount` = ? WHERE `id` = ?;");
                                    s.setInt(1, stack.getCount());
                                    s.setInt(1, id);
                                    s.setQueryTimeout(Database.TIMEOUT);
                                    s.executeUpdate();
                                }
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public int getMoney(EntityPlayerMP entityPlayer) {
        Economy economy = ForgeVault.getEconomy();
        if (economy != null) {
            return (int) economy.getBalance(ForgeVault.toBukkitPlayer(entityPlayer));
        }
        return 0;
    }

    public boolean onWithdrawMoney(EntityPlayerMP player, double coins) {
        Economy economy = ForgeVault.getEconomy();
        if (economy != null) {
            return economy.withdrawPlayer(ForgeVault.toBukkitPlayer(player), coins).transactionSuccess();
        }
        return false;
    }

}
