package ru.will0376.fuix.donate.server;

import net.milkbowl.vault.economy.Economy;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.fml.common.FMLLog;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.RegisteredServiceProvider;

public class ForgeVault {
	@SideOnly(Side.SERVER)
	private static Economy economy;

	@SideOnly(Side.SERVER)
	public static Economy getEconomy() {
		return economy;
	}

	@SideOnly(Side.SERVER)
	public static void setupEconomy() throws ClassNotFoundException {
		if (Bukkit.getServer().getPluginManager().getPlugin("Vault") == null) {
			FMLLog.info("Cannot find Vault!");
			return;
		}
		RegisteredServiceProvider<Economy> rsp = Bukkit.getServer()
				.getServicesManager()
				.getRegistration(net.milkbowl.vault.economy.Economy.class);
		if (rsp == null) {
			FMLLog.info("Registered Service Provider for Economy.class not found");
			return;
		}
		economy = rsp.getProvider();
		FMLLog.info("Economy successfully hooked up");
	}

	public static Player toBukkitPlayer(EntityPlayer player) {
		try {
			if (Bukkit.getServer() != null) {
				return Bukkit.getServer().getPlayer(player.getName());
			}
		} catch (Exception e) {
		}
		return null;
	}
}

