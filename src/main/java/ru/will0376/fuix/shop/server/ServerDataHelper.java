package ru.will0376.fuix.shop.server;

import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import ru.justagod.cutter.GradleSide;
import ru.justagod.cutter.GradleSideOnly;
import ru.will0376.fuix.Fuix;
import ru.will0376.fuix.donate.network.data.products.ProductItem;
import ru.will0376.fuix.library.init.ServerSide;
import ru.will0376.fuix.library.network.packet.PacketNotification;
import ru.will0376.fuix.library.server.ServerConfig;
import ru.will0376.fuix.library.server.data.Database;
import ru.will0376.fuix.library.util.MainUtils;
import ru.will0376.fuix.shop.network.data.Product;
import ru.will0376.fuix.shop.network.data.ShopEnchant;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@GradleSideOnly(GradleSide.SERVER)
public class ServerDataHelper {

    private final Database database;
    private List<Product> shopProduct = new ArrayList<>();
    private HashMap<Integer, ShopEnchant> shopEnchant = new HashMap<>();

    public ServerDataHelper() {
        this.database = new Database(ServerConfig.address, ServerConfig.port, ServerConfig.username, ServerConfig.password, ServerConfig.basename);
    }

    public List<Product> onLoadCatalog() {
        List<Product> shopProduct = new ArrayList<>();

        try (Connection c = database.getConnection()) {
            PreparedStatement s = c.prepareStatement("SELECT * FROM `" + ServerConfig.catalogTable + "` WHERE `servers` LIKE '%" + ServerConfig.server_id + "%' OR `servers` = 'ALL';");
            s.setQueryTimeout(Database.TIMEOUT);
            try (ResultSet set = s.executeQuery()) {
                while (set.next()) {

                    try {
                        int id = set.getInt("id");
                        int type_id = set.getInt("type_id");
                        String valueString = set.getString("value");
                        JSONObject valueJson = new JSONObject(valueString);
                        int price = set.getInt("price");
                        String name = set.getString("name");
                        String description = set.getString("description");
                        String imageUrl = set.getString("image_url");
                        String servers = set.getString("servers");
                        int amount = set.getInt("amount");
                        int discount = set.getInt("discount");
                        boolean enchant = set.getBoolean("enchant");
                        boolean enable = set.getBoolean("enable");
                        int bought = set.getInt("bought");

                        Product product = new Product(id, type_id, amount, price);
                        if (name.length() > 2) product.setName(name);
                        if (description.length() > 2) product.setDescription(description);
                        product.setEnchant(enchant);
                        product.setEnable(enable);
                        product.setBought(bought);
                        product.setDiscount(discount);
                        product.setImageUrl(imageUrl);
                        product.setServers(servers);

                        Object value = null;
                        if (type_id == 1) {
                            ItemStack stack = MainUtils.createItem(valueJson.getString("item"), amount, !valueJson.isNull("data") ? valueJson
                                    .getInt("data") : 0, !valueJson.isNull("tag") ? valueJson.getString("tag") : "");
                            if (stack != null) {
                                value = new ProductItem(stack);
                            }
                        }

                        if (value != null) {
                            shopProduct.add(product.setValue(value));
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        if (shopProduct.size() > 0) {
            this.shopProduct = shopProduct;
        } else shopProduct = this.shopProduct;

        return shopProduct;
    }

    public Product onEditProduct(Product shopProduct) {
        try (Connection c = database.getConnection()) {
            JSONObject value = new JSONObject();
            if (shopProduct.getTypeId() == 1) {
                ItemStack stack = ((ProductItem) shopProduct.getValue()).getStack();
                value.put("item", stack.getItem().getRegistryName().toString());
                value.put("data", stack.getItemDamage());
            }

            PreparedStatement s;
            if (shopProduct.getId() == -1) {
                s = c.prepareStatement("INSERT INTO `" + ServerConfig.catalogTable + "` (`type_id`, `servers`, `value`, `price`, `name`, `description`, `amount`, `discount`, `enchant`, `enable`, `image_url`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);");
                s.setInt(1, shopProduct.getTypeId());
                s.setString(2, shopProduct.getServers().isEmpty() ? String.valueOf(ServerConfig.server_id) : shopProduct
                        .getServers());
                s.setString(3, value.toString());
                s.setInt(4, shopProduct.getPrice());
                s.setString(5, shopProduct.getName());
                s.setString(6, shopProduct.getDescription());
                s.setInt(7, shopProduct.getAmount());
                s.setInt(8, shopProduct.getDiscount());
                s.setBoolean(9, shopProduct.isEnchant());
                s.setBoolean(10, shopProduct.isEnable());
                s.setString(11, shopProduct.getImageUrl());
            } else {
                s = c.prepareStatement("UPDATE `" + ServerConfig.catalogTable + "` SET `servers` = ?, `value` = ?, `price` = ?, `name` = ?, `description` = ?, `amount` = ?, `discount` = ?, `enchant` = ?, `enable` = ?, `image_url` = ? WHERE `id` = ?;");
                s.setString(1, shopProduct.getServers().isEmpty() ? String.valueOf(ServerConfig.server_id) : shopProduct
                        .getServers());
                s.setString(2, value.toString());
                s.setInt(3, shopProduct.getPrice());
                s.setString(4, shopProduct.getName());
                s.setString(5, shopProduct.getDescription());
                s.setInt(6, shopProduct.getAmount());
                s.setInt(7, shopProduct.getDiscount());
                s.setBoolean(8, shopProduct.isEnchant());
                s.setBoolean(9, shopProduct.isEnable());
                s.setString(10, shopProduct.getImageUrl());
                s.setInt(11, shopProduct.getId());
            }
            s.setQueryTimeout(Database.TIMEOUT);
            s.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        onLoadCatalog();

        return shopProduct;
    }

    public Product onRemoveProduct(Product shopProduct) {
        try (Connection c = database.getConnection()) {
            PreparedStatement s = c.prepareStatement("DELETE FROM `" + ServerConfig.catalogTable + "` WHERE `id` = ?");
            s.setInt(1, shopProduct.getId());
            s.setQueryTimeout(Database.TIMEOUT);
            s.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        onLoadCatalog();

        return shopProduct;
    }

    public HashMap<Integer, ShopEnchant> onLoadEnchant() {
        HashMap<Integer, ShopEnchant> shopEnchant = new HashMap<>();

        try (Connection c = database.getConnection()) {
            PreparedStatement s = c.prepareStatement("SELECT * FROM `" + ServerConfig.enchantTable + "`");
            s.setQueryTimeout(Database.TIMEOUT);
            try (ResultSet set = s.executeQuery()) {
                while (set.next()) {
                    shopEnchant.put(set.getInt("enchant_id"), new ShopEnchant(set.getInt("enchant_id"), set.getString("name"), set
                            .getInt("price"), set.getInt("max")).setDescription(set.getString("desc")));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        if (shopEnchant.size() > 0) {
            this.shopEnchant = shopEnchant;
        } else shopEnchant = this.shopEnchant;

        return shopEnchant;
    }

    public void onBuyProduct(EntityPlayerMP entityPlayerMP, Product shopProduct, List<ShopEnchant> shopEnchants) {
        int shopCount = shopProduct.getAmount();
        if (shopCount > 2304) {
            new PacketNotification("§cОшибка в количестве товара!", 3, "INFO").sendTo(entityPlayerMP);
            return;
        }

        try (Connection c = database.getConnection()) {
            PreparedStatement s = c.prepareStatement("SELECT * FROM `" + ServerConfig.catalogTable + "` WHERE `id` = ? and (`servers` LIKE '%" + ServerConfig.server_id + "%' OR `servers` = 'ALL');");
            s.setInt(1, shopProduct.getId());
            s.setQueryTimeout(Database.TIMEOUT);
            try (ResultSet rs = s.executeQuery()) {
                if (rs.next()) {
                    int id = rs.getInt("id");

                    if (!rs.getBoolean("enable")) {
                        new PacketNotification("§cВ данный момент продукта нет на складе!", 3, "INFO").sendTo(entityPlayerMP);
                        return;
                    }

                    JSONObject value = new JSONObject();

                    int price = (rs.getInt("price") * (100 - rs.getInt("discount")) / 100);

                    int amount = rs.getInt("amount");
                    int amount_ = shopCount / amount;

                    if (rs.getInt("type_id") == 1) {
                        JSONArray enchant = new JSONArray();

                        if (!rs.getBoolean("enchant")) {
                            price = price * amount_;
                        } else {
                            int priceEnchant = 0;
                            try {
                                for (ShopEnchant enchant1 : shopEnchants) {
                                    ShopEnchant enchant2 = getShopEnchant(enchant1.getId());
                                    if (enchant1.getValue() > 0 && enchant2 != null) {
                                        JSONObject jsonObject = new JSONObject();
                                        if (enchant1.getValue() <= enchant2.getMax()) {
                                            priceEnchant += enchant2.getPrice() * enchant1.getValue();
                                            jsonObject.put("id", enchant1.getId());
                                            jsonObject.put("lvl", enchant1.getValue());
                                        } else {
                                            priceEnchant += enchant2.getPrice() * enchant2.getMax();
                                            jsonObject.put("id", enchant1.getId());
                                            jsonObject.put("lvl", enchant2.getMax());
                                        }
                                        enchant.put(jsonObject);
                                    }
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            price = price + priceEnchant;
                            price = price * amount_;
                        }

                        JSONObject dataTag = new JSONObject();
                        try {
                            if (enchant.length() > 0) dataTag.put("ench", enchant);
                            if (!dataTag.isNull("ench")) value.put("dataTag", dataTag);

                            ItemStack stack = ((ProductItem) shopProduct.getValue()).getStack();
                            value.put("item", stack.getItem().getRegistryName().toString());
                            value.put("data", stack.getItemDamage());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else if (rs.getInt("type_id") == 3) {
                        String valueString = rs.getString("value");
                        try {
                            value = new JSONObject(valueString);
                            value.put("title", rs.getString("name"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    if (price <= 0) {
                        new PacketNotification("§cОшибка цена имеет неправильный формат!", 3, "INFO").sendTo(entityPlayerMP);
                        return;
                    }

                    if (!Fuix.getServer().dbHandler.onWithdrawMoney(entityPlayerMP, price)) {
                        new PacketNotification("§cУ вас недостаточно денег для покупки, пополните счёт!", 3, "INFO").sendTo(entityPlayerMP);
                        return;
                    }

                    s = c.prepareStatement("INSERT INTO `" + ServerConfig.cartTable + "` (`uuid`, `server_id`, `type_id`, `value`, `amount`) VALUES (?, ?, ?, ?, ?);");
                    s.setString(1, (entityPlayerMP.getUniqueID().toString()));
                    s.setInt(2, ServerConfig.server_id);
                    s.setInt(3, shopProduct.getTypeId());
                    s.setString(4, value.toString());
                    s.setInt(5, shopCount);
                    s.setQueryTimeout(Database.TIMEOUT);
                    s.executeUpdate();

                    s = c.prepareStatement("UPDATE `" + ServerConfig.catalogTable + "` SET `bought` = `bought` + 1 WHERE `id` = ?;");
                    s.setInt(1, id);
                    s.setQueryTimeout(Database.TIMEOUT);
                    s.executeUpdate();

                    new PacketNotification("§aПокупка прошла успешно! Товар перемещен в корзину.", 3, "INFO").sendTo(entityPlayerMP);

                    ServerSide.onUpdateMoney(entityPlayerMP);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public List<Product> getShopProduct() {
        return shopProduct;
    }

    public List<ShopEnchant> getShopEnchant() {
        return new ArrayList<>(shopEnchant.values());
    }

    public ShopEnchant getShopEnchant(int id) {
        if (shopEnchant.containsKey(id)) {
            return shopEnchant.get(id);
        }
        return null;
    }
}
