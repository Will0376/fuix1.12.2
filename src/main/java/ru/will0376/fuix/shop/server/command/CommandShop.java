package ru.will0376.fuix.shop.server.command;

import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import ru.justagod.cutter.GradleSide;
import ru.justagod.cutter.GradleSideOnly;
import ru.will0376.fuix.Fuix;
import ru.will0376.fuix.library.init.ServerSide;
import ru.will0376.fuix.library.network.packet.PacketNotification;
import ru.will0376.fuix.library.network.simplenet.ShopGuiToClient;
import ru.will0376.fuix.library.server.command.CommandExecuteException;
import ru.will0376.fuix.library.server.command.CommandExecutor;
import ru.will0376.fuix.library.server.command.CommandHandler;
import ru.will0376.fuix.library.util.PermissionsUtils;
import ru.will0376.fuix.shop.server.ServerDataHelper;

@SideOnly(Side.SERVER)
@GradleSideOnly(GradleSide.SERVER)
public class CommandShop extends CommandExecutor {

	@SideOnly(Side.SERVER)
	@CommandHandler(name = "shop", group = "FuixShop")
	public void shop(final ICommandSender sender, final String[] args) throws CommandExecuteException {
		if (sender instanceof EntityPlayerMP) {
			EntityPlayerMP player = (EntityPlayerMP) sender;
			if (args.length <= 0 || args[0].equalsIgnoreCase("item")) {
				ServerDataHelper dataHelper = Fuix.getServer().dataHelper;
				new ShopGuiToClient(ShopGuiToClient.Type.GUISHOP, dataHelper.getShopProduct(), dataHelper.getShopEnchant())
						.setEditMode(PermissionsUtils.hasPex(player, "fuix.shop.edit"))
						.sendTo((EntityPlayerMP) sender);

				ServerSide.onUpdateMoney(player);
			} else if (args[0].equalsIgnoreCase("group")) {
				new PacketNotification("Покупка донат групп в разработки! Ожидайте...", 3, "INFO").sendTo((EntityPlayerMP) sender);
			}
		}
	}
}