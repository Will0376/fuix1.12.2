package ru.will0376.fuix.shop.client.ui.modal;


import net.minecraft.client.Minecraft;
import net.minecraft.util.ResourceLocation;
import ru.will0376.fuix.Fuix;
import ru.will0376.fuix.donate.client.gui.GuiUtils;
import ru.will0376.fuix.donate.network.data.products.ProductItem;
import ru.will0376.fuix.library.client.ui.ModalWindow;
import ru.will0376.fuix.library.client.ui.Window;
import ru.will0376.fuix.library.client.ui.drawable.ButtonDrawable;
import ru.will0376.fuix.library.client.ui.util.ArrayListObserver;
import ru.will0376.fuix.library.client.ui.util.DrawHelper;
import ru.will0376.fuix.library.client.ui.util.FontHelper;
import ru.will0376.fuix.library.client.ui.widget.Button;
import ru.will0376.fuix.library.client.ui.widget.*;
import ru.will0376.fuix.library.network.IGuiResult;
import ru.will0376.fuix.library.network.packet.data.ResultGui;
import ru.will0376.fuix.library.network.simplenet.ShopGuiToServer;
import ru.will0376.fuix.shop.client.ui.GuiShop;
import ru.will0376.fuix.shop.client.ui.elements.ElementEnchant;
import ru.will0376.fuix.shop.network.data.Product;
import ru.will0376.fuix.shop.network.data.ShopEnchant;

import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ModalProductInfo extends ModalWindow implements IGuiResult {

    private final Product shopProduct;
    private final List<ShopEnchant> shopEnchant;
    private final HashMap<Integer, ShopEnchant> enchants;
    private int priceNew = 0;
    private int priceEnchant = 0;
    private NumStep stepNumber;
    private Button btnBuy;
    private Button btnEnchant;
    private int valueNew;

    public ModalProductInfo(Product shopProduct_, List<ShopEnchant> shopEnchant_, Window mainWindow) {
        super(mainWindow, 200, 51); // 161
        this.shopProduct = shopProduct_.clone();
        this.shopEnchant = new ArrayList<ShopEnchant>(shopEnchant_);

        this.priceNew = shopProduct.getPrice();
        this.valueNew = shopProduct.getAmount();
        this.enchants = new HashMap<>();
    }

    public void onResultGui(ResultGui result) {
        btnBuy.setEnabled(true);
    }

    @Override
    protected void onInit(int x, int y) {

        if (getMainWindow() instanceof GuiShop) {
            GuiShop guiShop = (GuiShop) getMainWindow();
            if (guiShop.editMode && shopProduct.getTypeId() != 3) {
                addComponent(new ImageButton("textSetting", new Rectangle(getWidth() - 18, -10, 8, 8), new Rectangle(5, 5), Fuix.MOD_ID, "textures/gui/icons/ic_cog.png", this)
                        .setCallback(() -> {
                            getMainWindow().onViewModal(new ModalProductSetting(shopProduct, shopEnchant, true, getMainWindow()));
                        })
                        .setDrawable(ButtonDrawable.BLUE));
            }
        }

        addComponent(new ImageButton("textExit", new Rectangle(getWidth() - 8, -10, 8, 8), new Rectangle(4, 4), Fuix.MOD_ID, "textures/gui/icons/ic_close.png", this)
                .setCallback(() -> {
                    onClose();
                })
                .setDrawable(ButtonDrawable.RED));

        addComponent(stepNumber = new NumStep("stepNumber", new Rectangle(32 + Minecraft.getMinecraft().fontRenderer.getStringWidth("Количество:"), 15, 34, 9), this)
                .setTuning(shopProduct.getAmount(), 100)
                .setClickListener(new NumStep.StepNumberListener() {
                    @Override
                    public void onStepNumber(int value) {
                        valueNew = value;
                    }
                }));

        stepNumber.setValue(shopProduct.getAmount());
        stepNumber.setStep(shopProduct.getAmount());
        stepNumber.setMax(2304);

        addComponent(btnBuy = (Button) new Button("btnBuy", new Rectangle(50, 14), this).setText("Купить")
                .setCallback(() -> {
                    btnBuy.setEnabled(false);
                    new ShopGuiToServer(ShopGuiToServer.Type.BUY, shopProduct.clone()
                            .setAmount(valueNew), new ArrayList<>(enchants.values())).sendToServer();
                })
                .setShadow(true));

        ListView categoryList = new ListView("EnchantList", new Rectangle(5, 32, getWidth() - 10, 103), this, new ArrayListObserver() {
            @Override
            public ListViewItem getListViewItem() {
                return new ElementEnchant(getWidth() - 14, 12, getListView());
            }
        });
        categoryList.getListViewContent().setElementsPadding(1);
        addComponent(categoryList);

        if (shopProduct.isEnchant()) {
            categoryList.updateList(new ArrayList<ShopEnchant>());
            categoryList.updateList(new ArrayList(shopEnchant));
        }

        categoryList.setVisible(false);
        categoryList.setCallback(new ListView.Callback() {
            @Override
            public void onUpdate(int type, Object value) {
                ShopEnchant enchant = (ShopEnchant) value;

                if (type == 1) {
                    if (enchants.containsKey(enchant.getId())) {
                        enchants.replace(enchant.getId(), enchant);
                    } else enchants.put(enchant.getId(), enchant);
                } else enchants.remove(enchant.getId());

                priceEnchant = 0;
                for (ShopEnchant e : enchants.values()) {
                    priceEnchant += e.getPrice() * e.getValue();
                }
            }
        });

        addComponent(btnEnchant = (Button) new Button("btnEnchant", new Rectangle(Minecraft.getMinecraft().fontRenderer.getStringWidth("Зачаровать предмет") + 10, 14), this)
                .setText("Зачаровать предмет")
                .setCallback(() -> {
                    if (categoryList.isVisible()) {
                        setSize(getWidth(), 51);
                        categoryList.setVisible(false);
                    } else {
                        setSize(getWidth(), 161);
                        categoryList.setVisible(true);
                    }
                })
                .setDrawable(ButtonDrawable.ORANGE));

        btnEnchant.setVisible(shopProduct.isEnchant());

        categoryList.getObserver().getObservableList();
    }

    @Override
    protected void onDraw() {
        Point p = getZeroPoint();

        btnEnchant.setBounds(new Rectangle(new Point(5, getHeight() - 20), btnEnchant.getBounds().getSize()));
        btnBuy.setBounds(new Rectangle(new Point(getWidth() - 55, getHeight() - 20), btnBuy.getBounds().getSize()));

        Window mw = getMainWindow();
        DrawHelper.drawRectRounded(mw.getZeroPoint().x, mw.getZeroPoint().y, mw.getWidth(), mw.getHeight(), 1, new Color(32, 32, 32, 150)
                .hashCode());

        GuiUtils.drawBackgroundGui(getZeroPoint(), getWidth(), getHeight());

        if (shopProduct.getTypeId() == 1) {
            ProductItem productItem = (ProductItem) shopProduct.getValue();

            int price_ = (priceNew + priceEnchant) * valueNew / shopProduct.getAmount();
            String price = price_ + "р";
            if (shopProduct.getDiscount() > 0) {
                price = "§m" + price_ + "р§6 " + (price_ * (100 - shopProduct.getDiscount()) / 100) + "р";
            }

            DrawHelper.draw(new ResourceLocation(Fuix.MOD_ID, "textures/gui/balance.png"), p.getX() + getWidth() - (14 + (Minecraft
                    .getMinecraft().fontRenderer.getStringWidth(price))), p.getY() + 16, 7, 7);
            FontHelper.draw(price, p.getX() + getWidth() - 5, p.getY() + 15, new Color(255, 255, 255).hashCode(), FontHelper.Align.RIGHT, 1);

            DrawHelper.drawRectRounded(p.getX() + 5, p.getY() + 5, 20, 20, 1, new Color(255, 255, 255, 80).hashCode());
            DrawHelper.drawItem(productItem.getStack(), p.getX() + 5, p.getY() + 5, 20);


            FontHelper.draw(productItem.getStack()
                    .getDisplayName(), p.getX() + 29, p.getY() + 6, new Color(255, 255, 255).hashCode(), 1);
            FontHelper.draw("Количество:", p.getX() + 29, p.getY() + 15, new Color(152, 251, 152, 180).hashCode(), 1);

            DrawHelper.drawRect(p.x, p.y + 28, getWidth(), 0.3, new Color(255, 255, 255, 60).hashCode());
            DrawHelper.drawRect(p.x, p.y + getHeight() - 23, getWidth(), 0.3, new Color(255, 255, 255, 60).hashCode());
        }
    }
}
