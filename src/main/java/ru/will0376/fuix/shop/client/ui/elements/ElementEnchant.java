package ru.will0376.fuix.shop.client.ui.elements;

import ru.will0376.fuix.library.client.ui.util.DrawHelper;
import ru.will0376.fuix.library.client.ui.util.FontHelper;
import ru.will0376.fuix.library.client.ui.widget.ListView;
import ru.will0376.fuix.library.client.ui.widget.ListViewItem;
import ru.will0376.fuix.library.client.ui.widget.Slider;
import ru.will0376.fuix.shop.network.data.ShopEnchant;

import java.awt.*;

public class ElementEnchant extends ListViewItem {

    private ShopEnchant enchant;
    private Slider slider;

    public ElementEnchant(int width, int height, ListView listView) {
        super(width, height, listView);
    }

    @Override
    public void onInit(int x, int y) {
        addComponent(slider = new Slider("Slider" + getId(), new Rectangle(40, 8), getWindow()));
        slider.setClickListener(new Slider.ChangeListener() {
            @Override
            public void onChange(int newValue) {
                enchant.setValue(newValue);
                if (getListView().getCallback() != null)
                    getListView().getCallback().onUpdate(newValue > 0 ? 1 : 0, enchant);
            }
        });
        slider.setSizeLine(4, 1);
        slider.setColorRegulator(new Color(255, 255, 255, 200));
    }

    @Override
    protected void onItemDraw(int x, int y, int topLeftX, int topLeftY) {
        if (enchant == null)
            this.enchant = ((ShopEnchant) getListView().getObserver().getObservableList().get(getRenderId()));

        slider.setRange(0, enchant.getMax());

        int posX = getAbsoluteBounds().x;
        int posY = getAbsoluteBounds().y;
        int width = getBounds().width;
        int height = getBounds().height;

        slider.getBounds().x = getBounds().x + width - 64;
        slider.getBounds().y = getBounds().y + 2;

        FontHelper.draw(enchant.getName(), posX + 4, posY + 2, new Color(255, 255, 255).hashCode(), 1);
        DrawHelper.drawRectRounded(posX, posY, width, height, 1, new Color(255, 255, 255, 18).hashCode());
        FontHelper.draw(String.valueOf(enchant.getValue()), posX + width - 10, posY + height / 2, new Color(255, 255, 255)
                .hashCode(), FontHelper.Align.CENTER, 1);
    }

    @Override
    protected void onUpdate(int x, int y) {
    }
}
