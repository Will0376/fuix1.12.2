package ru.will0376.fuix.shop.client.ui.modal;

import com.google.common.collect.ImmutableList;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.NonNullList;
import net.minecraftforge.fml.common.registry.ForgeRegistries;
import ru.will0376.fuix.Fuix;
import ru.will0376.fuix.donate.client.gui.GuiUtils;
import ru.will0376.fuix.donate.network.data.products.ProductItem;
import ru.will0376.fuix.library.client.ui.GuiComponent;
import ru.will0376.fuix.library.client.ui.ModalWindow;
import ru.will0376.fuix.library.client.ui.MouseAction;
import ru.will0376.fuix.library.client.ui.Window;
import ru.will0376.fuix.library.client.ui.drawable.ButtonDrawable;
import ru.will0376.fuix.library.client.ui.drawable.Drawable;
import ru.will0376.fuix.library.client.ui.util.ArrayListObserver;
import ru.will0376.fuix.library.client.ui.util.DrawHelper;
import ru.will0376.fuix.library.client.ui.util.FontHelper;
import ru.will0376.fuix.library.client.ui.widget.TextField;
import ru.will0376.fuix.library.client.ui.widget.*;
import ru.will0376.fuix.shop.network.data.Product;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class ModalSelectItem extends ModalWindow {

    public NonNullList<ItemStack> items = NonNullList.create();

    private TextField textSearch;
    private ListView listView;

    public ModalSelectItem(Window mainWindow) {
        super(mainWindow, 320, 180);

        List<String> itemList = ForgeRegistries.ITEMS.getValues()
                .stream()
                .map(e -> Objects.requireNonNull(e.getRegistryName()).toString())
                .collect(Collectors.toList());
        String[] itemNames = itemList.toArray(new String[0]);
        for (int i = 0; i < itemList.size(); ++i) {
            Item item = Item.getByNameOrId(itemNames[i]);
            if (item != null && item.getCreativeTab() != null && !item.getCreativeTab()
                    .getTabLabel()
                    .equals("buildcraft.facades") && !item.getCreativeTab()
                    .getTabLabel()
                    .equals("buildcraft.boards") && !item.getCreativeTab()
                    .getTabLabel()
                    .equals("appliedenergistics2.facades")) {
                item.getSubItems(item.getCreativeTab(), items);
            }
        }
    }

    @Override
    protected void onInit(int x, int y) {
        addComponent(new ImageButton("textExit", new Rectangle(getWidth() - 8, -10, 8, 8), new Rectangle(4, 4), Fuix.MOD_ID, "textures/gui/icons/ic_close.png", this)
                .setCallback(() -> {
                    onClose();
                })
                .setDrawable(ButtonDrawable.RED));

        textSearch = new TextField("textSearch", new Rectangle(this.getWidth() - 105, 5, 100, 12), this);
        textSearch.setHintText("Поиск...");
        textSearch.setHintTextColor(new Color(255, 255, 255, 200).hashCode());
        textSearch.setDrawable(new Drawable() {
            public void onDraw(GuiComponent component, State state) {
                Rectangle rec = component.getAbsoluteBounds();
                DrawHelper.drawRectRounded(rec.x, rec.y, rec.width, rec.height, 1, new Color(255, 255, 255, 20).hashCode());
            }
        });

        addComponent(textSearch);

        listView = new ListView("CartList", new Rectangle(5, 23, this.getWidth() - 10, this.getHeight() - 28), this, new ArrayListObserver() {
            @Override
            public ListViewItem getListViewItem() {
                return new ElementShop(20, 20, getListView());
            }
        });

        filterBySearch("");

        addComponent(new ListViewSlider("CartListSlider", listView));
    }

    @Override
    protected void onDraw() {
        Window mw = getMainWindow();
        DrawHelper.drawRectRounded(mw.getZeroPoint().x, mw.getZeroPoint().y, mw.getWidth(), mw.getHeight(), 1, new Color(32, 32, 32, 150)
                .hashCode());

        GuiUtils.drawBackgroundGui(getZeroPoint(), getWidth(), getHeight());

        FontHelper.draw("Добавление предмета", getZeroPoint().x + 5, getZeroPoint().y + 6, new Color(255, 255, 255).hashCode(), 1);
        DrawHelper.drawRect(getZeroPoint().x + 2, getZeroPoint().y + 20, getWidth() - 4, 0.3, new Color(255, 255, 255, 60)
                .hashCode());
    }

    protected void onUpdate() {
        if (textSearch != null) {
            if (textSearch.getIsChanged()) filterBySearch(textSearch.getText());
        }
    }

    private ArrayList<ItemStack> filterBySearch(final String query) {
        List<ItemStack> list = ImmutableList.copyOf(items)
                .stream()
                .filter(it -> it.getDisplayName().toLowerCase().contains(query.toLowerCase().trim()))
                .collect(Collectors.toList());

        listView.updateList(new ArrayList<>());
        listView.updateList(new ArrayList<>(list));

        return new ArrayList<>(list);
    }

    public class ElementShop extends ListViewItem {
        private ItemStack stack;

        public ElementShop(int width, int height, ListView listView) {
            super(width, height, listView);
        }

        @Override
        public void onInit(int x, int y) {
            this.setEnabled(true);
        }

        @Override
        protected void onItemDraw(int x, int y, int topLeftX, int topLeftY) {
            if (stack == null)
                this.stack = ((ItemStack) getListView().getObserver().getObservableList().get(getRenderId()));

            int posX = getAbsoluteBounds().x;
            int posY = getAbsoluteBounds().y;

            int width = getBounds().width;
            int height = getBounds().height;

            DrawHelper.drawRectRounded(posX, posY, width, height, 1, new Color(255, 255, 255, 80).hashCode());
            DrawHelper.drawRectRounded(posX + 0.3, posY + 0.3, width - 0.6, height - 0.6, 1, new Color(0, 0, 0, 60).hashCode());

            DrawHelper.drawItem(this.stack, posX + 1, posY + 1, 18);
        }

        @Override
        protected void onUpdate(int x, int y) {
        }

        @Override
        protected void handleMouse(MouseAction mouseAction) {
            if (mouseAction.getActionType()
                    .equals(MouseAction.ActionType.Click) && getAbsoluteBounds().contains(mouseAction.getPosX(), mouseAction
                    .getPosY())) {
                if (stack == null)
                    this.stack = ((ItemStack) getListView().getObserver().getObservableList().get(getRenderId()));

                ModalSelectItem.this.getMainWindow()
                        .onViewModal(new ModalProductSetting(new Product(-1, 1, 1, 1).setValue(new ProductItem(stack)), null, false, getMainWindow()));
            }
        }
    }


}
