package ru.will0376.fuix.shop.client.ui.elements;

import ru.will0376.fuix.library.client.ui.MouseAction;
import ru.will0376.fuix.library.client.ui.util.DrawHelper;
import ru.will0376.fuix.library.client.ui.util.FontHelper;
import ru.will0376.fuix.library.client.ui.widget.ListView;
import ru.will0376.fuix.library.client.ui.widget.ListViewItem;
import ru.will0376.fuix.shop.client.ui.GuiShop;

import java.awt.*;

public class ElementCategory extends ListViewItem {

    private final GuiShop guiShop;
    private GuiShop.CategotyOption categotyOption;

    public ElementCategory(GuiShop guiShop, int width, int height, ListView listView) {
        super(width, height, listView);
        this.guiShop = guiShop;
    }

    @Override
    public void onInit(int x, int y) {
        this.setEnabled(true);
    }

    @Override
    protected void onItemDraw(int x, int y, int topLeftX, int topLeftY) {
        if (categotyOption == null) this.categotyOption = ((GuiShop.CategotyOption) getListView().getObserver()
                .getObservableList()
                .get(getRenderId()));


        int posX = getAbsoluteBounds().x;
        int posY = getAbsoluteBounds().y;
        int width = getBounds().width;
        int height = getBounds().height;

        Color textColor = new Color(255, 255, 255);

        if (isFocused() || guiShop.selectCategory == getRenderId()) {
            DrawHelper.drawRect(posX, posY, width, height, new Color(255, 255, 255, 20).hashCode());
        }

        if (guiShop.selectCategory == getRenderId()) textColor = new Color(255, 204, 0);

        DrawHelper.drawRect(posX, posY + height, width, 0.2, new Color(255, 255, 255, 18).hashCode());

        //GuiDrawHelper.drawRoundedRect(posX + 0.3, posY + 0.3, width - 0.6, height - 0.6, 0.5, new Color(0, 0, 0).hashCode(), 140);

        FontHelper.draw(categotyOption.getName(), posX + 3, posY + (height / 2.0) + 0.5, textColor.hashCode(), FontHelper.Align.CENTER_Y, 0.8F);
    }

    @Override
    protected void onUpdate(int x, int y) {

    }

    @Override
    protected void handleMouse(MouseAction mouseAction) {
        if (mouseAction.getActionType().equals(MouseAction.ActionType.Click) && getAbsoluteBounds().contains(mouseAction
                .getPosX(), mouseAction.getPosY())) {
            if (categotyOption == null) this.categotyOption = ((GuiShop.CategotyOption) getListView().getObserver()
                    .getObservableList()
                    .get(getRenderId()));

            if (guiShop.selectCategory != getRenderId()) {
                guiShop.selectCategory = getRenderId();
                guiShop.setShopProducts(categotyOption.getItems());
            }
        }
    }
}
