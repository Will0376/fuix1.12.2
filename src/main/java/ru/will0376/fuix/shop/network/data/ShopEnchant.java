package ru.will0376.fuix.shop.network.data;


import io.netty.buffer.ByteBuf;
import net.minecraftforge.fml.common.network.ByteBufUtils;

import java.io.IOException;

public class ShopEnchant {

	private int id;
	private String name = "none";
	private String description = "none";
	private int price;
	private int value = 0;
	private int max;

	public ShopEnchant(int id, String name, int price, int max) {
		this.id = id;
		this.name = name;
		this.price = price;
		this.max = max;
	}

	public static ShopEnchant read(ByteBuf buf) throws IOException {
		ShopEnchant shopEnchant = new ShopEnchant(buf.readInt(), ByteBufUtils.readUTF8String(buf), buf.readInt(), buf.readInt());
		shopEnchant.setValue(buf.readInt());
		shopEnchant.setDescription(ByteBufUtils.readUTF8String(buf));
		return shopEnchant;
	}

	public int getId() {
		return id;
	}

	public ShopEnchant setId(int id) {
		this.id = id;
		return this;
	}

	public String getName() {
		return name;
	}

	public ShopEnchant setName(String name) {
		this.name = name;
		return this;
	}

	public String getDescription() {
		return description;
	}

	public ShopEnchant setDescription(String description) {
		this.description = description;
		return this;
	}

	public int getPrice() {
		return price;
	}

	public ShopEnchant setPrice(int price) {
		this.price = price;
		return this;
	}

	public int getMax() {
		return max;
	}

	public ShopEnchant setMax(int max) {
		this.max = max;
		return this;
	}

	public int getValue() {
		return value;
	}

	public ShopEnchant setValue(int value) {
		this.value = value;
		return this;
	}

	/**
	 * ==============================================================================================================
	 **/

	public ByteBuf write(ByteBuf buf) throws IOException {
		buf.writeInt(getId());
		ByteBufUtils.writeUTF8String(buf, getName());
//        buf.writeStringToBuffer(getName());
		buf.writeInt(getPrice());
		buf.writeInt(getMax());
		buf.writeInt(getValue());
//        buf.writeStringToBuffer(getDescription());
		ByteBufUtils.writeUTF8String(buf, getDescription());
		return buf;
	}
}
