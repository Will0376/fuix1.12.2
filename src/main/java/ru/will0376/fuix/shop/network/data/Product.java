package ru.will0376.fuix.shop.network.data;

import io.netty.buffer.ByteBuf;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import ru.will0376.fuix.donate.network.data.products.ProductGroup;
import ru.will0376.fuix.donate.network.data.products.ProductItem;

import java.io.IOException;

public class Product {

    private final int id;
    private final int type_id;
    private String name = "None";
    private String description = "None";

    private Object value;

    private int amount = 1;
    private int price = 0;
    private int discount = 0;
    private boolean enchant = false;
    private boolean enable = true;
    private int bought = 0;
    private String servers = "";
    private String imageUrl = "";

    public Product(int id, int type_id, int amount, int price) {
        this.id = id;
        this.type_id = type_id;
        this.amount = amount;
        this.price = price;
    }

    public static Product read(ByteBuf buf) throws IOException {
        int id = buf.readInt();
        int type_id = buf.readInt();

        Product shopProduct = new Product(id, type_id, buf.readInt(), buf.readInt());
        shopProduct.setName(ByteBufUtils.readUTF8String(buf));
        shopProduct.setDescription(ByteBufUtils.readUTF8String(buf));
        shopProduct.setDiscount(buf.readInt());
        shopProduct.setEnchant(buf.readBoolean());
        shopProduct.setEnable(buf.readBoolean());
        shopProduct.setBought(buf.readInt());

        shopProduct.setImageUrl(ByteBufUtils.readUTF8String(buf));
        shopProduct.setServers(ByteBufUtils.readUTF8String(buf));

        Object value = null;

        switch (type_id) {
            case 1:
                value = ProductItem.read(buf);
                break;
            case 3:
                value = ProductGroup.read(buf);
                break;
        }

        shopProduct.setValue(value);

        return shopProduct;
    }

    public int getId() {
        return id;
    }

    public int getTypeId() {
        return type_id;
    }

    public Object getValue() {
        return value;
    }

    public Product setValue(Object value) {
        this.value = value;
        return this;
    }

    public String getName() {
        return name;
    }

    public Product setName(String name) {
        this.name = name;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public Product setDescription(String description) {
        this.description = description;
        return this;
    }

    public int getAmount() {
        return amount;
    }

    public Product setAmount(int amount) {
        this.amount = amount;
        return this;
    }

    public int getPrice() {
        return price;
    }

    public Product setPrice(int price) {
        this.price = price;
        return this;
    }

    public boolean isEnchant() {
        return enchant;
    }

    public Product setEnchant(boolean enchant) {
        this.enchant = enchant;
        return this;
    }

    public boolean isEnable() {
        return enable;
    }

    public Product setEnable(boolean enable) {
        this.enable = enable;
        return this;
    }

    public int getBought() {
        return bought;
    }

    public Product setBought(int bought) {
        this.bought = bought;
        return this;
    }

    public int getDiscount() {
        return discount;
    }

    public Product setDiscount(int discount) {
        this.discount = discount;
        return this;
    }

    public String getServers() {
        return servers;
    }

    public Product setServers(String servers) {
        this.servers = servers;
        return this;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public Product setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
        return this;
    }

    /**
     * ==============================================================================================================
     **/

    public ByteBuf write(ByteBuf buf) throws IOException {
        buf.writeInt(getId());
        buf.writeInt(getTypeId());
        buf.writeInt(getAmount());
        buf.writeInt(getPrice());

        ByteBufUtils.writeUTF8String(buf, getName());
        ByteBufUtils.writeUTF8String(buf, getDescription());
       /* buf.writeStringToBuffer(getName());
        buf.writeStringToBuffer(getDescription());*/
        buf.writeInt(getDiscount());
        buf.writeBoolean(isEnchant());
        buf.writeBoolean(isEnable());
        buf.writeInt(getBought());

        /*buf.writeStringToBuffer(getImageUrl());
        buf.writeStringToBuffer(getServers());*/

        ByteBufUtils.writeUTF8String(buf, getImageUrl());
        ByteBufUtils.writeUTF8String(buf, getServers());

        switch (getTypeId()) {
            case 1:
                ((ProductItem) getValue()).write(buf);
                break;
            case 3:
                ((ProductGroup) getValue()).write(buf);
                break;
        }

        return buf;
    }

    public Product clone() {
        return new Product(getId(), getTypeId(), getAmount(), getPrice()).setValue(getValue())
                .setName(getName())
                .setDescription(getDescription())
                .setAmount(getAmount())
                .setEnchant(isEnchant())
                .setEnable(isEnable())
                .setBought(getBought())
                .setDiscount(getDiscount())
                .setImageUrl(getImageUrl())
                .setServers(getServers());
    }
}
