package ru.will0376.fuix.shop.network.data;

import java.util.ArrayList;

public class ShopCategory {

	private String name;
	private ArrayList<Product> items = new ArrayList<Product>();

	public ShopCategory(String name) {
		this.name = name;
		this.items = new ArrayList<Product>();
	}

	public ShopCategory addItem(Product item) {
		this.items.add(item);
		return this;
	}

	public String getName() {
		return name;
	}

	public ShopCategory setName(String name) {
		this.name = name;
		return this;
	}

	public ArrayList<Product> getItems() {
		return items;
	}

	public ShopCategory setItems(ArrayList<Product> items) {
		this.items = items;
		return this;
	}
}
